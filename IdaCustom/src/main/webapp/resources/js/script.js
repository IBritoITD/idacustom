/**
 * AdminLTE Demo Menu ------------------ You should not use this file in
 * production. This file is for demo purposes only.
 */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.sidebar-menu a').filter(function() {
	return this.href == url;
}).parent().addClass('active');

// for treeview
$('ul.treeview-menu a').filter(function() {
	return this.href == url;
}).parentsUntil($("ul.level-1")).addClass('active');

$("#inputMenu").keyup(function() {
	console.log("Key Upped!");
	filterMenu();
});
