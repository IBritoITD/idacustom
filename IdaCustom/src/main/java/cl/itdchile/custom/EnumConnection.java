package cl.itdchile.custom;

public enum EnumConnection {
	ABERTIS(1),
	DESARROLLO(2),
	AFIANZA(3),
	SEIDEMAN(4),
	NORMALIZA(5);
	

	private int conn;
	
	private EnumConnection (int conn){
		this.conn = conn;
	}

	public int getConn() {
		return conn;
	}

	public void setConn(int conn) {
		this.conn = conn;
	}

	
}
