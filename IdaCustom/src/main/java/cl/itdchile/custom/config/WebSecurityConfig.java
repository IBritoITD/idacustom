package cl.itdchile.custom.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

import cl.itdchile.custom.service.ida.UserDetailsServiceImp;
import cl.itdchile.custom.utils.StringUtil;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence charSequence) {
				return StringUtil.encodePassword(charSequence.toString(), "SHA1");
			}

			@Override
			public boolean matches(CharSequence charSequence, String s) {
				return StringUtil.encodePassword(charSequence.toString(), "SHA1").equals(s);
			}
		};
	}

	@Bean
	public AuthenticationProvider authenticator() {
		return new UserDetailsServiceImp();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticator());

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		super.configure(web);
		web.ignoring().antMatchers("/forward-sms", "/forward-dlr");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		http.authorizeRequests().antMatchers("/javax.faces.resource/**").permitAll();
		http.authorizeRequests().antMatchers("/api/**").permitAll();
		http.csrf().disable();

		http.authorizeRequests().antMatchers("/views/Admin/**").hasRole("ADMIN").and().formLogin(). // login
																									// configuration
				loginPage("/views/login.xhtml").loginProcessingUrl("/appLogin").usernameParameter("app_username")
				.passwordParameter("app_password").defaultSuccessUrl("/views/Admin/index.xhtml").and().logout(). // logout
																													// configuration
				logoutUrl("/appLogout");
		http.exceptionHandling().accessDeniedPage("/views/Error/404.xhtml");
		http.sessionManagement().invalidSessionUrl("/views/login.xhtml");

	}

}
