package cl.itdchile.custom.config;

import static org.hibernate.cfg.AvailableSettings.C3P0_ACQUIRE_INCREMENT;
import static org.hibernate.cfg.AvailableSettings.C3P0_MAX_SIZE;
import static org.hibernate.cfg.AvailableSettings.C3P0_MAX_STATEMENTS;
import static org.hibernate.cfg.AvailableSettings.C3P0_MIN_SIZE;
import static org.hibernate.cfg.AvailableSettings.C3P0_TIMEOUT;
import static org.hibernate.cfg.AvailableSettings.DRIVER;
import static org.hibernate.cfg.AvailableSettings.HBM2DDL_AUTO;
import static org.hibernate.cfg.AvailableSettings.PASS;
import static org.hibernate.cfg.AvailableSettings.SHOW_SQL;
import static org.hibernate.cfg.AvailableSettings.URL;
import static org.hibernate.cfg.AvailableSettings.USER;

import java.util.Properties;

import javax.management.relation.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.FileEntity;
import cl.itdchile.custom.model.ForwardSmsEntity;
import cl.itdchile.custom.model.JobTypeEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.ProcessTypeEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.model.SendSmsEntity;
import cl.itdchile.custom.model.ida.RoleEntity;
import cl.itdchile.custom.model.ida.SentSmsEntity;
import cl.itdchile.custom.model.ida.SmsLinkEntity;
import cl.itdchile.custom.model.ida.UserEntity;
import cl.itdchile.custom.model.normaliza.ReportSentSmsEntity;
import cl.itdchile.custom.model.normaliza.ReportSentSmsWaitingEntity;

@Configuration
@PropertySources({ @PropertySource("classpath:db.properties"), @PropertySource("classpath:app.properties"),
		@PropertySource("classpath:admin-config.properties"),
		@PropertySource("classpath:jasperreports_extension.properties") })
@EnableTransactionManagement
@ComponentScans(value = { @ComponentScan("cl.itdchile.custom") })
public class AppConfig {

	@Autowired
	private Environment env;

	@Primary
	@Bean(name = "customSessionFactory")
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

		Properties props = new Properties();

		// Setting JDBC properties
		props.put(DRIVER, env.getProperty("mysql.driver"));
		props.put(URL, env.getProperty("mysql.jdbcUrl"));
		props.put(USER, env.getProperty("mysql.customUsername"));
		props.put(PASS, env.getProperty("mysql.customPassword"));

		// Setting Hibernate properties
		props.put(SHOW_SQL, env.getProperty("hibernate.show_sql"));
		props.put(HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));

		// Setting C3P0 properties
		props.put(C3P0_MIN_SIZE, env.getProperty("hibernate.c3p0.min_size"));
		props.put(C3P0_MAX_SIZE, env.getProperty("hibernate.c3p0.max_size"));
		props.put(C3P0_ACQUIRE_INCREMENT, env.getProperty("hibernate.c3p0.acquire_increment"));
		props.put(C3P0_TIMEOUT, env.getProperty("hibernate.c3p0.timeout"));
		props.put(C3P0_MAX_STATEMENTS, env.getProperty("hibernate.c3p0.max_statements"));

		factoryBean.setHibernateProperties(props);

		factoryBean.setAnnotatedClasses(FileEntity.class, ConnectionEntity.class, SendSmsEntity.class,
				ProcessTrackingEntity.class, ProcessTypeEntity.class, EmailEntity.class, RunningJobEntity.class,
				ForwardSmsEntity.class, ReportSentSmsWaitingEntity.class, ReportSentSmsEntity.class,
				JobTypeEntity.class);

		return factoryBean;
	}

	@Primary
	@Bean(name = "customTransactionManager")
	@Qualifier("defTransaction")
	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getSessionFactory().getObject());
		return transactionManager;
	}

	@Bean(name = "idaSessionFactory")
	public LocalSessionFactoryBean getIdaSessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

		Properties props = new Properties();

		// Setting JDBC properties
		props.put(DRIVER, env.getProperty("mysql.driver"));
		props.put(URL, env.getProperty("mysql.jdbcUrlIDA"));
		props.put(USER, env.getProperty("mysql.idaUsername"));
		props.put(PASS, env.getProperty("mysql.idaPassword"));

		// Setting Hibernate properties
		props.put(SHOW_SQL, env.getProperty("hibernate.show_sql"));
		props.put(HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));

		// Setting C3P0 properties
		props.put(C3P0_MIN_SIZE, env.getProperty("hibernate.c3p0.min_size"));
		props.put(C3P0_MAX_SIZE, env.getProperty("hibernate.c3p0.max_size"));
		props.put(C3P0_ACQUIRE_INCREMENT, env.getProperty("hibernate.c3p0.acquire_increment"));
		props.put(C3P0_TIMEOUT, env.getProperty("hibernate.c3p0.timeout"));
		props.put(C3P0_MAX_STATEMENTS, env.getProperty("hibernate.c3p0.max_statements"));

		factoryBean.setHibernateProperties(props);

		factoryBean.setAnnotatedClasses(SmsLinkEntity.class, SentSmsEntity.class, UserEntity.class, RoleEntity.class);

		return factoryBean;
	}

	@Bean(name = "idaTransactionManager")
	public HibernateTransactionManager getIdaTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getIdaSessionFactory().getObject());
		return transactionManager;
	}

}
