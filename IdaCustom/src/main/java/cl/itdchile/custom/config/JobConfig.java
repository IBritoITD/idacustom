package cl.itdchile.custom.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import cl.itdchile.custom.EnumConnection;
import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.job.JobAbertis;
import cl.itdchile.custom.job.JobAfianza;
import cl.itdchile.custom.job.JobNormaliza;
import cl.itdchile.custom.job.JobSeideman;
import cl.itdchile.custom.utils.TriggerUtil;

@Configuration
@EnableAsync
@EnableScheduling
public class JobConfig implements SchedulingConfigurer {
	private static Logger log = Logger.getLogger(JobConfig.class);
	@Autowired
	Environment env;
	@Autowired(required = true)
	protected IConnectionDao conDao;

	@Bean
	public JobAfianza jobAfianza() {
		return new JobAfianza();
	}

	@Bean
	public JobSeideman jobSeideman() {
		return new JobSeideman();
	}

	@Bean
	public JobNormaliza jobNormaliza() {
		return new JobNormaliza();
	}

	@Bean
	public JobAbertis jobAbertis() {
		return new JobAbertis();
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		// TODO Auto-generated method stub
		taskRegistrar.setScheduler(taskExecutor());

	taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobAfianza().CreatePdfFileAfianzajob();
			}
		}, new TriggerUtil(conDao, EnumConnection.AFIANZA.getConn(), EnumJobType.GENERATE_INPUT.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobAfianza().generateNotProcessedActive();
			}
		}, new TriggerUtil(conDao, EnumConnection.AFIANZA.getConn(), EnumJobType.NOT_PROCESSED.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobSeideman().CreatePdfFileSeidemanjob();
			}
		}, new TriggerUtil(conDao, EnumConnection.SEIDEMAN.getConn(), EnumJobType.GENERATE_INPUT.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobSeideman().generateNotProcessedActive();
			}
		}, new TriggerUtil(conDao, EnumConnection.SEIDEMAN.getConn(), EnumJobType.NOT_PROCESSED.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobNormaliza().generatingCSVJob();
			}
		}, new TriggerUtil(conDao, EnumConnection.NORMALIZA.getConn(), EnumJobType.GENERATE_INPUT.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobAbertis().createXlsxFileJob();
			}
		}, new TriggerUtil(conDao, EnumConnection.ABERTIS.getConn(), EnumJobType.GENERATE_INPUT.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobAbertis().generateOutputReport();
			}
		}, new TriggerUtil(conDao, EnumConnection.ABERTIS.getConn(), EnumJobType.GENERATE_OUTPUT.getValue()));

		taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				jobAbertis().generateNotProcessed();
			}
		}, new TriggerUtil(conDao, EnumConnection.ABERTIS.getConn(), EnumJobType.NOT_PROCESSED.getValue()));

	}

	@Bean(destroyMethod = "shutdown")
	public Executor taskExecutor() {
		return Executors.newScheduledThreadPool(100);
	}

}
