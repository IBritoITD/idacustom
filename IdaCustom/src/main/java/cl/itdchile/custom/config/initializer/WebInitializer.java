package cl.itdchile.custom.config.initializer;

import java.io.FileNotFoundException;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.log4j.Logger;
import org.springframework.util.ResourceUtils;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.sun.faces.config.ConfigureListener;

import cl.itdchile.custom.config.WebConfig;
import cl.itdchile.custom.config.WebSecurityConfig;
import cl.itdchile.custom.config.listener.SessionListener;

public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	private static Logger log = Logger.getLogger(WebInitializer.class);
	public static String REAL_PATH;
	public static String REAL_PATH_REPORT;
	public static String FILE_SEPARATOR;

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		try {
			log.info(ResourceUtils.getFile("classpath:report").getPath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	/*	REAL_PATH = servletContext.getRealPath(Constants.FILE_SEP);
		log.info("REAL_PATH " + REAL_PATH);

		REAL_PATH_REPORT = REAL_PATH + Constants.FILE_SEP + "WEB-INF" + Constants.FILE_SEP + "classes"
				+ Constants.FILE_SEP + "Report" + Constants.FILE_SEP;

		log.info("REAL_PATH_REPORT " + REAL_PATH_REPORT);
		*/

		servletContext.addListener(ConfigureListener.class);
		servletContext.addListener(SessionListener.class);
		servletContext.addFilter("characterEncodingFilter", new CharacterEncodingFilter("UTF-8"));

		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
		webCtx.register(WebConfig.class);
		// webCtx.register(WebSecurityConfig.class);

		webCtx.setServletContext(servletContext);

		DispatcherServlet dispatcherServlet = new DispatcherServlet(webCtx);

		// throw NoHandlerFoundException to Controller
		dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);

		ServletRegistration.Dynamic servlet = servletContext.addServlet("dispatcher", dispatcherServlet);

		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
		servlet.addMapping("*.xhtml");

		ContextLoaderListener contextLoaderListener = new ContextLoaderListener(webCtx);

		servletContext.addListener(contextLoaderListener);

		// Filter.
		FilterRegistration.Dynamic fr = servletContext.addFilter("encodingFilter", CharacterEncodingFilter.class);

		fr.setInitParameter("encoding", "UTF-8");
		fr.setInitParameter("forceEncoding", "true");
		fr.addMappingForUrlPatterns(null, true, "/*");

	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return new Class<?>[] { WebSecurityConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return null;// new Class<?>[] { ErrorHandleFilter.class };
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		return new String[] { "/" };
	}
}
