package cl.itdchile.custom.config.filter;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@WebFilter(urlPatterns = "/views/Admin/*")
public class LoginFilter implements Filter {
	private static Log log = LogFactory.getLog(LoginFilter.class);

	@Override
	public void destroy() {
		// ...
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		String url = request instanceof HttpServletRequest ? httpRequest.getRequestURL().toString() : "N/A";
		
		try {
			log.info("from filter login processing url: " + url + "  " );

			// BufferedReader in = new BufferedReader(new
			// InputStreamReader(validateurl.openStream()));
			// System.out.println(in.readLine());

			chain.doFilter(request, response);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error ", e);
			// httpResponse.sendRedirect(httpRequest.getContextPath() +
			// "/views/Error/500.xhtml");

		}

	}


}
