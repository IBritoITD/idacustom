package cl.itdchile.custom.config.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

public class SessionListener implements HttpSessionListener {
	private static Logger log = Logger.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		log.info("session created");
		event.getSession().setMaxInactiveInterval(3600);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		log.info("session destroyed");
	}
}
