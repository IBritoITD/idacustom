package cl.itdchile.custom.dao.imp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Repository;

import cl.itdchile.custom.dao.IFileDao;
import cl.itdchile.custom.model.FileEntity;

@Repository
public class FileDaoImp implements IFileDao, Serializable {
	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public void saveFile(FileEntity file) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(file);
	}

	@Override
	public void deleteFile(Long fileId) {
		FileEntity file = (FileEntity) sessionFactory.getCurrentSession().load(FileEntity.class, fileId);
		if (null != file) {
			this.sessionFactory.getCurrentSession().delete(file);
		}

	}

	@Override
	public FileEntity getFile(Long fileId) {
		FileEntity file = (FileEntity) sessionFactory.getCurrentSession().load(FileEntity.class, fileId);
		if (null != file) {
			return file;
		} else {
			throw new ObjectRetrievalFailureException(FileEntity.class, fileId);
		}

	}

	@Override
	public List<FileEntity> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FileEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams) {
		Query namedQuery = sessionFactory.getCurrentSession().getNamedQuery(queryName);

		for (String s : queryParams.keySet()) {
			if (queryParams.get(s) instanceof Set) {
				namedQuery.setParameterList(s, (Set) queryParams.get(s));
			} else {
				namedQuery.setParameter(s, queryParams.get(s));
			}
		}

		return namedQuery.list();
	}

}
