package cl.itdchile.custom.dao;

import cl.itdchile.custom.model.ForwardSmsEntity;

public interface IForwardSmsDao {

	public void save(ForwardSmsEntity entity) throws Exception;

}
