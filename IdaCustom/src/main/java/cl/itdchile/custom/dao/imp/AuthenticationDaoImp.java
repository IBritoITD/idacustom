package cl.itdchile.custom.dao.imp;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import cl.itdchile.custom.dao.IAuthenticationDao;

public class AuthenticationDaoImp implements IAuthenticationDao{

	@Override
	public Authentication getAuthentication() {
		// TODO Auto-generated method stub
		return SecurityContextHolder.getContext().getAuthentication();
	}

}
