package cl.itdchile.custom.dao.imp;

import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.model.EmailEntity;

@Repository
@Transactional
public class EmailDaoImp implements IEmailDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(EmailEntity email) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Long id) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public EmailEntity getEmail(int id) throws Exception {
		// TODO Auto-generated method stub
		EmailEntity result = null;
		try (Session session = sessionFactory.openSession()) {

			result = session.get(EmailEntity.class, id);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public List<EmailEntity> getAll() throws Exception {
		// TODO Auto-generated method stub
		List<EmailEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<EmailEntity> cq = cb.createQuery(EmailEntity.class);
			Root<EmailEntity> cRoot = cq.from(EmailEntity.class);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<EmailEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmailEntity getEmailByType(int idProcessType) throws Exception {
		EmailEntity result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<EmailEntity> cq = cb.createQuery(EmailEntity.class);
			Root<EmailEntity> cRoot = cq.from(EmailEntity.class);

			Predicate processType = cb.equal(cRoot.get("id_process_type"), idProcessType);

			cq.where(processType);

			cq.select(cRoot);
			if (session.createQuery(cq).getResultList().size() > 0)
				result = session.createQuery(cq).getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

}
