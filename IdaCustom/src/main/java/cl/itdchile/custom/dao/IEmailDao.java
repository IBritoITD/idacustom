package cl.itdchile.custom.dao;

import java.util.List;
import java.util.Map;

import cl.itdchile.custom.model.EmailEntity;

public interface IEmailDao {

	void save(EmailEntity email) throws Exception;

	List<EmailEntity> getAll() throws Exception;

	void delete(Long id) throws Exception;

	EmailEntity getEmail(int id) throws Exception;

	EmailEntity getEmailByType(int idProcessType) throws Exception;

	List<EmailEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams) throws Exception;

}
