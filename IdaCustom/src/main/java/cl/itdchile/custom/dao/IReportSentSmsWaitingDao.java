package cl.itdchile.custom.dao;


import java.util.List;

import cl.itdchile.custom.model.normaliza.ReportSentSmsWaitingEntity;

public interface IReportSentSmsWaitingDao {

	public List<ReportSentSmsWaitingEntity> getReportSentSmsWaiting() throws Exception;

	public void save(ReportSentSmsWaitingEntity entity) throws Exception;

	void delete(ReportSentSmsWaitingEntity entity) throws Exception;
}
