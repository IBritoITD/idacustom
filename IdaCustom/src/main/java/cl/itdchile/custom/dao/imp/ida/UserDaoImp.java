package cl.itdchile.custom.dao.imp.ida;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.ida.IUserDao;
import cl.itdchile.custom.model.ida.UserEntity;

@Repository
@Transactional(transactionManager = "idaTransactionManager")
public class UserDaoImp implements Serializable, IUserDao {

	@Resource(name = "idaSessionFactory")
	private SessionFactory idaSessionFactory;

	@Override
	public UserEntity loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub

		CriteriaBuilder cb = idaSessionFactory.getCurrentSession().getCriteriaBuilder();

		CriteriaQuery<UserEntity> cq = cb.createQuery(UserEntity.class);
		Root<UserEntity> cRoot = cq.from(UserEntity.class);

		Predicate user = cb.equal(cRoot.get("username"), username);

		cq.where(user);

		cq.select(cRoot);

		List<UserEntity> users = idaSessionFactory.getCurrentSession().createQuery(cq).getResultList();
		if (users == null || users.isEmpty()) {
			throw new UsernameNotFoundException("user '" + username + "' not found...");
		} else {
			return users.stream().findFirst().get();
		}

	}

}
