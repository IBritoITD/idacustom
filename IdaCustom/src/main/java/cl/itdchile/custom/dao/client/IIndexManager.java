package cl.itdchile.custom.dao.client;

import org.springframework.stereotype.Repository;

import cl.itdchile.custom.model.client.IndexFullEntity;

@Repository
public interface IIndexManager {

	IndexFullEntity getInitial() throws Exception;

}
