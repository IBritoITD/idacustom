package cl.itdchile.custom.dao;

import java.util.List;

import cl.itdchile.custom.model.SendSmsEntity;

public interface ISendSmsManager {

	void registerSms(SendSmsEntity entity) throws Exception;

	void forwardDlr(String status, String key, String date) throws Exception;

	List<SendSmsEntity> getAll() throws Exception;

	void getProcessed() throws Exception;

	void getNotVerified() throws Exception;

}
