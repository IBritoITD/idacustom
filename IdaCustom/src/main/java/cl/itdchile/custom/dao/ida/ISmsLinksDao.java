package cl.itdchile.custom.dao.ida;

import cl.itdchile.custom.model.ida.SmsLinkEntity;

public interface ISmsLinksDao {

	public SmsLinkEntity getSmsLink(String code) throws Exception;

	public void save(SmsLinkEntity entity) throws Exception;

}
