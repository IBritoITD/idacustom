package cl.itdchile.custom.dao;

import java.util.List;
import java.util.Map;

import cl.itdchile.custom.model.FileEntity;

public interface IAppManager {
    public void saveFile(FileEntity file);
    public List<FileEntity> getAll();
    public void deleteFile(Long fileId);
    public FileEntity getFile(Long fileId);
    public void setFileDao(IFileDao fileDao);
    List<FileEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams);
}
