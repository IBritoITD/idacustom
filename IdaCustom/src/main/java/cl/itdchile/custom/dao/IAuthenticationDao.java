package cl.itdchile.custom.dao;

import org.springframework.security.core.Authentication;

public interface IAuthenticationDao {
	Authentication getAuthentication();
}
