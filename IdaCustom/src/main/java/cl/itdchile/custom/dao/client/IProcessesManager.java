package cl.itdchile.custom.dao.client;

import java.util.List;

import org.springframework.stereotype.Repository;

import cl.itdchile.custom.model.RunningJobEntity;

@Repository
public interface IProcessesManager {

	List<RunningJobEntity> getOldProcesses() throws Exception;

	void stopProcess(RunningJobEntity entity) throws Exception;

	void stopAllProcess(List<RunningJobEntity> entity) throws Exception;

}
