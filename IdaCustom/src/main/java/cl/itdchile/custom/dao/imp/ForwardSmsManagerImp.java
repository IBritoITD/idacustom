package cl.itdchile.custom.dao.imp;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.mchange.rmi.NotAuthorizedException;

import cl.itdchile.custom.dao.IForwardSmsDao;
import cl.itdchile.custom.dao.IForwardSmsManager;
import cl.itdchile.custom.model.ForwardSmsEntity;
import cl.itdchile.custom.utils.SOAPUtilManager;

@Repository
public class ForwardSmsManagerImp implements IForwardSmsManager {

	@Autowired
	private Environment env;
	@Autowired
	protected IForwardSmsDao dao;

	protected SOAPUtilManager soapManager;

	protected String fechahora;
	protected String sms;
	protected String celular;
	protected String response;

	@Override
	public void forwardSms(ForwardSmsEntity entity) throws Exception {
		try {
			SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy HH:mm");

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date d = format2.parse(entity.getOther_message());

			String dateString = format.format(d);

			this.fechahora = dateString;
			this.celular = entity.getAni();

			if (entity.getMessage() != null && !entity.getMessage().equalsIgnoreCase("")
					&& entity.getMessage().toUpperCase().substring(0, 3).equalsIgnoreCase("RUT")) {
				this.sms = entity.getMessage();
				if (entity.getUsername().equalsIgnoreCase(env.getProperty("api.clent.user"))
						&& entity.getPassword().equalsIgnoreCase(env.getProperty("api.clent.password"))) {

					this.soapManager = new SOAPUtilManager(fechahora.trim(), sms.trim(), celular.trim(),
							env.getProperty("api.clent.uri").trim());

					this.response = this.soapManager.callSoapWebService(env.getProperty("api.clent.url"),
							env.getProperty("api.clent.action"));
					entity.setCreated_at(new Date());
					entity.setResponse(this.response);

					this.dao.save(entity);

				} else {
					throw new NotAuthorizedException("Error de Autenticacion");
				}

			} else {
				throw new NotAuthorizedException("El SMS no comenzaba con la palabra RUT");
			}

		} catch (NotAuthorizedException e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new NotAuthorizedException(e.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
	}
}
