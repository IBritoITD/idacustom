package cl.itdchile.custom.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IForwardSmsDao;
import cl.itdchile.custom.model.ForwardSmsEntity;

@Repository
@Transactional
public class ForwardSmsDaoImp implements IForwardSmsDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(ForwardSmsEntity entity) throws Exception {
		try {

			sessionFactory.getCurrentSession().saveOrUpdate(entity);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

}
