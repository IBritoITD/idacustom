package cl.itdchile.custom.dao.imp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.RunningJobEntity;

@Repository
@Transactional
public class RunningJobDaoImp implements Serializable, IRunningJobDao {

	private static Logger log = Logger.getLogger(RunningJobDaoImp.class);

	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public void save(RunningJobEntity entity) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(entity);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Override
	public RunningJobEntity get(int idJobType, int idProcessType) throws Exception {
		RunningJobEntity result = null;
		try {

			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();

			CriteriaQuery<RunningJobEntity> cq = cb.createQuery(RunningJobEntity.class);
			Root<RunningJobEntity> cRoot = cq.from(RunningJobEntity.class);

			Predicate jobType = cb.equal(cRoot.get("id_job_type"), idJobType);
			Predicate processType = cb.equal(cRoot.get("id_process_type"), idProcessType);

			cq.where(jobType, processType);

			cq.select(cRoot);
			if (sessionFactory.getCurrentSession().createQuery(cq).getResultList().size() > 0)
				result = sessionFactory.getCurrentSession().createQuery(cq).getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void delete(RunningJobEntity entity) throws Exception {
		try {
			sessionFactory.getCurrentSession().delete(entity);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public List<RunningJobEntity> getOldProcesses() throws Exception {
		List<RunningJobEntity> result = null;
		try {

			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();

			CriteriaQuery<RunningJobEntity> cq = cb.createQuery(RunningJobEntity.class);
			Root<RunningJobEntity> cRoot = cq.from(RunningJobEntity.class);

			Predicate date = cb.lessThan(cRoot.get("created_at"),
					new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(2)));

			cq.where(date);

			cq.select(cRoot);

			result = sessionFactory.getCurrentSession().createQuery(cq).getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;

	}

	@Override
	public List<RunningJobEntity> getProcesses() throws Exception {
		List<RunningJobEntity> result = null;
		try {

			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();

			CriteriaQuery<RunningJobEntity> cq = cb.createQuery(RunningJobEntity.class);
			Root<RunningJobEntity> cRoot = cq.from(RunningJobEntity.class);

			cq.select(cRoot);

			result = sessionFactory.getCurrentSession().createQuery(cq).getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

}
