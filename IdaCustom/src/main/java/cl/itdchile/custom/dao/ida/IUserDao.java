package cl.itdchile.custom.dao.ida;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import cl.itdchile.custom.model.ida.UserEntity;

public interface IUserDao  {

	UserEntity loadUserByUsername(String username) throws UsernameNotFoundException;


}
