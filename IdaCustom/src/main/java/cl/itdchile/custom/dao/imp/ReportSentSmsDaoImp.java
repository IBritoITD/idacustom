package cl.itdchile.custom.dao.imp;

import java.io.Serializable;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IReportSentSmsDao;
import cl.itdchile.custom.model.normaliza.ReportSentSmsEntity;

@Repository
@Transactional
public class ReportSentSmsDaoImp implements Serializable, IReportSentSmsDao {

	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public ReportSentSmsEntity getReportSentSms(int process_type_id) throws Exception {
		ReportSentSmsEntity result = null;
		try {

			Query namedQuery = sessionFactory.getCurrentSession()
					.createNamedQuery("reportSentSms", ReportSentSmsEntity.class).setParameter(1, process_type_id);

			if (namedQuery.getResultList().size() > 0)
			result = (ReportSentSmsEntity) namedQuery.getResultList().stream().findFirst().get();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void save(ReportSentSmsEntity entity) throws Exception {
		try {

			sessionFactory.getCurrentSession().saveOrUpdate(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}

	}
	
	@Override
	public void delete(ReportSentSmsEntity entity) throws Exception {
		try {
			sessionFactory.getCurrentSession().delete(entity);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
	}

}
