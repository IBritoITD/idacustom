package cl.itdchile.custom.dao;

import java.util.List;
import java.util.Map;

import cl.itdchile.custom.model.SendSmsEntity;

public interface ISendSmsDao {

	void save(SendSmsEntity entity) throws Exception;

	void update(SendSmsEntity entity) throws Exception;

	void delete(SendSmsEntity entity) throws Exception;

	SendSmsEntity getSendSmsByIdIDA(String id_ida) throws Exception;

	List<SendSmsEntity> getAll() throws Exception;

	List<SendSmsEntity> getProcessed() throws Exception;

	List<SendSmsEntity> findNotVerified() throws Exception;
}
