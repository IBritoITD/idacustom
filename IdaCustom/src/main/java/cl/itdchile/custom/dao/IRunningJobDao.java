package cl.itdchile.custom.dao;

import java.util.List;

import cl.itdchile.custom.model.RunningJobEntity;

public interface IRunningJobDao {
	void save(RunningJobEntity entity) throws Exception;

	RunningJobEntity get(int idJobType, int idProcessType) throws Exception;

	void delete(RunningJobEntity entity) throws Exception;

	List<RunningJobEntity> getOldProcesses() throws Exception;

	List<RunningJobEntity> getProcesses() throws Exception;
}
