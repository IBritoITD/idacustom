package cl.itdchile.custom.dao;

import java.util.List;
import java.util.Map;

import cl.itdchile.custom.model.FileEntity;

public interface IFileDao {
	void saveFile(FileEntity file) throws Exception;

	List<FileEntity> getAll() throws Exception;

	void deleteFile(Long fileId) throws Exception;

	FileEntity getFile(Long fileId) throws Exception;

	List<FileEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams) throws Exception;
}
