
package cl.itdchile.custom.dao.imp.ida;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.ida.ISmsLinksDao;
import cl.itdchile.custom.model.ida.SmsLinkEntity;
import javassist.NotFoundException;

@Repository
@Transactional(transactionManager = "idaTransactionManager")
public class SmsLinksDaoImp implements Serializable, ISmsLinksDao {

	@Resource(name = "idaSessionFactory")
	private SessionFactory idaSessionFactory;

	@Override
	public SmsLinkEntity getSmsLink(String code) throws Exception {
		SmsLinkEntity result = null;
		try {
			CriteriaBuilder cb = idaSessionFactory.getCurrentSession().getCriteriaBuilder();

			CriteriaQuery<SmsLinkEntity> cq = cb.createQuery(SmsLinkEntity.class);
			Root<SmsLinkEntity> cRoot = cq.from(SmsLinkEntity.class);
			cq.where(cb.like(cRoot.get("code"), code));
			cq.select(cRoot);
			if (idaSessionFactory.getCurrentSession().createQuery(cq).getResultList().size() > 0)
				result = idaSessionFactory.getCurrentSession().createQuery(cq).getSingleResult();

			if (result == null) {
				throw new NotFoundException("Registro con codigo " + code + " no encontrado");
			}
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new NotFoundException(e.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void save(SmsLinkEntity entity) throws Exception {
		try {

			idaSessionFactory.getCurrentSession().saveOrUpdate(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}

	}

}
