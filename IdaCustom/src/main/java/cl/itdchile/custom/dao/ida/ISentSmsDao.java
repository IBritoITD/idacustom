package cl.itdchile.custom.dao.ida;

import java.util.Date;
import java.util.List;

import cl.itdchile.custom.model.ida.SentSmsEntity;

public interface ISentSmsDao {

	public List<SentSmsEntity> getSentSmsAll(int client_id) throws Exception;
	public SentSmsEntity getSentSmsId(long id) throws Exception;
	public List<SentSmsEntity> getSentSmsFilter(int client_id,Date created_at_id, long id_sent_sms_id) throws Exception;


}
