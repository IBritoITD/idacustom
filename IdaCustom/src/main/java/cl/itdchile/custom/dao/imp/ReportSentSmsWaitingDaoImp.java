package cl.itdchile.custom.dao.imp;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IReportSentSmsWaitingDao;
import cl.itdchile.custom.model.normaliza.ReportSentSmsWaitingEntity;

@Repository
@Transactional
public class ReportSentSmsWaitingDaoImp implements Serializable, IReportSentSmsWaitingDao {

	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public List<ReportSentSmsWaitingEntity> getReportSentSmsWaiting() throws Exception {
		// TODO Auto-generated method stub
		List<ReportSentSmsWaitingEntity> result = null;
		try {

			Query namedQuery = sessionFactory.getCurrentSession().createNamedQuery("reportSentSmsWaiting",
					ReportSentSmsWaitingEntity.class);

			if (namedQuery.list().size() > 0)
				result = namedQuery.list();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void save(ReportSentSmsWaitingEntity entity) throws Exception {
		// TODO Auto-generated method stub
		try {

			sessionFactory.getCurrentSession().saveOrUpdate(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public void delete(ReportSentSmsWaitingEntity entity) throws Exception {
		// TODO Auto-generated method stub
		try {

			sessionFactory.getCurrentSession().delete(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
	}

}
