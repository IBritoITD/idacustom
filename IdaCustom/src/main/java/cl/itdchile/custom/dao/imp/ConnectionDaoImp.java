package cl.itdchile.custom.dao.imp;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.model.ConnectionEntity;

@Service
public class ConnectionDaoImp implements IConnectionDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveConnection(ConnectionEntity connection) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {
			tx = session.beginTransaction();
			session.saveOrUpdate(connection);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			tx.rollback();
			e.printStackTrace();
		}
	}

	@Override
	public List<ConnectionEntity> getAll() throws Exception {
		List<ConnectionEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ConnectionEntity> cq = cb.createQuery(ConnectionEntity.class);
			Root<ConnectionEntity> cRoot = cq.from(ConnectionEntity.class);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void deleteFile(int idConnection) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {
			tx = session.beginTransaction();
			ConnectionEntity con = (ConnectionEntity) session.load(ConnectionEntity.class, idConnection);
			if (con != null) {
				System.out.println("2: " + idConnection);
				session.delete(con);
			}
			tx.commit();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}

	}

	@Override
	public ConnectionEntity getConnection(int connId) throws Exception {
		ConnectionEntity result = null;
		try (Session session = sessionFactory.openSession()) {

			result = session.get(ConnectionEntity.class, connId);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public ConnectionEntity getConnectionFiltered(Long idClient, String name, int idType) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
