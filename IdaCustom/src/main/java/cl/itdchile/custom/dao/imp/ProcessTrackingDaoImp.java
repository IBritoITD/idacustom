package cl.itdchile.custom.dao.imp;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Repository;

import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.model.FileEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;

@Repository
public class ProcessTrackingDaoImp implements Serializable, IProcessTrackingDao {

	@Autowired
	protected SessionFactory sessionFactory;

	@Override
	public void save(ProcessTrackingEntity entity) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {
			tx = session.beginTransaction();
			entity.setCreated_at(new Date());
			entity.setUpdated_at(new Date());
			session.saveOrUpdate(entity);

			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public void update(ProcessTrackingEntity entity) throws Exception {

		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
			entity.setUpdated_at(new Date());
			session.update(entity);

			session.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public List<ProcessTrackingEntity> getAll() throws Exception {
		List<ProcessTrackingEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public List<ProcessTrackingEntity> getNotProcessed(List<Integer> list) throws Exception {
		List<ProcessTrackingEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate date = cb.lessThan(cRoot.get("created_at"),
					new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1)));

			Predicate processing = cb.equal(cRoot.get("processing"), true);
			Predicate finished = cb.equal(cRoot.get("finished"), false);

			Predicate process_type = cRoot.get("id_process_type").in(list);

			cq.where(date, processing, finished, process_type);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void delete(Long id) throws Exception {
		FileEntity file = null;
		try (Session session = sessionFactory.openSession()) {
			file = (FileEntity) session.load(FileEntity.class, id);
			if (null != file) {
				session.delete(file);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public ProcessTrackingEntity get(Long id) throws Exception {
		ProcessTrackingEntity file = null;
		try (Session session = sessionFactory.openSession()) {
			file = (ProcessTrackingEntity) session.load(ProcessTrackingEntity.class, id);

			if (null == file) {
				throw new ObjectRetrievalFailureException(ProcessTrackingEntity.class, id);
			}
		} catch (ObjectRetrievalFailureException e) {
			e.printStackTrace();
			throw new ObjectRetrievalFailureException(ProcessTrackingEntity.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return file;
	}

	@Override
	public ProcessTrackingEntity getByFile(String file_name) throws Exception {
		ProcessTrackingEntity result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate name = cb.like(cRoot.get("file_name"), file_name);
			Predicate processing = cb.equal(cRoot.get("processing"), true);
			Predicate finished = cb.equal(cRoot.get("finished"), false);

			cq.where(name, processing, finished);
			cq.select(cRoot);

			result = session.createQuery(cq).getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public ProcessTrackingEntity getNotProcess(List<Integer> list, boolean process) throws Exception {
		// TODO Auto-generated method stub
		ProcessTrackingEntity result = null;
		Calendar fecha = Calendar.getInstance();
		fecha.add(Calendar.HOUR, -2);
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate date = cb.lessThan(cRoot.get("created_at"), fecha.getTime());
			Predicate processing = cb.equal(cRoot.get("processing"), process);
			Predicate finished = cb.equal(cRoot.get("finished"), false);
			Predicate process_type = cRoot.get("id_process_type").in(list);

			cq.where(date, processing, finished, process_type);
			cq.select(cRoot);
			if (session.createQuery(cq).getResultList().size() > 0)
				result = session.createQuery(cq).getResultList().stream().findFirst().get();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public Long countProcessed(int idProcessType) throws Exception {
		Long result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate finished = cb.equal(cRoot.get("finished"), true);
			Predicate process_type = null;

			if (idProcessType != -1) {
				process_type = cRoot.get("id_process_type").in(idProcessType);
			}

			cq.where(finished, process_type);
			cq.select(cRoot);

			result = new Long(session.createQuery(cq).getResultList().size());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("Result: " + result);
		return result;
	}

	@Override
	public Long countNotProcessed(int idProcessType) throws Exception {

		Long result = null;
		Calendar fecha = Calendar.getInstance();
		fecha.add(Calendar.HOUR, -2);

		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate date = cb.lessThan(cRoot.get("created_at"), fecha.getTime());
			Predicate finished = cb.equal(cRoot.get("finished"), false);
			Predicate processed = cb.equal(cRoot.get("processed"), true);
			Predicate process_type = null;

			if (idProcessType != -1) {
				process_type = cRoot.get("id_process_type").in(idProcessType);
			}

			cq.where(finished, processed, process_type, date);
			cq.select(cRoot);

			result = new Long(session.createQuery(cq).getResultList().size());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;

	}

	@Override
	public Long countProcessing(int idProcessType) throws Exception {
		Long result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<ProcessTrackingEntity> cq = cb.createQuery(ProcessTrackingEntity.class);
			Root<ProcessTrackingEntity> cRoot = cq.from(ProcessTrackingEntity.class);

			Predicate finished = cb.equal(cRoot.get("finished"), false);
			Predicate processed = cb.equal(cRoot.get("processed"), true);
			Predicate process_type = null;

			if (idProcessType != -1) {
				process_type = cRoot.get("id_process_type").in(idProcessType);
			}

			cq.where(finished, processed, process_type);
			cq.select(cRoot);

			result = new Long(session.createQuery(cq).getResultList().size());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

}
