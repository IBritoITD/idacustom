package cl.itdchile.custom.dao;

import cl.itdchile.custom.model.ForwardSmsEntity;

public interface IForwardSmsManager {

	public void forwardSms(ForwardSmsEntity entity) throws Exception;

}
