package cl.itdchile.custom.dao;

import cl.itdchile.custom.model.normaliza.ReportSentSmsEntity;

public interface IReportSentSmsDao {

	public ReportSentSmsEntity getReportSentSms(int process_type_id) throws Exception;

	public void save(ReportSentSmsEntity entity) throws Exception;

	void delete(ReportSentSmsEntity entity) throws Exception;
}
