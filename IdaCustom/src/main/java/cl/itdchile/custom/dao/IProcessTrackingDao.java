package cl.itdchile.custom.dao;

import java.util.List;

import cl.itdchile.custom.model.ProcessTrackingEntity;

public interface IProcessTrackingDao {

	public void save(ProcessTrackingEntity entity) throws Exception;

	public void update(ProcessTrackingEntity entity) throws Exception;

	public List<ProcessTrackingEntity> getAll() throws Exception;

	public List<ProcessTrackingEntity> getNotProcessed(List<Integer> list) throws Exception;

	public ProcessTrackingEntity getNotProcess(List<Integer> list, boolean process) throws Exception;

	public void delete(Long id) throws Exception;

	public ProcessTrackingEntity get(Long id) throws Exception;

	ProcessTrackingEntity getByFile(String file_name) throws Exception;

	Long countProcessed(int idProcessType) throws Exception;

	Long countNotProcessed(int idProcessType) throws Exception;

	Long countProcessing(int idProcessType) throws Exception;

}
