package cl.itdchile.custom.dao.imp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.itdchile.custom.dao.ISendSmsDao;
import cl.itdchile.custom.model.SendSmsEntity;

@Repository
public class SendSmsDaoImp implements Serializable, ISendSmsDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(SendSmsEntity entity) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {

			tx = session.beginTransaction();
			entity.setCreated_at(new Date());
			entity.setUpdated_at(new Date());
			session.save(entity);
			session.getTransaction().commit();

		} catch (Exception e) {
			// TODO: handle exception
			if (tx != null)
				tx.rollback();
			e.printStackTrace();

			throw new Exception();
		}

	}

	@Override
	public void update(SendSmsEntity entity) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {

			tx = session.beginTransaction();
			entity.setUpdated_at(new Date());
			session.update(entity);

			session.getTransaction().commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public SendSmsEntity getSendSmsByIdIDA(String id_ida) throws Exception {
		SendSmsEntity result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<SendSmsEntity> cq = cb.createQuery(SendSmsEntity.class);
			Root<SendSmsEntity> cRoot = cq.from(SendSmsEntity.class);
			cq.where(cb.equal(cRoot.get("id_ida"), id_ida));
			cq.select(cRoot);

			result = session.createQuery(cq).getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public List<SendSmsEntity> getAll() throws Exception {
		List<SendSmsEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<SendSmsEntity> cq = cb.createQuery(SendSmsEntity.class);
			Root<SendSmsEntity> cRoot = cq.from(SendSmsEntity.class);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public List<SendSmsEntity> getProcessed() throws Exception {
		List<SendSmsEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<SendSmsEntity> cq = cb.createQuery(SendSmsEntity.class);
			Root<SendSmsEntity> cRoot = cq.from(SendSmsEntity.class);
			cq.where(cb.notLike(cRoot.get("status"), "MESSAGE QUEUED"));
			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void delete(SendSmsEntity entity) throws Exception {
		Transaction tx = null;
		try (Session session = sessionFactory.openSession()) {

			tx = session.beginTransaction();
			session.delete(entity);
			session.getTransaction().commit();

		} catch (Exception e) {
			// TODO: handle exception
			if (tx != null)
				tx.rollback();
			e.printStackTrace();

			throw new Exception();
		}
	}

	@Override
	public List<SendSmsEntity> findNotVerified() throws Exception {
		List<SendSmsEntity> result = null;
		try (Session session = sessionFactory.openSession()) {

			CriteriaBuilder cb = session.getCriteriaBuilder();

			CriteriaQuery<SendSmsEntity> cq = cb.createQuery(SendSmsEntity.class);
			Root<SendSmsEntity> cRoot = cq.from(SendSmsEntity.class);

			Predicate status = cb.notLike(cRoot.get("status"), "MESSAGE QUEUED");
			Predicate date = cb.lessThan(cRoot.get("created_at"),
					new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(12)));

			cq.where(status, date);

			cq.select(cRoot);

			result = session.createQuery(cq).getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

}
