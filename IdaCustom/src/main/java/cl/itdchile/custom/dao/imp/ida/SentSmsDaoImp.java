package cl.itdchile.custom.dao.imp.ida;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.ida.ISentSmsDao;
import cl.itdchile.custom.model.ida.SentSmsEntity;

@Repository
@Transactional(transactionManager = "idaTransactionManager")
public class SentSmsDaoImp implements Serializable, ISentSmsDao {

	@Resource(name = "idaSessionFactory")
	private SessionFactory idaSessionFactory;

	@Override
	public List<SentSmsEntity> getSentSmsAll(int client_id) throws Exception {
		// TODO Auto-generated method stub
		List<SentSmsEntity> result = null;
		try {

			Query namedQuery = idaSessionFactory.getCurrentSession()
					.createNamedQuery("sentSmsClientAll", SentSmsEntity.class).setParameter(1, client_id);

			result = namedQuery.list();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public List<SentSmsEntity> getSentSmsFilter(int client_id, Date created_at_id, long id_sent_sms_id)
			throws Exception {
		// TODO Auto-generated method stub
		List<SentSmsEntity> result = null;
		try {

			Query namedQuery = idaSessionFactory.getCurrentSession()
					.createNamedQuery("sentSmsClientAllValidateIda", SentSmsEntity.class).setParameter(1, client_id)
					.setParameter(2, id_sent_sms_id).setParameter(3, created_at_id);

			result = namedQuery.getResultList();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public SentSmsEntity getSentSmsId(long id) throws Exception {
		// TODO Auto-generated method stub
		SentSmsEntity result = null;
		try {

			Query namedQuery = idaSessionFactory.getCurrentSession().createNamedQuery("sentSmsId", SentSmsEntity.class)
					.setParameter(1, id);
			if (namedQuery.getResultList().size() > 0)
				result = (SentSmsEntity) namedQuery.getResultList().stream().findFirst().get();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

}
