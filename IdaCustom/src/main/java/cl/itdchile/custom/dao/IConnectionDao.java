package cl.itdchile.custom.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cl.itdchile.custom.model.ConnectionEntity;

@Repository
public interface IConnectionDao {

	void saveConnection(ConnectionEntity connection) throws Exception;

	List<ConnectionEntity> getAll() throws Exception;

	void deleteFile(int idConnection) throws Exception;

	ConnectionEntity getConnection(int connId) throws Exception;

	ConnectionEntity getConnectionFiltered(Long idClient, String name, int idType) throws Exception;

}
