package cl.itdchile.custom;

public enum EnumJobType {
	GENERATE_INPUT(1), 
	GENERATE_OUTPUT(2), 
	NOT_PROCESSED(3);

	private int value;

	private EnumJobType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
