package cl.itdchile.custom.service.client;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.dao.client.IProcessesManager;
import cl.itdchile.custom.model.RunningJobEntity;

@Service
public class ProcessesManagerImp implements IProcessesManager {

	private static Logger log = Logger.getLogger(ProcessesManagerImp.class);

	@Autowired
	protected IRunningJobDao dao;

	@Override
	public List<RunningJobEntity> getOldProcesses() throws Exception {
		List<RunningJobEntity> result = null;
		log.info("Manager: Process Manager - GetProcesses");
		try {
			result = dao.getProcesses();

			if (result.size() > 0) {
				Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
				for (RunningJobEntity job : result) {
					if (setOfThread.stream()
							.anyMatch(t -> t.getName().equalsIgnoreCase(job.getThread_name()) && t.isAlive())) {
						job.setThreadActive(true);

					}
					int diferencia = (int) ((System.currentTimeMillis() - job.getCreated_at().getTime()) / 1000);

					int dias = 0;
					int horas = 0;
					int minutos = 0;
					if (diferencia > 86400) {
						dias = (int) Math.floor(diferencia / 86400);
						diferencia = diferencia - (dias * 86400);
					}
					if (diferencia > 3600) {
						horas = (int) Math.floor(diferencia / 3600);
						diferencia = diferencia - (horas * 3600);
					}
					if (diferencia > 60) {
						minutos = (int) Math.floor(diferencia / 60);
						diferencia = diferencia - (minutos * 60);
					}
					job.setExecutionTime(
							dias + " dias, " + horas + " horas, " + minutos + " minutos y " + diferencia + " segundos");

				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void stopProcess(RunningJobEntity entity) throws Exception {
		try {
			Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();

			if (setOfThread.stream()
					.anyMatch(t -> t.getName().equalsIgnoreCase(entity.getThread_name()) && t.isAlive())) {

				setOfThread.stream().filter(t -> t.getName().equalsIgnoreCase(entity.getThread_name()) && t.isAlive())
						.findFirst().get().interrupt();

				this.dao.delete(entity);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Override
	public void stopAllProcess(List<RunningJobEntity> entity) throws Exception {
		try {
			Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
			for (RunningJobEntity job : entity) {
				if (setOfThread.stream()
						.anyMatch(t -> t.getName().equalsIgnoreCase(job.getThread_name()) && t.isAlive())) {

					setOfThread.stream().filter(t -> t.getName().equalsIgnoreCase(job.getThread_name()) && t.isAlive())
							.findFirst().get().interrupt();

					this.dao.delete(job);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}

	}

}
