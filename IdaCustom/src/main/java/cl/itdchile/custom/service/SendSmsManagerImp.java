package cl.itdchile.custom.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.dao.ISendSmsDao;
import cl.itdchile.custom.dao.ISendSmsManager;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.SendSmsEntity;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;

@Service("sendSmsManager")
@Configurable
public class SendSmsManagerImp implements ISendSmsManager, Serializable {

	private static Logger log = Logger.getLogger(SendSmsManagerImp.class);

	@Autowired
	protected ISendSmsDao dao;

	@Autowired
	protected IEmailDao emailDao;

	@Autowired
	private Environment env;

	@Override
	public void registerSms(SendSmsEntity entity) throws Exception {
		EmailEntity email = null;
		try {

			// entity.setText(entity.getText().replace(" ", "%20"));
			StringBuilder parameters = new StringBuilder("?username=").append(entity.getUser_name())
					.append("&password=").append(URLEncoder.encode(entity.getPassword(), "UTF-8")).append("&phone=")
					.append(URLEncoder.encode(entity.getNumber(), "UTF-8")).append("&message=")
					.append(URLEncoder.encode(entity.getText(), "UTF-8")).append("&custom_id=")
					.append(entity.getRecsa_id());

			URL url = new URL(env.getProperty("api.ida") + parameters);

			email = this.emailDao.getEmail(1);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			int status = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}

			in.close();
			con.disconnect();
			String response = content.toString().replace("OK", "");

			entity.setId_ida(response);
			if (status == 200) {
				entity.setStatus("MESSAGE QUEUED");
			} else {
				entity.setStatus(response);
			}

			this.dao.save(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

			try {
				email.setFileName(entity.getRecsa_id().toString());
				email.setNotificationType(EnumNotificationType.ERROR.toString());
				email.setMessage(EnumNotificationType.ERROR.toString() + " de " + e.getMessage());
				email.setTitle("Recsa SMS ");
				if (email.isActive_error())
					MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC Error");
			} catch (Exception e2) {
				e2.printStackTrace();
			}

			throw new Exception();
		}

	}

	@Override
	public void forwardDlr(String status, String key, String date) throws Exception {
		EmailEntity email = email = this.emailDao.getEmail(EnumProcessType.RECSA.getId());
		SendSmsEntity send = null;
		try {

			String resp_code;
			resp_code = "004";
			if (status == "CONFIRMED DELIVERY") {
				resp_code = "001";
			} else if (status == "UNDELIVERED" || status == "EXPIRED" || status == "REJECTED") {
				resp_code = "003";
			} else if (status == "SENT") {
				resp_code = "005";
			}

			send = this.dao.getSendSmsByIdIDA(key);
			send.setStatus(status);
			send.setUpdated_at(new Date());
			this.dao.update(send);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			try {
				email.setFileName("Recsa SMS");
				email.setNotificationType(EnumNotificationType.ERROR.toString());
				email.setMessage(EnumNotificationType.ERROR.toString() + " de " + e.getMessage());
				email.setTitle("Recsa SMS ");
				if (email.isActive_error())
					MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC Error");
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw new Exception();
		}

	}

	@Override
	public List<SendSmsEntity> getAll() throws Exception {
		List<SendSmsEntity> result = null;
		try {
			result = this.dao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	@Override
	public void getProcessed() throws Exception {
		HttpURLConnection con = null;
		EmailEntity email = this.emailDao.getEmail(EnumProcessType.RECSA.getId());
		try {

			List<SendSmsEntity> entity = this.dao.getProcessed();

			for (SendSmsEntity sms : entity) {

				String resp_code;
				resp_code = "004";
				if (sms.getStatus() == "CONFIRMED DELIVERY") {
					resp_code = "001";
				} else if (sms.getStatus() == "UNDELIVERED" || sms.getStatus() == "EXPIRED"
						|| sms.getStatus() == "REJECTED") {
					resp_code = "003";
				} else if (sms.getStatus() == "SENT") {
					resp_code = "005";
				}

				URL url = new URL(env.getProperty("api.recsa") + "?id_sms=" + sms.getRecsa_id().toString()
						+ "&resp_publi=" + resp_code);

				con = (HttpURLConnection) url.openConnection();

				con.setRequestMethod("GET");

				int statusCode = con.getResponseCode();
				if (statusCode == 200) {
					log.info("Message verified: " + sms.getRecsa_id());
					// this.dao.delete(sms);
				}

				con.disconnect();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			try {
				email.setFileName("Recsa SMS");
				email.setNotificationType(EnumNotificationType.ERROR.toString());
				email.setMessage(EnumNotificationType.ERROR.toString() + " de " + e.getMessage());
				email.setTitle("Recsa SMS ");
				if (email.isActive_error())
					MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC Error");
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw new Exception();
		} finally {
			if (con != null) {
				con.disconnect();
				con = null;
			}
		}

	}

	@Override
	public void getNotVerified() throws Exception {
		HttpURLConnection con = null;
		try {
			List<SendSmsEntity> entity = this.dao.findNotVerified();

			for (SendSmsEntity sms : entity) {

				String resp_code;
				resp_code = "005";

				URL url = new URL(env.getProperty("api.recsa") + "?id_sms=" + sms.getRecsa_id().toString()
						+ "&resp_publi=" + resp_code);

//				URL url = new URL("http://localhost:8083/idaCustom/SendSms/forwardSendSms2?id_sms="
//						+ sms.getRecsa_id().toString() + "&resp_publi=" + resp_code);

				con = (HttpURLConnection) url.openConnection();

				con.setRequestMethod("GET");

				int statusCode = con.getResponseCode();
				if (statusCode == 200) {
					log.info("Message verified: " + sms.getRecsa_id());
					// this.dao.delete(sms);
				}

				con.disconnect();
				sms.setStatus("SENT");
				this.dao.save(sms);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		} finally {
			if (con != null) {
				con.disconnect();
				con = null;
			}
		}

	}

}
