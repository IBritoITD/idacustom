package cl.itdchile.custom.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IAppManager;
import cl.itdchile.custom.dao.IFileDao;
import cl.itdchile.custom.model.FileEntity;

@Service("appManager")
@Configurable
public class AppManagerImpl implements IAppManager {
	private IFileDao fileDao;

	@Autowired
	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	@Transactional("defTransaction")
	public void saveFile(FileEntity file) {
		try {
			this.fileDao.saveFile(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Transactional("defTransaction")
	public List<FileEntity> getAll() {
		// TODO Auto-generated method stub
		List<FileEntity> result = new ArrayList<FileEntity>();
		try {
			result = this.fileDao.getAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@Transactional("defTransaction")
	public void deleteFile(Long fileId) {
		// TODO Auto-generated method stub
		try {
			this.fileDao.deleteFile(fileId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional("defTransaction")
	public FileEntity getFile(Long fileId) {
		// TODO Auto-generated method stub
		FileEntity result = new FileEntity();
		try {
			result = this.fileDao.getFile(fileId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Transactional("defTransaction")
	public List<FileEntity> findByNamedQuery(String queryName, Map<String, Object> queryParams) {
		List<FileEntity> result = new ArrayList<FileEntity>();
		try {
			result = this.fileDao.findByNamedQuery(queryName, queryParams);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}
