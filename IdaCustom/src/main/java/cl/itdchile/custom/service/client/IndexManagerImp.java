package cl.itdchile.custom.service.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.client.IIndexManager;
import cl.itdchile.custom.model.client.IndexFullEntity;

@Service
public class IndexManagerImp implements IIndexManager {

	@Autowired
	IProcessTrackingDao trackDao;

	@Override
	public IndexFullEntity getInitial() throws Exception {
		IndexFullEntity result = new IndexFullEntity();
		try {

			result.setAbertisProcessed(this.trackDao.countProcessed(EnumProcessType.ABERTIS.getId()));
			result.setAfianzaProcessed(this.trackDao.countProcessed(EnumProcessType.AFIANZA.getId()));
			result.setNormalizaProcessed(this.trackDao.countProcessed(EnumProcessType.NORMALIZA.getId()));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

}
