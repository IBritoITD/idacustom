package cl.itdchile.custom.service.ida;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.dao.ida.IUserDao;
import cl.itdchile.custom.model.ida.RoleEntity;
import cl.itdchile.custom.model.ida.UserEntity;

@Service
public class UserDetailsServiceImp implements AuthenticationProvider {

	@Autowired
	private IUserDao userDao;
	@Autowired
	private PasswordEncoder passwordEncoder;

	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<RoleEntity> roles) {

		return roles.stream().filter(role -> role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE))
				.map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}

	@Transactional(transactionManager = "idaTransactionManager", readOnly = true)
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();

		UserEntity user = userDao.loadUserByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}

		// You can get the password here
		String password = passwordEncoder.encode(authentication.getCredentials().toString());
		// Your custom authentication logic here
		if (username.equalsIgnoreCase(user.getUsername()) && password.equalsIgnoreCase(user.getPassword())) {
			// new Userpa
			UserDetails userDetails = (UserDetails) user;

			Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, null,
					mapRolesToAuthorities(user.getRoles()));
			SecurityContextHolder.getContext().setAuthentication(auth);
			// return new UsernamePasswordAuthenticationToken(username, password,
			// mapRolesToAuthorities(user.getRoles()));
			
			return auth;
		}
		return null;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
