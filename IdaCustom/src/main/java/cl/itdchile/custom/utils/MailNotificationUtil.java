package cl.itdchile.custom.utils;

import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

import cl.itdchile.custom.model.EmailEntity;

public class MailNotificationUtil {

	public static String mailStart(String archivo) throws Exception {

		String str = "<table style='font-family: Arial, sans-serif; border: 1px solid #5f84a6; font-size: 8.5pt; margin: auto;' cellspacing='0' cellpadding='0'>"
				+ "<tr  color: #000;'>" + "<td style='padding: 8px 15px;'> "
				+ "	<center> Notificaci&oacute;n archivo : <br> <b>" + archivo
				+ " <span style='color:blue'>Iniciado</span></b> </center>" + "</td>" + "</tr>" + "</table>";

		return str;

	}

	public static String mailStartProcesing(String cliente) throws Exception {

		String str = "<table style='font-family: Arial, sans-serif; border: 1px solid #5f84a6; font-size: 8.5pt; margin: auto;' cellspacing='0' cellpadding='0'>"
				+ "<tr  color: #000;'>" + "<td style='padding: 8px 15px;'> "
				+ "	<center> Notificaci&oacute;n archivo en proceso de creacion  <br> <b> Cliente " + cliente
				+ " <span style='color:blue'>Iniciado</span></b> </center>" + "</td>" + "</tr>" + "</table>";

		return str;

	}

	public static String mailEnd(String archivo) throws Exception {

		String str = "<table style='font-family: Arial, sans-serif; border: 1px solid #5f84a6; font-size: 8.5pt; margin: auto;' cellspacing='0' cellpadding='0'>"
				+ "<tr  color: #000 '>" + "<td style='padding: 8px 15px;'> "
				+ "	<center> Notificaci&oacute;n archivo : <br> <b>" + archivo
				+ " <span style='color:green'>Finalizado</span></b> </center>" + "</td>" + "</tr>" + "</table>";

		return str;

	}

	public static String mailError(String archivo, String error) throws Exception {

		String str = "<table style='font-family: Arial, sans-serif; border: 1px solid #5f84a6; font-size: 8.5pt; margin: auto;' cellspacing='0' cellpadding='0'>"
				+ "<tr  color: #000 '>" + "<td style='padding: 8px 15px;'> "
				+ "	<center> Notificaci&oacute;n : <br> <b>" + archivo
				+ " <span style='color:red'>Error</span></b> </center>" + "<br>" + error + "</td>" + "</tr>"
				+ "</table>";

		return str;

	}

	public static String mailHtml(EmailEntity mail) {

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String dateString = format.format(new Date());

		StringBuilder html = new StringBuilder(" <table style='width: 100%; border-collapse: collapse'> ")
				.append("<tbody>").append("<tr>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 0'>")
				.append("<table style='width: 100%; border-collapse: collapse'>").append("<tbody>").append("<tr>")
				.append("<td id='m_-3559479355100869877content' style='font: 14px/1.4285714 Arial, sans-serif; padding: 0 0 0 10px'>")
				.append("<table style='width: 100%; border-collapse: collapse'>").append("<tbody>").append("<tr>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 0; line-height: 1'>")
				.append("<span> <strong>" + mail.getNotificationType() + "</strong> </span> ").append("</td>")
				.append("</tr>")
				.append("<tr> <td style='font: 14px/1.4285714 Arial, sans-serif; padding: 5px 0 0; font-weight: bold; line-height: 1.2'> </td> </tr>")
				.append("<tr>").append(" <td style='font: 14px/1.4285714 Arial, sans-serif; padding: 10px 0 20px'>")
				.append("<table style='width: 100%; border-collapse: collapse'>").append("<tbody>").append("<tr>")
				.append("<th style='border-bottom: 1px solid #ccc; text-align: left; font-weight: bold; padding: 5px; width: 132px'>Proceso</th>")
				.append("<th style='border-bottom: 1px solid #ccc; text-align: left; font-weight: bold; padding: 5px; width: 100px'>"
						+ (mail.getProcessType().getId() != 3 ? "Archivo" : "Mensaje") + "</th>")
				.append("<th style='border-bottom: 1px solid #ccc; text-align: left; font-weight: bold; padding: 5px'>Mensaje</th>")
				.append("<th style='border-bottom: 1px solid #ccc; text-align: left; font-weight: bold; padding: 5px; width: 100px'>Fecha</th>")
				.append("</tr>").append("<tr>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 5px; border-bottom: 1px solid #ccc; line-height: 24px; color: #707070; width: 132px'>")
				.append(mail.getProcessType().getDescription() + "</td>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 5px; border-bottom: 1px solid #ccc; line-height: 24px; color: #707070'>")
				.append(mail.getFileName() + "</td>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 5px; border-bottom: 1px solid #ccc; line-height: 24px; color: #707070; width: 50px; font-family: Monaco, monospace; font-size: 12px'>")
				.append(mail.getMessage() + "</td>")
				.append("<td style='font: 14px/1.4285714 Arial, sans-serif; padding: 5px; border-bottom: 1px solid #ccc; line-height: 24px; color: #707070; width: 100px'>")
				.append("<div>" + dateString + "</div>").append("</td>")
				.append("</tr></tbody></table></td></tr></tbody></table>")
				.append("</tbody></table></td></tr></tbody></table>").append("</td></tr></tbody></table></td></tr>")
				.append("<tr>")

				.append(" </tr></tbody></table>");

		return html.toString();

	}

}