package cl.itdchile.custom.utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cl.itdchile.custom.model.EmailEntity;

public class MailUtil {

	protected Session mailSession;
	protected Properties mailProperties;
	protected MimeMessage mailMessage;

	public static void sendEmail(EmailEntity emailConfig, String html, String asunto) throws Exception {
		
		
		
		Properties props = System.getProperties();

		props.put("mail.smtp.host", emailConfig.getHost()); // El servidor SMTP de Google
		props.put("mail.smtp.user", emailConfig.getUser());
		props.put("mail.smtp.clave", emailConfig.getPass()); // La clave de la cuenta

		props.put("mail.smtp.auth", String.valueOf(emailConfig.isAuth())); // Usar autenticación mediante usuario y
																			// clave
		props.put("mail.smtp.starttls.enable", String.valueOf(emailConfig.isStarttls_enable())); // Para conectar de
																									// manera segura
																									// al
		// servidor SMTP
		props.put("mail.smtp.port", emailConfig.getPort()); // El puerto SMTP seguro de Google

		try {

			Session emailSession = Session.getDefaultInstance(props);

			MimeMessage message = new MimeMessage(emailSession);
			message.setFrom(new InternetAddress(emailConfig.getUser(), emailConfig.getTitle()));
			message.addRecipients(Message.RecipientType.TO, addRecipientsList(emailConfig.getEmail_to()));
			message.setContent(html, "text/html");
			message.setSubject(asunto);
			Transport transport = emailSession.getTransport("smtp");

			transport.connect(emailConfig.getHost().trim(), emailConfig.getUser().trim(), emailConfig.getPass().trim());
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static InternetAddress[] addRecipientsList(String mail) throws AddressException {
		String[] emails = mail.split(";");
		InternetAddress[] recipientAddress = new InternetAddress[emails.length];

		for (int i = 0; i < emails.length; i++) {
			System.out.println("-->   " + emails[i].trim());
			recipientAddress[i] = new InternetAddress(emails[i].trim());
		}

		return recipientAddress;

	}

}