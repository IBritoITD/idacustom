package cl.itdchile.custom.utils;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SOAPUtilManager {

	private static Logger log = Logger.getLogger(SOAPUtilManager.class);

	protected String fechahora;
	protected String sms;
	protected String celular;
	protected String myNamespaceURI;

	public SOAPUtilManager(String fechahora, String sms, String celular, String myNamespaceURI) {
		super();
		this.fechahora = fechahora;
		this.sms = sms;
		this.celular = celular;
		this.myNamespaceURI = myNamespaceURI;
	}

	public SOAPUtilManager(String fechahora, String sms, String celular) {
		super();
		this.fechahora = fechahora;
		this.sms = sms;
		this.celular = celular;
	}

	private void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("ConsultaRut");
		SOAPElement rut = soapBodyElem.addChildElement("rut");
		rut.addTextNode(this.sms);
		SOAPElement celular = soapBodyElem.addChildElement("celular");
		celular.addTextNode(this.celular);
		SOAPElement fecha = soapBodyElem.addChildElement("fechahora");
		fecha.addTextNode(this.fechahora);
	}

	public String callSoapWebService(String soapEndpointUrl, String soapAction) throws Exception {
		String result = null;
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);
			// Print the SOAP Response
			System.out.println("Response SOAP Message:");
			soapResponse.writeTo(System.out);
			SOAPBody soapBody = soapResponse.getSOAPBody();
			NodeList nodes = soapBody.getElementsByTagName("return");

			// check if the node exists and get the value

			Node node = nodes.item(0);
			result = node != null ? node.getTextContent() : "";

			soapConnection.close();
		} catch (Exception e) {
			System.err.println(
					"\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
			e.printStackTrace();
			throw new Exception();
		}
		return result;
	}

	private SOAPMessage createSOAPRequest(String soapAction) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);

		soapMessage.saveChanges();

		/* Print the request message, just for debugging purposes */
		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;
	}

	public String getFechahora() {
		return fechahora;
	}

	public void setFechahora(String fechahora) {
		this.fechahora = fechahora;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

}
