package cl.itdchile.custom.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JasperReportUtil {
	private static Logger log = Logger.getLogger(JasperReportUtil.class);

	public JRBeanCollectionDataSource getDataSourceBean(List data) {
		return new JRBeanCollectionDataSource(data);
	}

	public void generatPDFFile(String urlJasper, HashMap<String, Object> map, Connection con, String urlFilePDF)
			throws FileNotFoundException, JRException {
		JasperPrint print = JasperFillManager.fillReport(new FileInputStream(urlJasper), map, con);
		JasperExportManager.exportReportToPdfFile(print, urlFilePDF);
	}

	public void generatPDFOutputStream(String urlJasper, ByteArrayOutputStream out, HashMap<String, Object> map)
			throws FileNotFoundException, JRException {

		System.out.println("generatPDFOutputStream " + urlJasper);
		JasperPrint print = JasperFillManager.fillReport(new FileInputStream(urlJasper), map, new JREmptyDataSource());
		JasperExportManager.exportReportToPdfStream(print, out);

	}

	public <T> void generatPDFOutputStreamBean(String urlJasper, ByteArrayOutputStream out, HashMap<String, Object> map,
			List<T> obj) throws FileNotFoundException, JRException {

		System.out.println("generatPDFOutputStream " + urlJasper);
		JasperPrint print = JasperFillManager.fillReport(new FileInputStream(urlJasper), map,
				new JRBeanCollectionDataSource(obj));
		JasperExportManager.exportReportToPdfStream(print, out);

	}

	public void generatPDFFile(String urlJasper, HashMap<String, Object> map, String urlFilePDF) {
		JasperPrint print;
		try {
			print = JasperFillManager.fillReport(new FileInputStream(urlJasper), map, new JREmptyDataSource());
			JasperExportManager.exportReportToPdfFile(print, urlFilePDF);
		} catch (FileNotFoundException | JRException e) {
			// TODO Auto-generated catch block
			log.debug("Error ", e);
		}

	}
}
