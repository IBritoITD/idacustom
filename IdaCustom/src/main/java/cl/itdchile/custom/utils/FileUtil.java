package cl.itdchile.custom.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

import com.itextpdf.text.pdf.PdfEncryptor;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

public class FileUtil {

	public static String getFileName(String fileName) {
		if (fileName.contains("/"))
			fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
		else {
			if (fileName.contains("\\"))
				fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
		}

		return fileName;
	}

	public static String readFile(String path, Charset encoding) 
			  throws IOException 
	{
	  byte[] encoded = Files.readAllBytes(Paths.get(path));
	  return new String(encoded, encoding);
	}
	
	public File InputStreamToFile(InputStream entrada, String name) {
		try {
			File file = new File(name);// Aqui le dan el nombre y/o con la ruta
										// del archivo salida
			OutputStream salida = new FileOutputStream(file);
			byte[] buf = new byte[1024];// Actualizado me olvide del 1024
			int len;
			while ((len = entrada.read(buf)) > 0) {
				salida.write(buf, 0, len);
			}
			salida.close();
			entrada.close();
			return file;
		} catch (IOException e) {
			System.out.println("Se produjo el error : " + e.toString());
		}
		return null;
	}

	public static boolean checkExists(String directory, String file) {
		File dir = new File(directory);
		if(dir.exists()) {
			File[] dir_contents = dir.listFiles();

			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().equals(file))
					return true;
			}
		}

		return false;
	}
	
	
	public static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
	
	public static File writeFileByteEncrypt(String FileName, byte[] data, String pass) throws Exception {

		File file = new File(FileName);
		System.out.println(file.getPath());
		File outputFile = new File(FileName);
		int permissions = PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_COPY;
		try {
			FileUtils.writeByteArrayToFile(file, data);
			PdfReader reader = new PdfReader(new FileInputStream(file));
			PdfEncryptor.encrypt(reader, new FileOutputStream(outputFile), pass.getBytes(), pass.getBytes(),
					permissions, false);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);

		}
		return outputFile;
	}
	public static InputStream getStream(String name) {
		try {
			File initialFile = new File(name);
	        InputStream targetStream = new FileInputStream(initialFile);
	        return targetStream;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
