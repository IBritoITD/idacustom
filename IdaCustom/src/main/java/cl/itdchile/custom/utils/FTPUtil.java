package cl.itdchile.custom.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import cl.itdchile.custom.Constants;

public class FTPUtil {
	private static Logger log = Logger.getLogger(FTPUtil.class);
	FTPClient client = new FTPClient();
	private String sFTPHost;
	private Integer sFTPPort;
	private String sFTPUser;
	private String sFTPPass;
	private String directory;
	private boolean login = false;

	public FTPUtil(String sFTPHost, Integer sFTPPort, String sFTPUser, String sFTPPass, String directory) {
		this.sFTPHost = sFTPHost;
		this.sFTPPort = sFTPPort;
		this.sFTPUser = sFTPUser;
		this.sFTPPass = sFTPPass;
		this.directory = directory;

		System.out.println("sFTPHost " + sFTPHost + " sFTPPort " + sFTPPort + " sFTPUser " + sFTPUser + " sFTPPass "
				+ sFTPPass + " directory +" + directory);
	}

	public boolean connectFtp() {
		try {
			client.connect(sFTPHost, sFTPPort);
			client.enterLocalPassiveMode();
			login = client.login(sFTPUser, sFTPPass);
			client.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
			client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
			client.makeDirectory(Constants.FILE_SEP_REMOTE);
		} catch (IOException ioe) {
			
			ioe.printStackTrace();
		}
		if (login) {
			log.info("Connection to " + sFTPHost + " ESTABLISHED");
			return true;
		} else {
			log.error("Error login in " + sFTPHost);
			return false;
		}
	}

	public String printDirectory() throws IOException {

		return client.printWorkingDirectory();

	}

	public FTPFile[] printFiles() throws IOException {
		return client.listFiles();

	}

	public boolean downloadFile(String srcFile, String destinoFile, String finalFileName) {
		boolean result = false;
		boolean retry = true;
		int retryCount = 0;

		try {
			while (retry) {

				if (!login && !client.isConnected()) {
					this.connectFtp();
				}
				if (login) {
					String finalPath = destinoFile + Constants.FILE_SEP + finalFileName;
					File folder = new File(destinoFile);

					System.out.println(destinoFile);
					folder.mkdirs();

					File downloadFile2 = new File(finalPath);

					OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
					InputStream inputStream = client.retrieveFileStream(srcFile);
					byte[] bytesArray = new byte[4096];
					int bytesRead = -1;
					while ((bytesRead = inputStream.read(bytesArray)) != -1) {
						outputStream2.write(bytesArray, 0, bytesRead);
					}
					result = client.completePendingCommand();
					outputStream2.close();
					inputStream.close();
					retry = false;
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						this.connectFtp();

						retryCount++;
						log.info("Retry dowloadFile file " + srcFile);
					} else {
						log.info("Skipping File " + srcFile);
						retry = false;
					}

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		} finally {
			this.disconnect();
		}

		return result;

	}

	public String searchFile(String prefix, String extension) throws IOException {
		boolean retry = true;
		int retryCount = 0;
		System.out.println("login searchFile " + prefix);

		
		String nombreArchivo = null;

		try {
			while (retry) {
				if (!login && !client.isConnected()) {
					this.connectFtp();
				}
				if (login) {
					client.makeDirectory(Constants.FILE_SEP_REMOTE);
					FTPFile[] files = printFiles();
					for (int i = 0; i < files.length; i++) {
						String fileName = files[i].getName();
						log.debug(fileName + " " + prefix);
						if (prefix != null && !prefix.isEmpty() && !prefix.equals("")) {
							if (extension.equalsIgnoreCase("*")) {
								if (fileName.startsWith(prefix)) {
									nombreArchivo = fileName;

									break;

								}
							} else {
								if (fileName.startsWith(prefix) && fileName.endsWith(extension)) {
									nombreArchivo = fileName;
									break;
								}
							}
						} else if (!fileName.equals(".") && !fileName.equals("..") && fileName.endsWith(extension)) {
							nombreArchivo = fileName;
							break;
						}
					}
					retry = false;
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						this.connectFtp();

						retryCount++;
						log.info("Retry search file ");
					} else {
						log.info("Skipping File ");
						retry = false;
					}

				}
			}
		} catch (Exception e) {
			log.error("Error searching file " + prefix + " in " + sFTPHost + " Cause: " + e.getMessage());
		} finally {
			this.disconnect();
		}
		return nombreArchivo;

	}

	public void removeFile(String file) {
		System.out.println("login removeFile " + login);
		boolean retry = true;
		int retryCount = 0;
		try {
			while (retry) {

				if (!login && !client.isConnected()) {
					this.connectFtp();
				}
				if (login) {
					log.info("Delete File " + file + " from ftp server");
					client.deleteFile(file);
					retry = false;
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						this.connectFtp();

						retryCount++;
						log.info("Retry remove file " + file);
					} else {
						log.info("Skipping File " + file);
						retry = false;
					}

				}
			}
		} catch (Exception e) {
			log.error("Error Delete File " + file + " from ftp server");
			e.printStackTrace();
		} finally {
			this.disconnect();
		}

	}

	public boolean uploadFile(File file, String path) {
		boolean retry = true;
		int retryCount = 0;

		boolean done = false;
		try {
			while (retry) {
				if (!login && !client.isConnected()) {
					this.connectFtp();
				}
				if (login) {
					
					System.out.println("UploadFile " + login + " file" + file.getName());
					client.enterLocalPassiveMode();
					InputStream inputStream = new FileInputStream(file);
					client.setFileType(FTPClient.BINARY_FILE_TYPE);
					client.changeWorkingDirectory(path);
					done = client.storeFile(file.getName(), inputStream);
					System.out.println(done);
					inputStream.close();
					
					retry = false;
					
					
					/*
					 client.makeDirectory(path);
					InputStream in = new FileInputStream(file);
					client.setFileType(FTPClient.BINARY_FILE_TYPE);
					 done = client.storeFile(file.getName() ,in);
					in.close();
					
					
					retry = false;
					 */
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						this.connectFtp();

						retryCount++;
						log.info("Retry uploadFile file " + file);
					} else {
						log.info("Skipping File " + file);
						retry = false;
					}

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex);

		} finally {
			this.disconnect();
		}
		return done;
	}

	public boolean moverArchivo(String file) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		boolean status = false;
		try {
			status = client.rename(file, dateFormat.format(date) + "/" + file.replace(" ", ""));
			if (!status) {
				removeFile(file);
			}

			return status;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;

		} finally {
			this.disconnect();
		}
	}

	public void createDirectory(String carpeta, String ruta) {

		try {
			boolean directo = false;
			if (!login && !client.isConnected()) {
				this.connectFtp();
			}
			System.out.println("creando directorio "+ruta);
			boolean result =client.changeWorkingDirectory(carpeta + Constants.FILE_SEP_REMOTE + ruta);
			
			System.out.println("creando directorio existe "+ruta+ " - "+result);
			
			if (!result){
				directo=client.makeDirectory(carpeta + Constants.FILE_SEP_REMOTE + ruta);
			}
			System.out.println("creando directorio "+ruta+ " - "+directo);
		} catch (Exception e) {
			// No such folder yet:


		} finally {
			this.disconnect();
		}
	}

	public boolean compareFile(String ftpFileName, String localFileName) {
		// PropertyConfigurator.configure("log4j.properties");
		try {
			InputStream fis = client.retrieveFileStream(ftpFileName);
			byte[] bytes = IOUtils.toByteArray(fis);
			String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(bytes);
			fis.close();

			FileInputStream file2 = new FileInputStream(new File(localFileName));
			byte[] bytes2 = IOUtils.toByteArray(file2);
			String md52 = org.apache.commons.codec.digest.DigestUtils.md5Hex(bytes2);
			file2.close();

			if (md5.equals(md52)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			this.disconnect();
		}

	}

	public void disconnect() {
		try {
			client.disconnect();
			login= false;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
