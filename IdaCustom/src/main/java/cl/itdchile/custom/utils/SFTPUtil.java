package cl.itdchile.custom.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import cl.itdchile.custom.Constants;

public class SFTPUtil {
	private static Logger log = Logger.getLogger(SFTPUtil.class);
	JSch jsch = new JSch();
	private String sFTPHost;
	private Integer sFTPPort;
	private String sFTPUser;
	private String sFTPPass;
	private String directory;
	ChannelSftp c = null;
	private boolean login = false;

	public SFTPUtil(String sFTPHost, Integer sFTPPort, String sFTPUser, String sFTPPass, String directory) {
		this.sFTPHost = sFTPHost;
		this.sFTPPort = sFTPPort;
		this.sFTPUser = sFTPUser;
		this.sFTPPass = sFTPPass;
		this.directory = directory;
	}

	public boolean connectSftp() throws SftpException {

		Session session = null;
		Channel channel = null;

		try {
			session = jsch.getSession(sFTPUser, sFTPHost, sFTPPort);
			session.setPassword(sFTPPass);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			c = (ChannelSftp) channel;

			log.info("Session " + c.getId() + " to " + sFTPHost + " ESTABLISHED");
		} catch (JSchException e) {

			log.error("Error login in " + sFTPHost + " Cause: " + e.getMessage());
			if (e.getMessage().equalsIgnoreCase("Auth fail")) {
				return false;
			} else {
				return true;
			}

		}
		return true;
	}

	public void printFiles() throws SftpException, InterruptedException {
		boolean retry = true;
		int retryCount = 0;
		while (retry) {
			if (c == null || !c.isConnected()) {
				retry = connectSftp();
			}
			if (c != null && c.isConnected()) {
				Vector vector = c.ls("/");
				log.info("Printing files of /");
				for (int i = 0; i < vector.size(); i++) {
					log.info(vector.get(i).toString());
				}
			}
			if (retry) {
				if (retryCount < 30) {
					Thread.sleep(1000);
					connectSftp();
					retryCount++;
					log.info("Retry search file ");
				} else {
					log.info("Skipping File ");
					retry = false;
				}

			}
		}
	}

	public String searchFile(String folder, String prefix, String extension) {
		Vector<Object> vector;
		String nombreArchivo = null;
		boolean retry = true;
		int retryCount = 0;

		try {
			while (retry) {
				if (c == null || !c.isConnected()) {
					retry = connectSftp();
				}
				if (c != null && c.isConnected()) {

					vector = c.ls(folder);

					for (Object f : vector) {

						String[] elements = f.toString().split(" ");
						if (prefix != null && !prefix.isEmpty() && !prefix.equals("")) {

							if (elements[elements.length - 1].startsWith(prefix)
									&& elements[elements.length - 1].endsWith(extension)) {

								nombreArchivo = elements[elements.length - 1];

								break;
							}
						} else if (!elements[elements.length - 1].equals(".")
								&& !elements[elements.length - 1].equals("..")
								&& elements[elements.length - 1].endsWith(extension)) {
							nombreArchivo = elements[elements.length - 1];
							break;
						}
					}
					retry = false;

				} else {
					log.error("Error: not connected ");
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(10000);
						connectSftp();
						retryCount++;
						log.info("Retry search file ");
					} else {
						log.info("Skipping File ");
						retry = false;
					}

				}
			}
		} catch (Exception e) {
			log.error("Error searching file " + prefix + " in " + sFTPHost + " Cause: " + e.getMessage());
			e.printStackTrace();
		} finally {
			closeConection();
		}

		return nombreArchivo;

	}

	public String searchFileFullExtension(String folder, String prefix, String extension, String extension1,
			String extension2) {
		Vector<Object> vector;
		String nombreArchivo = null;
		boolean retry = true;
		int retryCount = 0;

		try {
			while (retry) {

				if (c == null || !c.isConnected()) {
					connectSftp();
				}
				if (c != null && c.isConnected()) {

					vector = c.ls(folder);

					for (Object f : vector) {

						String[] elements = f.toString().split(" ");
						if (prefix != null && !prefix.isEmpty() && !prefix.equals("")) {

							if (elements[elements.length - 1].startsWith(prefix)
									&& (elements[elements.length - 1].endsWith(extension)
											|| elements[elements.length - 1].endsWith(extension1)
											|| elements[elements.length - 1].endsWith(extension2))) {

								nombreArchivo = elements[elements.length - 1];

								break;
							}
						} else if (!elements[elements.length - 1].equals(".")
								&& !elements[elements.length - 1].equals("..")
								&& (elements[elements.length - 1].endsWith(extension)
										|| elements[elements.length - 1].endsWith(extension1)
										|| elements[elements.length - 1].endsWith(extension2))) {
							nombreArchivo = elements[elements.length - 1];
							break;
						}
					}
					retry = false;

				} else {
					log.error("Error: not connected ");
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						connectSftp();
						retryCount++;
						log.info("Retry search file ");
					} else {
						log.info("Skipping File ");
						retry = false;
					}

				}
			}
		} catch (Exception e) {
			log.error("Error searching file " + prefix + " in " + sFTPHost + " Cause: " + e.getMessage());
			e.printStackTrace();
		} finally {
			closeConection();
		}

		return nombreArchivo;

	}

	public boolean downloadFile(String remoteFile, String path, String finalFileName) {
		boolean val = false;
		boolean retry = true;
		int retryCount = 0;
		try {
			while (retry) {
				if (c == null || !c.isConnected()) {
					retry = connectSftp();
				}
				if (c != null && c.isConnected()) {
					File folder = new File(path);
					folder.mkdirs();

					File downloadFile1 = new File(path + Constants.FILE_SEP + finalFileName);

					log.info("Verificando Archivo " + remoteFile);

					String src = remoteFile;
					String dst = "";
					if (downloadFile1.exists()) {
						downloadFile1.delete();
					}
					if (downloadFile1 != null) {
						dst = path + Constants.FILE_SEP + finalFileName;
						log.info("origen de descarga remota " + src);
						log.info("destino local " + dst);
						c.get(src, dst);
						val = true;
						retry = false;
					}

				}
				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						connectSftp();
						retryCount++;
						log.info("Retry dowloadFile file " + remoteFile);
					} else {
						log.info("Skipping File " + remoteFile);
						retry = false;
					}

				}
			}

		} catch (Exception e) {
			log.error("Error downloading file " + remoteFile + " in " + sFTPHost + " Cause: " + e.getMessage());
			e.printStackTrace();
		} finally {
			closeConection();
		}
		return val;

	}

	public boolean removeFile(String fileName, String repository) throws InterruptedException {
		boolean val = false;
		boolean retry = true;
		int retryCount = 0;
		if (fileName != null && !fileName.isEmpty()) {
			try {
				while (retry) {
					if (c == null || !c.isConnected()) {
						retry = connectSftp();
					}
					if (c != null && c.isConnected()) {
						log.debug("archivo a eliminar " + repository + Constants.FILE_SEP_REMOTE + fileName);
						c.rm(repository + Constants.FILE_SEP_REMOTE + fileName);
						val = true;
						retry = false;
					}
					if (retry) {
						if (retryCount < 30) {
							Thread.sleep(1000);
							connectSftp();
							retryCount++;
							log.info("Retry removeFile file " + fileName);
						} else {
							log.info("Skipping File " + fileName);
							retry = false;
						}

					}
				}
			} catch (SftpException e) {
				log.error("Error deleting file " + fileName + " in " + sFTPHost + " Cause: " + e.getMessage());
				e.printStackTrace();
			} finally {
				closeConection();
			}
		}

		return val;
	}

	public void closeConection() {
		Session session;
		if (c != null) {
			try {
				session = c.getSession();
				int sessionId = c.getId();
				c.exit();
				session.disconnect();
				log.info("Session " + sessionId + " to " + sFTPHost + " CLOSSED");
			} catch (JSchException e) {
				log.error("Error closing connection in " + sFTPHost + " Cause: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public boolean uploadFile(File f1, String path) throws InterruptedException {
		boolean val = false;
		boolean retry = true;
		int retryCount = 0;
		System.out.println("uploadFile " + f1.getPath() + " " + path);
		try {
			while (retry) {
				if (c == null || !c.isConnected()) {
					retry = connectSftp();
				}
				if (c != null && c.isConnected()) {
					c.cd(path);
					c.put(new FileInputStream(f1), f1.getName(), ChannelSftp.OVERWRITE);
					val = true;
					retry = false;
				}
				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						connectSftp();
						retryCount++;
						log.info("Retry uploadFile file " + f1);
					} else {
						log.info("Skipping File " + f1);
						retry = false;
					}

				}
			}
		} catch (SftpException | FileNotFoundException e) {
			log.error("Error uploading file " + f1.getName() + " in " + sFTPHost + " Cause: " + e.getMessage());
			e.printStackTrace();
		} finally {
			closeConection();
		}
		return val;
	}

	public void createDirectory(String carpeta, String ruta) throws InterruptedException {
		boolean retry = true;
		int retryCount = 0;
		try {
			while (retry) {
				if (c == null || !c.isConnected()) {
					retry = connectSftp();
				}
				if (c != null && c.isConnected()) {
					c.cd(carpeta + Constants.FILE_SEP_REMOTE + ruta);
					retry = false;
				}

				if (retry) {
					if (retryCount < 30) {
						Thread.sleep(1000);
						connectSftp();
						retryCount++;
						log.info("Retry createDirectory file " + carpeta + Constants.FILE_SEP_REMOTE + ruta);
					} else {
						log.info("Skipping File " + carpeta + Constants.FILE_SEP_REMOTE + ruta);
						retry = false;
					}

				}
			}
		} catch (SftpException e) {
			// No such folder yet:

			try {
				while (retry) {
					if (c == null || !c.isConnected()) {
						retry = connectSftp();
					}
					if (c != null && c.isConnected()) {

						c.mkdir(carpeta + Constants.FILE_SEP_REMOTE + ruta);
						retry = false;
					}
					if (retry) {
						if (retryCount < 30) {
							Thread.sleep(1000);
							connectSftp();
							retryCount++;
							log.info("Retry createDirectory file " + carpeta + Constants.FILE_SEP_REMOTE + ruta);
						} else {
							log.info("Skipping File " + carpeta + Constants.FILE_SEP_REMOTE + ruta);
							retry = false;
						}

					}
				}
			} catch (SftpException e1) {
				// TODO Auto-generated catch block

			}

		} finally {
			closeConection();
		}

	}
}
