package cl.itdchile.custom.utils;

public class FormatUtil {

	private FormatUtil() {

	}

	public static String formatMiles(String num) {
		if (num.length() == 4) {
			num = num.substring(0, 1) + "." + num.substring(1, num.length());
		} else if (num.length() == 5) {
			num = num.substring(0, 2) + "." + num.substring(2, num.length());
		} else if (num.length() == 6) {
			num = num.substring(0, 3) + "." + num.substring(3, num.length());
		} else if (num.length() == 7) {
			num = num.substring(0, 1) + "." + num.substring(1, 4) + "." + num.substring(4, num.length());
		} else if (num.length() == 8) {
			num = num.substring(0, 2) + "." + num.substring(1, 4) + "." + num.substring(4, num.length());
		}
		return num;
	}

	public static String mes2(String fecha) {
		if (fecha.length() < 8) {
			fecha = "0" + fecha;
		}
		int mes = Integer.parseInt(fecha.substring(4, 6));

		System.out.println("mes numero: " + mes);
		String monthString;

		switch (mes) {
		case 1:
			monthString = "Ene";
			break;
		case 2:
			monthString = "Feb";
			break;
		case 3:
			monthString = "Mar";
			break;
		case 4:
			monthString = "Abr";
			break;
		case 5:
			monthString = "May";
			break;
		case 6:
			monthString = "Jun";
			break;
		case 7:
			monthString = "Jul";
			break;
		case 8:
			monthString = "Ag";
			break;
		case 9:
			monthString = "Sep";
			break;
		case 10:
			monthString = "Oct";
			break;
		case 11:
			monthString = "Nov";
			break;
		case 12:
			monthString = "Dic";
			break;
		default:
			monthString = "";
		}
		return monthString;
	}

	public static String mes(String fecha) {
		if (fecha.length() < 8) {
			fecha = "0" + fecha;
		}
		System.out.println(fecha);
		int mes = Integer.parseInt(fecha.substring(2, 4));

		System.out.println("mes numero: " + mes);
		String monthString;

		switch (mes) {
		case 1:
			monthString = "Ene";
			break;
		case 2:
			monthString = "Feb";
			break;
		case 3:
			monthString = "Mar";
			break;
		case 4:
			monthString = "Abr";
			break;
		case 5:
			monthString = "May";
			break;
		case 6:
			monthString = "Jun";
			break;
		case 7:
			monthString = "Jul";
			break;
		case 8:
			monthString = "Ag";
			break;
		case 9:
			monthString = "Sep";
			break;
		case 10:
			monthString = "Oct";
			break;
		case 11:
			monthString = "Nov";
			break;
		case 12:
			monthString = "Dic";
			break;
		default:
			monthString = "";
		}
		return monthString;
	}

	public static String formatDecimal(String numero) {
		System.out.println("fecha: " + numero);
		if (numero.length() > 3) {
			numero = numero.substring(0, numero.length() - 3) + ","
					+ numero.substring(numero.length() - 3, numero.length());
		}
		return numero;
	}

	public static String formatFecha(String fecha) {
		if (fecha.length() > 4) {
			if (fecha.length() < 8) {
				fecha = "0" + fecha;
			}
			fecha = fecha.substring(0, 2) + "/" + fecha.substring(2, 4) + "/" + fecha.substring(4, fecha.length());
		} else {
			fecha = "";
		}
		return fecha;
	}
}
