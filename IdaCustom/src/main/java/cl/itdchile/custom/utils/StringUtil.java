package cl.itdchile.custom.utils;

import java.security.MessageDigest;

public class StringUtil {
	public static String encodePassword(final String password, final String algorithm) {
		final byte[] unencodedPassword = password.getBytes();

		MessageDigest md = null;

		try {
			// first create an instance, given the provider
			md = MessageDigest.getInstance(algorithm);
		} catch (final Exception e) {
			return password;
		}

		md.reset();

		// call the update method one or more times
		// (useful when you don't know the size of your data, eg. stream)
		md.update(unencodedPassword);

		// now calculate the hash
		final byte[] encodedPassword = md.digest();

		final StringBuffer buf = new StringBuffer();

		for (final byte element : encodedPassword) {
			if ((element & 0xff) < 0x10)
				buf.append("0");

			buf.append(Long.toString(element & 0xff, 16));
		}

		return buf.toString();
	}
}
