package cl.itdchile.custom.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.model.ConnectionEntity;

public class TriggerUtil implements Trigger {
	private static Logger log = Logger.getLogger(TriggerUtil.class);
	private IConnectionDao conDao;
	private int conectinId;
	private int jobType;

	public TriggerUtil(IConnectionDao conDao, int conectinId, int jobType) {
		super();
		this.conDao = conDao;
		this.conectinId = conectinId;
		this.jobType = jobType;

	}

	@Override
	public Date nextExecutionTime(TriggerContext triggerContext) {
		// TODO Auto-generated method stub
		Calendar nextExecutionTime = new GregorianCalendar();
		try {
			ConnectionEntity conn = conDao.getConnection(this.conectinId);
			log.info("la configuracion " + conn.getName() + " time input " + conn.getGenerate_input_time()
					+ " time output " + conn.getGenerate_output_time() + " time not processed "
					+ conn.getNot_processed_time());

			Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
			nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());

			int timer = 0;

			if (jobType == EnumJobType.GENERATE_INPUT.getValue()) {
				timer = (conn.getGenerate_input_time() == 0) ? 60000 : (int) conn.getGenerate_input_time();
			} else if (jobType == EnumJobType.GENERATE_OUTPUT.getValue()) {
				timer = (conn.getGenerate_output_time() == 0) ? 60000 : (int) conn.getGenerate_output_time();
			} else if (jobType == EnumJobType.NOT_PROCESSED.getValue()) {
				timer = (conn.getNot_processed_time() == 0) ? 60000 : (int) conn.getNot_processed_time();
			}

			nextExecutionTime.add(Calendar.MILLISECOND, timer);
			// you can get the value from wherever you want
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error ", e);
			e.printStackTrace();
		}

		return nextExecutionTime.getTime();
	}

}
