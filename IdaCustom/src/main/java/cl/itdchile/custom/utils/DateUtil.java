package cl.itdchile.custom.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.support.CronSequenceGenerator;

/**
 * Date Utility Class used to convert Strings to Dates and Timestamps
 * 
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a> to correct time
 *         pattern. Minutes should be mm not MM (MM is month).
 */
public class DateUtil {
	private static Logger log = Logger.getLogger(DateUtil.class);
	private static final String TIME_PATTERN = "HH:mm";

	/**
	 * Checkstyle rule: utility classes should not have public constructor
	 */
	private DateUtil() {

	}

	/**
	 * Return default datePattern (dd/MM/yyyy)
	 * 
	 * @return a string representing the date pattern on the UI
	 */
	public static String getDatePattern() {
		// Locale locale = LocaleContextHolder.getLocale();
		String defaultDatePattern = "dd/MM/yyyy HH:mm";
		/*
		 * try { defaultDatePattern = ResourceBundle.getBundle(Constants.BUNDLE_KEY,
		 * locale) .getString("date.format"); } catch (MissingResourceException mse) {
		 * defaultDatePattern = "dd/MM/yyyy HH:mm"; }
		 */

		return defaultDatePattern;
	}

	public static String getBirthdayPattern() {
		Locale locale = LocaleContextHolder.getLocale();
		String defaultDatePattern = "dd/MM/";
		/*
		 * try { defaultDatePattern = ResourceBundle.getBundle(Constants.BUNDLE_KEY,
		 * locale) .getString("date.format"); } catch (MissingResourceException mse) {
		 * defaultDatePattern = "dd/MM/yyyy HH:mm"; }
		 */

		return defaultDatePattern;
	}

	public static String getDateTimePattern() {
		return "dd/MM/yyyy HH:mm:ss.S";
	}

	/**
	 * This method attempts to convert an Oracle-formatted date in the form
	 * dd-MMM-yyyy to MM/dd/yyyy.
	 *
	 * @param aDate date from database as a string
	 * @return formatted string for the ui
	 */
	public static String getDate(Date aDate) {
		SimpleDateFormat df;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(getDatePattern());
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date/time in the format
	 * you specify on input
	 *
	 * @param aMask   the date pattern the string is in
	 * @param strDate a string representation of a date
	 * @return a converted Date object
	 * @see java.text.SimpleDateFormat
	 * @throws ParseException when String doesn't match the expected format
	 */
	public static Date convertStringToDate(String aMask, String strDate) throws ParseException {
		SimpleDateFormat df;
		Date date;
		df = new SimpleDateFormat(aMask);

		if (log.isDebugEnabled()) {
			log.debug("converting '" + strDate + "' to date with mask '" + aMask + "'");
		}

		try {
			date = df.parse(strDate);
		} catch (ParseException pe) {
			// log.error("ParseException: " + pe);
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());
		}

		return (date);
	}

	/**
	 * This method returns the current date time in the format: MM/dd/yyyy HH:MM a
	 *
	 * @param theTime the current time
	 * @return the current date/time
	 */
	public static String getTimeNow(Date theTime) {
		return getDateTime(TIME_PATTERN, theTime);
	}

	/**
	 * This method returns the current date in the format: MM/dd/yyyy
	 * 
	 * @return the current date
	 * @throws ParseException when String doesn't match the expected format
	 */
	public static Calendar getToday() throws ParseException {
		Date today = new Date();
		SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

		// This seems like quite a hack (date -> string -> date),
		// but it works ;-)
		String todayAsString = df.format(today);
		Calendar cal = new GregorianCalendar();
		cal.setTime(convertStringToDate(todayAsString));

		return cal;
	}

	/**
	 * This method generates a string representation of a date's date/time in the
	 * format you specify on input
	 *
	 * @param aMask the date pattern the string is in
	 * @param aDate a date object
	 * @return a formatted string representation of the date
	 * 
	 * @see java.text.SimpleDateFormat
	 */
	public static String getDateTime(String aMask, Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate == null) {
			log.error("aDate is null!");
		} else {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date based on the System
	 * Property 'dateFormat' in the format you specify on input
	 * 
	 * @param aDate A date to convert
	 * @return a string representation of the date
	 */
	public static String convertDateToString(Date aDate) {
		return getDateTime(getDatePattern(), aDate);
	}

	public static String convertDateToBirthdayString(Date aDate) {
		return getDateTime(getBirthdayPattern(), aDate);
	}

	/**
	 * This method converts a String to a date using the datePattern
	 * 
	 * @param strDate the date to convert (in format MM/dd/yyyy)
	 * @return a date object
	 * @throws ParseException when String doesn't match the expected format
	 */
	public static Date convertStringToDate(String strDate) throws ParseException {
		Date aDate = null;

		try {
			if (log.isDebugEnabled()) {
				log.debug("converting date with pattern: " + getDatePattern());
			}

			aDate = convertStringToDate(getDatePattern(), strDate);
		} catch (ParseException pe) {
			log.error("Could not convert '" + strDate + "' to a date, throwing exception");
			pe.printStackTrace();
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());
		}

		return aDate;
	}

	public static int minutesDiff(Date earlierDate, Date laterDate) {
		if (earlierDate == null || laterDate == null)
			return 0;

		return (int) ((laterDate.getTime() / 60000) - (earlierDate.getTime() / 60000));
	}

	public static int secondsDiff(Date earlierDate, Date laterDate) {
		if (earlierDate == null || laterDate == null)
			return 0;

		return (int) ((laterDate.getTime() / 1000) - (earlierDate.getTime() / 1000));
	}

	public static int miliSecondsDiff(Date earlierDate, Date laterDate) {
		if (earlierDate == null || laterDate == null)
			return 0;

		return (int) (laterDate.getTime() - earlierDate.getTime());
	}

	public static boolean isBetween(Calendar cal, int from, int to) {
		log.debug("Verifiying From: " + from + " To: " + to);
		int t = cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE);
		boolean isBetween = to > from && t >= from && t <= to || to < from && (t >= from || t <= to);

		return isBetween;
	}

	public static boolean isHoliday(Calendar cal, String holidays) {

		try {
			if (holidays != null && holidays.length() > 0) {
				String[] days = holidays.split(",");
				for (int i = 0; i < days.length; i++) {
					String actualDay = days[i];
					String[] dayPart = actualDay.split("-");

					Integer day = new Integer(dayPart[0]);
					Integer month = new Integer(dayPart[1]);

					if (cal.get(Calendar.MONTH) + 1 == month && cal.get(Calendar.DAY_OF_MONTH) == day) {
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("Error resolving holiday string: " + holidays + ". Message: " + e.getMessage());
		}

		return false;
	}

	public static Date getNextExecution(Date lastConn, String cronExpression, String timeZone) {
		CronSequenceGenerator generator = new CronSequenceGenerator(cronExpression, TimeZone.getTimeZone(timeZone));
		Date nextRunDate = generator.next(lastConn);
		return nextRunDate;
	}

	public static Date diffDates(Date fecha, int dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_YEAR, dias);
		return calendar.getTime();
	}

	public static String convertDateToString(Date date, String pattern) {
		return getDateTime(pattern, date);
	}
}
