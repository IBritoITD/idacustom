package cl.itdchile.custom.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.itdchile.custom.dao.IForwardSmsManager;
import cl.itdchile.custom.model.ForwardSmsEntity;

@RestController
public class ClentController {

	private static Logger log = Logger.getLogger(ClentController.class);

	@Autowired
	private IForwardSmsManager manager;

	@PostMapping(consumes = "application/json", value = "/api/clent/forward")
	public ResponseEntity<String> forwardResponse(@RequestBody ForwardSmsEntity entity) {
		try {
			log.info("Service: /clent/forward");

			this.manager.forwardSms(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

}
