package cl.itdchile.custom.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.itdchile.custom.dao.ISendSmsManager;
import cl.itdchile.custom.model.SendSmsEntity;

@RestController
public class SendSmsController {
	private static Logger log = Logger.getLogger(SendSmsController.class);
	@Autowired
	private ISendSmsManager sendSmsManager;

	@GetMapping("/forward-sms")
	public ResponseEntity<String> forwardSendSms(@RequestParam("password") String password,
			@RequestParam("id") long recsa_id, @RequestParam("text") String text,
			@RequestParam("to_number") String number) {
		try {
			log.info("Service : SendSms/forwardSendSms");
//			SendSmsEntity entity = new SendSmsEntity(text, number, password, recsa_id, "ibrito");
			SendSmsEntity entity = new SendSmsEntity(text, number, password, recsa_id, "recsaapi");
			this.sendSmsManager.registerSms(entity);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("false");
		}
		return ResponseEntity.status(HttpStatus.OK).body("true");
	}

	@PostMapping(path = "/forward-dlr")
	public @ResponseBody ResponseEntity<String> forwardDlr(HttpServletRequest request) {
		try {
			log.info("Service : SendSms/forwardDlr");

			this.sendSmsManager.forwardDlr(request.getParameter("status"), request.getParameter("message_key"),
					request.getParameter("date"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("false");
		}
		return ResponseEntity.status(HttpStatus.OK).body("true");
	}

}
