package cl.itdchile.custom.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cl.itdchile.custom.dao.ida.ISmsLinksDao;
import cl.itdchile.custom.model.ida.SmsLinkEntity;
import cl.itdchile.custom.utils.SmsLinksUtil;
import javassist.NotFoundException;

@RestController
public class SmsLinksController implements Serializable {
	private static final long serialVersionUID = -5525023967587689070L;

	private static Logger log = Logger.getLogger(SmsLinksController.class);

	@Autowired
	protected ISmsLinksDao dao;

	@GetMapping("/api/shortLink/{id}")
	public ResponseEntity<String> getLink(@PathVariable("id") String code, HttpServletRequest request,
			HttpServletResponse response) {
		SmsLinkEntity result = null;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		try {

			log.info("Service: /shortLink/" + code);
			log.info(request.getContextPath());
			if (code != null && code.length() <= 0) {
				throw new NotFoundException("Codigo nulo o vacio");
			}

			result = this.dao.getSmsLink(code);

			result.setBrowser(SmsLinksUtil.getClientBrowser(request));
			result.setDevice(SmsLinksUtil.getClientOS(request));
			result.setOs(SmsLinksUtil.getClientOS(request));
			result.setIp(SmsLinksUtil.resolveIp(request));

			result.setOpen_counter(result.getOpen_counter() + 1);
			result.setOpened_at(new Date());
			this.dao.save(result);

			response.setStatus(HttpStatus.OK.value());
			response.sendRedirect(result.getOriginal_link());
		} catch (NotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Fecha: " + dt.format(date) + "Error al abrir url desde: " + SmsLinksUtil.resolveIp(request)
					+ " a la url codigo: " + code);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			try {
				response.sendRedirect("Error/404.xhtml");
			} catch (Exception e2) {
				e.printStackTrace();
			}

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("errorPage");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Fecha: " + dt.format(date) + "Error al abrir url desde: " + SmsLinksUtil.resolveIp(request)
					+ " a la url codigo: " + code);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			try {
				response.sendRedirect("Error/500.xhtml");
			} catch (Exception e2) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("errorPage");
		}
		log.info("Fecha: " + dt.format(date) + " Redirect desde la ip: " + SmsLinksUtil.resolveIp(request) + " hacia "
				+ result.getOriginal_link() + " desde la url " + code);

		return null;
	}

	@GetMapping("/api/shortLink")
	public void getLink(HttpServletRequest request, HttpServletResponse response) {
		try {
			log.info("Service: /shortLink");
			response.sendRedirect(request.getContextPath() + "/views/Error/404.xhtml");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
