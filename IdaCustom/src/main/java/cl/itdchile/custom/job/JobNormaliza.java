package cl.itdchile.custom.job;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumConnection;
import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IReportSentSmsDao;
import cl.itdchile.custom.dao.IReportSentSmsWaitingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.dao.ida.ISentSmsDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.JobTypeEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.ProcessTypeEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.model.normaliza.ReportSentSmsEntity;
import cl.itdchile.custom.thread.normaliza.GenerateNormalizaCsvThread;
import cl.itdchile.custom.utils.FTPUtil;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;

@Component
public class JobNormaliza {
	private static Logger log = Logger.getLogger(JobNormaliza.class);

	@Autowired
	protected ISentSmsDao sentSmsIdadao;

	@Autowired
	protected IReportSentSmsDao reportSentSmsDao;
	@Autowired
	protected IReportSentSmsWaitingDao reportSentSmsWaitingDao;

	@Autowired
	protected IEmailDao emailDao;

	@Autowired(required = true)
	private IRunningJobDao jobDao;

	@Autowired(required = true)
	protected IConnectionDao conDao;

	@Autowired(required = true)
	protected IProcessTrackingDao trackDao;

	@Async
	public void generatingCSVJob() {
		log.info("iniciando generatingCSVJob normaliza ");
		ReportSentSmsEntity rptSent = new ReportSentSmsEntity();
		try {
			RunningJobEntity runningJob = null;
			runningJob = this.jobDao.get(EnumJobType.GENERATE_INPUT.getValue(), EnumProcessType.NORMALIZA.getId());
			EmailEntity email = this.emailDao.getEmail(1);
			ConnectionEntity conn = this.conDao.getConnection(EnumConnection.NORMALIZA.getConn());

			if (runningJob == null) {
				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());
//				runningJob.setId_process_type(EnumProcessType.NORMALIZA.getId());
//				runningJob.setId_job_type(EnumJobType.GENERATE_INPUT.getValue());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.GENERATE_INPUT.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.NORMALIZA.getId()));

				rptSent = this.reportSentSmsDao.getReportSentSms(EnumProcessType.NORMALIZA.getId());

				ProcessTrackingEntity tracking = new ProcessTrackingEntity();
				DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

				DateFormat dateFormatFileDate = new SimpleDateFormat("yyyy_MM");
				String fileName = null;
				Date date = new Date();
				boolean removed = false;
				FTPUtil ftp = null;
				String remotepath = "";

				// direccion local del computador

				String localPath = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP
						+ conn.getId_ida_client() + Constants.FILE_SEP + "custom";

				log.info("ruta local  " + localPath);

				// conectandome al sftp
				ftp = new FTPUtil(conn.getConn_ip(), conn.getConn_port(), conn.getConn_user(), conn.getConn_pass(),
						Constants.FILE_SEP_REMOTE + conn.getIn_repo());

				// haciendo una busqueda del archivo a evaluar

				fileName = ftp.searchFile(conn.getPrefix() + dateFormatFileDate.format(date), "csv");

				fileName = (fileName != null) ? fileName : "";
				tracking = new ProcessTrackingEntity("I", fileName, "File " + fileName + " Started", new Date(),
						new Date(), true, false, EnumProcessType.NORMALIZA.getId());
				this.trackDao.save(tracking);

				remotepath = Constants.FILE_SEP_REMOTE + fileName;

				log.info("capturando archivo remoto sftp  " + remotepath);

				if (fileName.length() > 0) {

					removed = ftp.downloadFile(remotepath, localPath, fileName);
					tracking.setPhase("I,D");
					this.trackDao.save(tracking);

					if (removed) {

						ftp.removeFile(fileName);
						tracking.setPhase("I,D,R");
						this.trackDao.save(tracking);
					}
				}
				String file = localPath + Constants.FILE_SEP + fileName;
				String justFileName = "";

				if (fileName.length() > 0) {
					justFileName = fileName.substring(0, fileName.lastIndexOf("."));
				} else {
					if (rptSent != null) {
						localPath = Constants.FTP_DIR + dateFormat.format(rptSent.getCreated_at()) + Constants.FILE_SEP
								+ conn.getId_ida_client() + Constants.FILE_SEP + "custom";

						fileName = rptSent.getFile();
						File archivo = new File(localPath + Constants.FILE_SEP + fileName);
						if (!archivo.exists()) {
							fileName = "";
							reportSentSmsDao.delete(rptSent);
						}
					}
				}

				tracking.setPhase("I,D,R,TS");
				this.trackDao.save(tracking);

				GenerateNormalizaCsvThread thread = new GenerateNormalizaCsvThread(fileName, ftp, conn, email, tracking,
						trackDao, runningJob, jobDao, reportSentSmsDao, sentSmsIdadao, reportSentSmsWaitingDao);
				thread.start();

			} else {
				log.info("generatingCSVJob ya hay un registro en proceso");
			}
			log.info("Finalizando generatingCSVJob Normaliza " + "  ");

		} catch (Exception e) {
			log.info("Error  ", e);
			try {
				reportSentSmsDao.delete(rptSent);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

}
