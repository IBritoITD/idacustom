package cl.itdchile.custom.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumConnection;
import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.JobTypeEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.ProcessTypeEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.thread.afianza.GenerateAfianzaPdfThread;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import cl.itdchile.custom.utils.SFTPUtil;

@Component
public class JobAfianza {
	private static Logger log = Logger.getLogger(JobAfianza.class);

	@Autowired(required = true)
	protected IEmailDao emailDao;
	@Autowired(required = true)
	protected IConnectionDao conDao;
	@Autowired(required = true)
	protected IProcessTrackingDao trackDao;
	@Autowired(required = true)
	private IRunningJobDao jobDao;
	
	

	@Async
	public void CreatePdfFileAfianzajob() {
		log.info("iniciando CreatePdfFileAfianzajob Afianza ");
		try {
			ProcessTrackingEntity tracking = new ProcessTrackingEntity();
			RunningJobEntity runningJob = null;
			runningJob = this.jobDao.get(EnumJobType.GENERATE_INPUT.getValue(), EnumProcessType.AFIANZA.getId());
			
			if (runningJob == null) {

				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());
//				runningJob.setId_process_type(EnumProcessType.AFIANZA.getId());
//				runningJob.setId_job_type(EnumJobType.GENERATE_INPUT.getValue());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.GENERATE_INPUT.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.AFIANZA.getId()));

				DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				String fileName = null;
				Date date = new Date();
				boolean removed = false;
				SFTPUtil sftp = null;
				String remotepath = "";
				String extension = null;
				EmailEntity email = this.emailDao.getEmail(1);
				ConnectionEntity conn = this.conDao.getConnection(EnumConnection.AFIANZA.getConn());

				// direccion local del computador

				String localPath = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP
						+ conn.getId_ida_client() + Constants.FILE_SEP + "custom";

				log.info("ruta local  " + localPath);

				// conectandome al sftp
				sftp = new SFTPUtil(conn.getConn_ip(), conn.getConn_port(), conn.getConn_user(), conn.getConn_pass(),
						Constants.FILE_SEP_REMOTE + conn.getIn_repo());

				// haciendo una busqueda del archivo a evaluar
				String searchExtension = "xls,xlsx,txt";

				fileName = sftp.searchFileFullExtension(conn.getIn_repo(), conn.getPrefix(), "xls", "txt", "xlsx");
				remotepath = conn.getIn_repo() + Constants.FILE_SEP_REMOTE + fileName;

				log.info("capturando archivo remoto sftp  " + remotepath);

				if (fileName != null) {

					tracking = new ProcessTrackingEntity("I", fileName, "File " + fileName + " Started", new Date(),
							new Date(), true, false, EnumProcessType.AFIANZA.getId());
					this.trackDao.save(tracking);
					removed = sftp.downloadFile(remotepath, localPath, fileName);
					tracking.setPhase("I,D");
					tracking.setProcessing(true);
					this.trackDao.save(tracking);

					if (removed) {

						sftp.removeFile(fileName, conn.getIn_repo());
						extension = FilenameUtils.getExtension(fileName);
						tracking.setPhase("I,D,R");
						tracking.setProcessing(true);
						this.trackDao.save(tracking);
					}
				}
				if (extension != null && !extension.isEmpty()) {

					String file = localPath + Constants.FILE_SEP + fileName;
					String justFileName = fileName.substring(0, fileName.lastIndexOf("."));
					String typeReport = "";

					email.setFileName(justFileName);
					email.setNotificationType(EnumNotificationType.INICIO.toString());
					email.setMessage(
							EnumNotificationType.INICIO.toString() + " de " + EnumProcessType.AFIANZA.toString());
					email.setTitle("Archivo: " + justFileName);

					if (email.isActive_start())
						MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC Error");

					tracking.setPhase("I,D,R,TS");
					tracking.setProcessing(true);
					this.trackDao.save(tracking);
					System.out.println(justFileName + "  " + justFileName.indexOf("CTCH") + " "
							+ justFileName.indexOf("VCLH") + " " + justFileName.indexOf("VCH"));
					if (justFileName.indexOf("VCLH") != -1) {
						typeReport = "VCLH";
						log.debug("es un archivo vclh (Aviso de Vcto. Cuota Leasing Habitacional) " + justFileName);
					}

					if (justFileName.indexOf("VCH") != -1) {
						typeReport = "VCH";
						log.debug("es un archivo vch (Aviso de Vcto. Credito Hipotecario) " + justFileName);
					}
					if (justFileName.indexOf("CTCH") != -1) {
						typeReport = "CTCH";
						log.debug("es un archivo ctch (comunicacion trimestral credito hipotecario) " + justFileName);
					}
					GenerateAfianzaPdfThread thread = new GenerateAfianzaPdfThread(file, justFileName, sftp, conn,
							typeReport, email, tracking, trackDao, runningJob, jobDao);
					thread.start();

				}
			} else {
				log.info("CreatePdfFileAfianzajob ya hay un registro en proceso");
			}
			log.info("Finalizando CreatePdfFileAfianzajob Afianza  ");

		} catch (Exception e) {
			log.info("Error  ", e);

			e.printStackTrace();
		}
	}

	@Async

	public void generateNotProcessedActive() {
		log.info("Start Executing generateNotProcessed Afianza");
		try {

			RunningJobEntity runningJob = null;
			runningJob = this.jobDao.get(EnumJobType.NOT_PROCESSED.getValue(), EnumProcessType.AFIANZA.getId());

			if (runningJob == null) {

				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());
//				runningJob.setId_process_type(EnumProcessType.AFIANZA.getId());
//				runningJob.setId_job_type(EnumJobType.NOT_PROCESSED.getValue());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.NOT_PROCESSED.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.AFIANZA.getId()));

				List<Integer> typeList = new ArrayList<Integer>();

				typeList.add(EnumProcessType.AFIANZA.getId());

				ProcessTrackingEntity tracking = this.trackDao.getNotProcess(typeList, true);

				if (tracking != null) {
					System.out.println("entre generateNotProcessedActive " + tracking.getId());
					ConnectionEntity conection = this.conDao.getConnection(EnumConnection.AFIANZA.getConn());
					SFTPUtil sftp = new SFTPUtil(conection.getConn_ip(), conection.getConn_port(),
							conection.getConn_user(), conection.getConn_pass(),
							Constants.FILE_SEP_REMOTE + conection.getIn_repo());

					DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

					String localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at())
							+ Constants.FILE_SEP + conection.getId_ida_client() + Constants.FILE_SEP + "custom";
					EmailEntity email = this.emailDao.getEmail(1);

					tracking.setProcessing(false);
					this.trackDao.save(tracking);
					String file = localPath + Constants.FILE_SEP + tracking.getFile_name();
					String justFileName = tracking.getFile_name().substring(0,
							tracking.getFile_name().lastIndexOf("."));
					String typeReport = "";

					if (justFileName.indexOf("VCLH") != -1) {
						typeReport = "VCLH";
						log.debug("es un archivo vclh (Aviso de Vcto. Cuota Leasing Habitacional) " + justFileName);
					}

					if (justFileName.indexOf("VCH") != -1) {
						typeReport = "VCH";
						log.debug("es un archivo vch (Aviso de Vcto. Credito Hipotecario) " + justFileName);
					}
					if (justFileName.indexOf("CTCH") != -1) {
						typeReport = "CTCH";
						log.debug("es un archivo ctch (comunicacion trimestral credito hipotecario) " + justFileName);
					}

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("I")) {
						tracking.setPhase("I,D,R,TS");
					}

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("D")) {
						tracking.setPhase("I,D,R,TS");
					}

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("R")) {
						tracking.setPhase("I,D,R,TS");
					}

					if (tracking.getPhase().indexOf("TS") > 0) {
						GenerateAfianzaPdfThread thread = new GenerateAfianzaPdfThread(file, justFileName, sftp,
								conection, typeReport, email, tracking, trackDao, runningJob, jobDao);
						thread.start();
					}
				} else {

					log.info("no hay ningun archivo con proceso incompleto");
				}
			} else {
				log.info("generateNotProcessed Afianza ya hay un registro en proceso");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("End Executing generateNotProcessed Afianza");
	}

}
