package cl.itdchile.custom.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumConnection;
import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.JobTypeEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.ProcessTypeEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.thread.seideman.GenerateSeidemanPdfThread;
import cl.itdchile.custom.utils.FTPUtil;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;

@Component
public class JobSeideman {
	private static Logger log = Logger.getLogger(JobSeideman.class);

	@Autowired(required = true)
	protected IEmailDao emailDao;
	@Autowired(required = true)
	protected IConnectionDao conDao;
	@Autowired
	protected IProcessTrackingDao trackDao;
	@Autowired(required = true)
	private IRunningJobDao jobDao;

	@Async
	public void CreatePdfFileSeidemanjob() {
		log.info("iniciando CreatePdfFileSeidemanjob Seideman ");
		try {
			RunningJobEntity runningJob = null;
			runningJob = this.jobDao.get(EnumJobType.GENERATE_INPUT.getValue(), EnumProcessType.SEIDEMAN.getId());

			if (runningJob == null) {
				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());
//				runningJob.setId_process_type(EnumProcessType.SEIDEMAN.getId());
//				runningJob.setId_job_type(EnumJobType.GENERATE_INPUT.getValue());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.GENERATE_INPUT.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.SEIDEMAN.getId()));

				ProcessTrackingEntity tracking = new ProcessTrackingEntity();
				DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				String fileName = null;
				Date date = new Date();
				boolean removed = false;
				FTPUtil ftp = null;
				String remotepath = "";
				String extension = null;
				EmailEntity email = this.emailDao.getEmail(1);
				ConnectionEntity conn = this.conDao.getConnection(EnumConnection.SEIDEMAN.getConn());

				// direccion local del computador

				String localPath = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP
						+ conn.getId_ida_client() + Constants.FILE_SEP + "custom";

				log.info("ruta local  " + localPath);

				// conectandome al sftp
				ftp = new FTPUtil(conn.getConn_ip(), conn.getConn_port(), conn.getConn_user(), conn.getConn_pass(),
						Constants.FILE_SEP_REMOTE + conn.getIn_repo());

				// haciendo una busqueda del archivo a evaluar

				ftp.createDirectory("", conn.getIn_repo());
				ftp.createDirectory("", conn.getOut_repo());
				fileName = ftp.searchFile(conn.getPrefix(), "csv");
				remotepath = Constants.FILE_SEP_REMOTE + fileName;

				log.info("capturando archivo remoto sftp  " + remotepath);

				if (fileName != null) {

					tracking = new ProcessTrackingEntity("I", fileName, "File " + fileName + " Started", new Date(),
							new Date(), true, false, EnumProcessType.SEIDEMAN.getId());
					this.trackDao.save(tracking);
					removed = ftp.downloadFile(remotepath, localPath, fileName);
					tracking.setPhase("I,D");
					this.trackDao.save(tracking);

					if (removed) {

						ftp.removeFile(fileName);
						extension = FilenameUtils.getExtension(fileName);
						tracking.setPhase("I,D,R");
						this.trackDao.save(tracking);
					}
				}
				if (extension != null && !extension.isEmpty()) {

					String file = localPath + Constants.FILE_SEP + fileName;
					String justFileName = fileName.substring(0, fileName.lastIndexOf("."));

					email.setFileName(fileName);
					email.setNotificationType(EnumNotificationType.INICIO.toString());
					email.setMessage(
							EnumNotificationType.INICIO.toString() + ":" + EnumProcessType.SEIDEMAN.toString());
					email.setTitle("Archivo: " + fileName);

					if (email.isActive_start())
						MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC ");

					tracking.setPhase("I,D,R,TS");
					this.trackDao.save(tracking);

					GenerateSeidemanPdfThread thread = new GenerateSeidemanPdfThread(file, justFileName, ftp, conn,
							email, tracking, trackDao, runningJob, jobDao);
					thread.start();

				}
			} else {
				log.info("CreatePdfFileSeidemanjob ya hay un registro en proceso");
			}
			log.info("Finalizando CreatePdfFileSeidemanjob Seideman  ");

		} catch (Exception e) {
			log.info("Error  ", e);

			e.printStackTrace();
		}
	}

	@Async
	// @Scheduled(cron = "0 0/10 * * * ?") // (cron= "0 0 12 1/1 * ? *") para cada
	// d�a a las 12
//	@Scheduled(fixedRate = 60000)
	public void generateNotProcessedActive() {
		log.info("Start Executing generateNotProcessed seideman");
		try {
			RunningJobEntity runningJob = null;
			runningJob = this.jobDao.get(EnumJobType.NOT_PROCESSED.getValue(), EnumProcessType.SEIDEMAN.getId());

			if (runningJob == null) {

				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());
//				runningJob.setId_process_type(EnumProcessType.SEIDEMAN.getId());
//				runningJob.setId_job_type(EnumJobType.NOT_PROCESSED.getValue());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.NOT_PROCESSED.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.SEIDEMAN.getId()));

				List<Integer> typeList = new ArrayList<Integer>();

				typeList.add(EnumProcessType.SEIDEMAN.getId());

				ProcessTrackingEntity tracking = this.trackDao.getNotProcess(typeList, true);

				if (tracking != null) {

					ConnectionEntity conection = this.conDao.getConnection(EnumConnection.SEIDEMAN.getConn());
					FTPUtil ftp = new FTPUtil(conection.getConn_ip(), conection.getConn_port(),
							conection.getConn_user(), conection.getConn_pass(),
							Constants.FILE_SEP_REMOTE + conection.getIn_repo());
					DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

					String localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at())
							+ Constants.FILE_SEP + conection.getId_ida_client() + Constants.FILE_SEP + "custom";
					EmailEntity email = this.emailDao.getEmail(1);

					tracking.setProcessing(false);
					this.trackDao.save(tracking);
					String file = localPath + Constants.FILE_SEP + tracking.getFile_name();
					String justFileName = tracking.getFile_name().substring(0,
							tracking.getFile_name().lastIndexOf("."));

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("I")) {
						tracking.setPhase("I,D,R,TS");
					}

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("D")) {
						tracking.setPhase("I,D,R,TS");
					}

					if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("R")) {
						tracking.setPhase("I,D,R,TS");
					}
					if (tracking.getPhase().indexOf("TS") > 0) {
						GenerateSeidemanPdfThread thread = new GenerateSeidemanPdfThread(file, justFileName, ftp,
								conection, email, tracking, trackDao, runningJob, jobDao);
						thread.start();
					}
				}
			} else {
				log.info("generateNotProcessedActive ya hay un registro en proceso");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("End Executing generateNotProcessed seideman");
	}

}
