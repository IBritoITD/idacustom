package cl.itdchile.custom.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumConnection;
import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IAppManager;
import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.dao.IEmailDao;
import cl.itdchile.custom.dao.IFileDao;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.FileEntity;
import cl.itdchile.custom.model.JobTypeEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.ProcessTypeEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.thread.abertis.GenerateReportThread;
import cl.itdchile.custom.thread.abertis.GenerateXlsxThread;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import cl.itdchile.custom.utils.SFTPUtil;

@Component
public class JobAbertis {

	private static Logger log = Logger.getLogger(JobAbertis.class);

	@Autowired
	private IAppManager appManager;
	@Autowired
	protected IConnectionDao conDao;
	@Autowired
	protected IProcessTrackingDao trackDao;
	@Autowired
	protected IEmailDao emailDao;
	@Autowired
	private IFileDao fileDao;
	@Autowired
	private IRunningJobDao jobDao;

	@Async
	public void createXlsxFileJob() {

		log.info("Start Executing createXlsxFileJob");
		ProcessTrackingEntity tracking = null;
		RunningJobEntity runningJob = null;

		try {
			runningJob = this.jobDao.get(EnumJobType.GENERATE_INPUT.getValue(), EnumProcessType.ABERTIS.getId());
			if (runningJob == null) {

				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.GENERATE_INPUT.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.ABERTIS.getId()));

				DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

				ConnectionEntity conection = this.conDao.getConnection(EnumConnection.ABERTIS.getConn());

				String fileName = null;

				Date date = new Date();
				String localPath = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP
						+ conection.getId_ida_client() + Constants.FILE_SEP + "custom";

				String extension = null;

				SFTPUtil sftp = new SFTPUtil(conection.getConn_ip(), conection.getConn_port(), conection.getConn_user(),
						conection.getConn_pass(), conection.getIn_repo());

				boolean removed = false;

				fileName = sftp.searchFile(conection.getIn_repo(), conection.getPrefix(), ".txt");
				EmailEntity email = this.emailDao.getEmailByType(EnumProcessType.ABERTIS.getId());
				if (fileName != null) {

					email.setFileName(fileName);
					email.setNotificationType(EnumNotificationType.INICIO.toString());
					email.setMessage(
							EnumNotificationType.INICIO.toString() + " de " + EnumProcessType.ABERTIS.toString());
					email.setTitle("Archivo: " + fileName);
					if (email.isActive_start())
						MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC");

					tracking = new ProcessTrackingEntity();
					tracking.setPhase("A");
					tracking.setComments(
							"File " + fileName.substring(0, fileName.lastIndexOf(".")) + " Found!. Starting Process");
					tracking.setFile_name(fileName.substring(0, fileName.lastIndexOf(".")));
					tracking.setProcessing(true);
					tracking.setFinished(false);
					tracking.setId_process_type(EnumProcessType.ABERTIS.getId());
					this.trackDao.save(tracking);

					removed = sftp.downloadFile(conection.getIn_repo() + Constants.FILE_SEP_REMOTE + fileName,
							localPath, fileName);

					if (removed) {

						sftp.removeFile(fileName, conection.getIn_repo());
						extension = FilenameUtils.getExtension(fileName);

						tracking.setPhase("B");
						tracking.setUpdated_at(new Date());
						tracking.setComments("File " + fileName + " removed from SFTP");
						this.trackDao.save(tracking);
					}
				}

				if (extension != null && !extension.isEmpty()) {

					tracking.setPhase("C");
					tracking.setUpdated_at(new Date());
					tracking.setComments("Starting Thread!");

					this.trackDao.save(tracking);

					String file = localPath + Constants.FILE_SEP + fileName;
					String justFileName = fileName.substring(0, fileName.lastIndexOf("."));

					GenerateXlsxThread thread = new GenerateXlsxThread(file, justFileName, conection.getId_ida_client(),
							date, appManager, sftp, conection.getOut_repo(), this.trackDao, tracking, email, runningJob,
							jobDao, conection);

					thread.start();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("End Executing createXlsxFileJob");
	}

	@Async
	public void generateNotProcessed() {
		log.info("Start Executing generateNotProcessed");
		RunningJobEntity runningJob = null;
		try {

			List<Integer> typeList = new ArrayList<Integer>();

			typeList.add(EnumProcessType.ABERTIS.getId());
			typeList.add(EnumProcessType.ABERTISOUT.getId());

			ProcessTrackingEntity tracking = this.trackDao.getNotProcess(typeList, true);

			if (tracking != null) {

				ConnectionEntity conection = this.conDao.getConnection(EnumConnection.ABERTIS.getConn());

				SFTPUtil sftp = new SFTPUtil(conection.getConn_ip(), conection.getConn_port(), conection.getConn_user(),
						conection.getConn_pass(), conection.getIn_repo());
				DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

				String localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
						+ conection.getId_ida_client() + Constants.FILE_SEP + "custom";

				boolean removed = false;
				String extension = ".txt";
				runningJob = this.jobDao.get(EnumJobType.NOT_PROCESSED.getValue(), EnumProcessType.ABERTIS.getId());
				if (runningJob == null) {

					runningJob = new RunningJobEntity();
					runningJob.setCreated_at(new Date());
					runningJob.setJobType(new JobTypeEntity(EnumJobType.NOT_PROCESSED.getValue()));
					runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.ABERTIS.getId()));

					if (tracking.getId_process_type() == 1) {
						if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("a")) {
							removed = sftp.downloadFile(conection.getIn_repo() + Constants.FILE_SEP_REMOTE
									+ tracking.getFile_name() + ".txt", localPath, tracking.getFile_name() + ".txt");

							if (removed) {

								sftp.removeFile(tracking.getFile_name(), conection.getIn_repo());
								extension = FilenameUtils.getExtension(tracking.getFile_name());

								tracking.setPhase("a,b");
								tracking.setUpdated_at(new Date());
								tracking.setComments("File " + tracking.getFile_name() + " removed from SFTP");
								this.trackDao.save(tracking);
							}
						}

						if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("b")
								|| tracking.getPhase().length() >= 2) {
							if (extension != null && !extension.isEmpty()) {

								tracking.setPhase("a,b,c");
								tracking.setUpdated_at(new Date());
								tracking.setComments("Starting Thread!");

								this.trackDao.save(tracking);
								EmailEntity email = this.emailDao.getEmail(1);

								String file = localPath + Constants.FILE_SEP + tracking.getFile_name() + extension;

								email.setFileName(file);
								email.setNotificationType(EnumNotificationType.INICIO.toString());
								email.setMessage(EnumNotificationType.INICIO.toString() + " de "
										+ EnumProcessType.ABERTIS.toString());
								email.setTitle("Archivo: " + file);

								MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC");

								GenerateXlsxThread thread = new GenerateXlsxThread(file, tracking.getFile_name(),
										conection.getId_ida_client(), tracking.getCreated_at(), appManager, sftp,
										conection.getOut_repo(), this.trackDao, tracking, email, runningJob, jobDao,
										conection);

								thread.start();
							}
						}
					}
				}

				runningJob = this.jobDao.get(EnumJobType.NOT_PROCESSED.getValue(), EnumProcessType.ABERTISOUT.getId());
				if (runningJob == null) {
					runningJob = new RunningJobEntity();
					runningJob.setCreated_at(new Date());

					runningJob.setJobType(new JobTypeEntity(EnumJobType.NOT_PROCESSED.getValue()));
					runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.ABERTISOUT.getId()));

					if (tracking.getId_process_type() == 4) {

						FileEntity f = fileDao.getFile(tracking.getId_file());
						f.setProcessing(true);
						f.setUpdateddAt(new Date());
						appManager.saveFile(f);

						if (tracking.getPhase().substring(tracking.getPhase().length() - 1).equalsIgnoreCase("a")
								|| tracking.getPhase().length() >= 1) {

							EmailEntity email = this.emailDao.getEmail(EnumProcessType.ABERTISOUT.getId());

							email.setFileName(f.getFileName());
							email.setNotificationType(EnumNotificationType.INICIO.toString());
							email.setMessage(EnumNotificationType.INICIO.toString() + " de "
									+ EnumProcessType.ABERTISOUT.toString());
							email.setTitle("Archivo: " + f.getFileName());
							if (email.isActive_start())
								MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC");
							GenerateReportThread reportThread = new GenerateReportThread(appManager, f,
									conection.getId_ida_client(), sftp, conection.getOut_repo(),
									conection.getServ_ida_user(), conection.getServ_ida_pass(), this.trackDao, tracking,
									email, runningJob, jobDao);
							reportThread.start();
						}

					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("End Executing generateNotProcessed");
	}

	@Async
	public void generateOutputReport() {
		log.info("Start Executing generateOutputReport");
		ProcessTrackingEntity tracking = null;
		RunningJobEntity runningJob = null;
		try {
			runningJob = this.jobDao.get(EnumJobType.GENERATE_OUTPUT.getValue(), EnumProcessType.ABERTIS.getId());
			if (runningJob == null) {
				runningJob = new RunningJobEntity();
				runningJob.setCreated_at(new Date());

				runningJob.setJobType(new JobTypeEntity(EnumJobType.GENERATE_OUTPUT.getValue()));
				runningJob.setProcessType(new ProcessTypeEntity(EnumProcessType.ABERTIS.getId()));

				ConnectionEntity conection = this.conDao.getConnection(EnumConnection.ABERTIS.getConn());

				SFTPUtil sftp = new SFTPUtil(conection.getConn_ip(), conection.getConn_port(), conection.getConn_user(),
						conection.getConn_pass(), conection.getIn_repo());

				List<FileEntity> files = appManager.findByNamedQuery("FileForReport", new HashMap<String, Object>());
				EmailEntity email = this.emailDao.getEmail(1);
				if (files.size() > 0) {
					FileEntity f = files.get(0);
					f.setProcessing(true);
					f.setUpdateddAt(new Date());
					appManager.saveFile(f);

					tracking = new ProcessTrackingEntity();
					tracking.setPhase("a");
					tracking.setId_file(f.getId());
					tracking.setComments("File " + f.getFileName() + " Found!. Starting Process");
					tracking.setFile_name(f.getFileName());
					tracking.setProcessing(true);
					tracking.setFinished(false);
					tracking.setId_process_type(EnumProcessType.ABERTISOUT.getId());
					this.trackDao.save(tracking);

					email.setFileName(f.getFileName());
					email.setNotificationType(EnumNotificationType.INICIO.toString());
					email.setMessage(
							EnumNotificationType.INICIO.toString() + " de " + EnumProcessType.ABERTISOUT.toString());
					email.setTitle("Archivo: " + f.getFileName());

					if (email.isActive_start())
						MailUtil.sendEmail(email, MailNotificationUtil.mailHtml(email), "Notificacion IC");

					GenerateReportThread reportThread = new GenerateReportThread(appManager, f,
							conection.getId_ida_client(), sftp, conection.getOut_repo(), conection.getServ_ida_user(),
							conection.getServ_ida_pass(), this.trackDao, tracking, email, runningJob, jobDao);

					reportThread.start();

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("End Executing generateOutputReport");
	}
}
