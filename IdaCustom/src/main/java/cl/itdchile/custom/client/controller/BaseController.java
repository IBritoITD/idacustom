package cl.itdchile.custom.client.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

@Controller
public abstract class BaseController {
	protected Authentication getLoggedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		return auth;
	}
}
