package cl.itdchile.custom.client.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cl.itdchile.custom.dao.ISendSmsManager;
import cl.itdchile.custom.model.SendSmsEntity;

@ManagedBean(name = "historicRecsaController")
@Controller
@ViewScoped
public class HistoricRecsaController {
	private static Logger log = Logger.getLogger(HistoricRecsaController.class);

	@Autowired
	protected ISendSmsManager manager;

	protected List<SendSmsEntity> sendSmsList;
	protected List<SendSmsEntity> filteredSendSmsList;

	@PostConstruct
	public void init() {
		try {
			log.info("Module: Historic Recsa Controller");

			this.getData();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void getData() {
		try {

			this.sendSmsList = this.manager.getAll();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void refresh() {

	}

	public List<SendSmsEntity> getSendSmsList() {
		return sendSmsList;
	}

	public void setSendSmsList(List<SendSmsEntity> sendSmsList) {
		this.sendSmsList = sendSmsList;
	}

	public List<SendSmsEntity> getFilteredSendSmsList() {
		return filteredSendSmsList;
	}

	public void setFilteredSendSmsList(List<SendSmsEntity> filteredSendSmsList) {
		this.filteredSendSmsList = filteredSendSmsList;
	}

}
