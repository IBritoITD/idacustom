package cl.itdchile.custom.client.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cl.itdchile.custom.dao.client.IIndexManager;
import cl.itdchile.custom.model.client.IndexFullEntity;

@ManagedBean(name = "indexController")
@Controller
@ViewScoped
public class IndexController implements Serializable {

	private static Logger log = Logger.getLogger(IndexController.class);

	protected IndexFullEntity initialData;

	@Autowired
	IIndexManager indexManager;

	@PostConstruct
	public void init() {
		try {
			log.info("Module: Index");

			this.getData();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void getData() {
		try {
			initialData = this.indexManager.getInitial();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public IndexFullEntity getInitialData() {
		return initialData;
	}

	public void setInitialData(IndexFullEntity initialData) {
		this.initialData = initialData;
	}

}
