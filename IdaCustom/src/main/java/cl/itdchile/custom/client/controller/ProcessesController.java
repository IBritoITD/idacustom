package cl.itdchile.custom.client.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cl.itdchile.custom.EnumJobType;
import cl.itdchile.custom.dao.client.IProcessesManager;
import cl.itdchile.custom.model.RunningJobEntity;

@ManagedBean(name = "processesController")
@Controller
@ViewScoped
public class ProcessesController implements Serializable {

	private static Logger log = Logger.getLogger(ProcessesController.class);

	private FacesContext context = FacesContext.getCurrentInstance();

	@Autowired
	protected IProcessesManager manager;

	protected List<RunningJobEntity> oldProcessList;
	protected List<RunningJobEntity> filteredoldProcessList;

	@PostConstruct
	public void init() {
		try {
			log.info("Module: Processes" + EnumJobType.GENERATE_INPUT);
			this.getData();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void getData() {
		try {
			this.oldProcessList = this.manager.getOldProcesses();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void stopProcess(RunningJobEntity entity) {
		try {
			this.manager.stopProcess(entity);
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_INFO, "Proceso Detenido", null));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public void stopAllProcess() {
		try {
			this.manager.stopAllProcess(this.oldProcessList);
			context.addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Todos los Procesos Detenidos", null));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public void refresh() {

	}

	public List<RunningJobEntity> getOldProcessList() {
		return oldProcessList;
	}

	public void setOldProcessList(List<RunningJobEntity> oldProcessList) {
		this.oldProcessList = oldProcessList;
	}

	public List<RunningJobEntity> getFilteredoldProcessList() {
		return filteredoldProcessList;
	}

	public void setFilteredoldProcessList(List<RunningJobEntity> filteredoldProcessList) {
		this.filteredoldProcessList = filteredoldProcessList;
	}

}
