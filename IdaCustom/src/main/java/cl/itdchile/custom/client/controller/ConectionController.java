package cl.itdchile.custom.client.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cl.itdchile.custom.dao.IConnectionDao;
import cl.itdchile.custom.model.ConnectionEntity;

@ManagedBean(name = "conectionController")
@Controller
@ViewScoped
public class ConectionController implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(ConectionController.class);

	protected List<ConnectionEntity> connList;
	protected List<ConnectionEntity> filteredConnList;
	protected ConnectionEntity conection;
	protected String dialogTitle = "Agregar Conexi�n";
	private PrimeFaces current = PrimeFaces.current();
	private FacesContext context = FacesContext.getCurrentInstance();

	@Autowired
	protected IConnectionDao connDao;

	@PostConstruct
	public void init() {
		try {
			this.connList = this.connDao.getAll();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	public void save() {
		try {
			log.info("---Save---");

			this.connDao.saveConnection(this.conection);
			this.connList = this.connDao.getAll();

			current.executeScript("PF('dlg1').hide();");

			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Conexi�n creada"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public void refresh() {

	}

	public void createNew() {
		try {
			this.conection = new ConnectionEntity();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public void updateConection(int id) {
		try {
			this.conection = this.connDao.getConnection(id);
			current.executeScript("PF('dlg1').show();");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public void deleteConnection(int id) {
		try {
			log.info("Delete Connection");

			this.connDao.deleteFile(id);
			this.connList = this.connDao.getAll();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito", "Conexi�n eliminada"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
		}
	}

	public String getDialogTitle() {
		return dialogTitle;
	}

	public void setDialogTitle(String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

	public List<ConnectionEntity> getConnList() {
		return connList;
	}

	public void setConnList(List<ConnectionEntity> connList) {
		this.connList = connList;
	}

	public List<ConnectionEntity> getFilteredConnList() {
		return filteredConnList;
	}

	public void setFilteredConnList(List<ConnectionEntity> filteredConnList) {
		this.filteredConnList = filteredConnList;
	}

	public ConnectionEntity getConection() {
		return conection;
	}

	public void setConection(ConnectionEntity conection) {
		this.conection = conection;
	}

}
