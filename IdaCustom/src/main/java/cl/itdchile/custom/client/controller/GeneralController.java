package cl.itdchile.custom.client.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import cl.itdchile.custom.dao.IAuthenticationDao;

@ManagedBean(name = "generalController")
@Controller
@ViewScoped
public class GeneralController implements Serializable {

	private static Logger log = Logger.getLogger(GeneralController.class);

	private String prueba = "";
	


	@PostConstruct
	public void init() {
		try {
			log.info("Module: general");
			log.debug("holaaaa");
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null) {
				prueba = auth.getName();
			} else {
				prueba = "---";
				System.out.println("contexto null");
			}
			
			
			
			
			
			

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public String getPrueba() {
		return prueba;
	}

	public void setPrueba(String prueba) {
		this.prueba = prueba;
	}

}
