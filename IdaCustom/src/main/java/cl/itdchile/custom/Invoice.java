package cl.itdchile.custom;

/**
 * @author Gerencia ITD
 *
 */
public class Invoice {
	public String nombre;
	public String numero;
	public String rut;
	public String monto;
	public String fechaEmision;
	public String linkFactura;
	public String fechaVencimiento;
	public String verAqui;
	
	public Invoice(String nombre,String numero,String rut,String monto,
			String fechaEmision,String linkFactura,String fechaVencimiento,String verAqui){
		this.nombre = nombre;
		this.numero = numero;
		this.rut = rut;
		this.monto = monto;
		this.fechaEmision = fechaEmision;
		this.linkFactura = linkFactura;
		this.fechaVencimiento = fechaVencimiento;
		this.verAqui = verAqui;
	}
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getLinkFactura() {
		return linkFactura;
	}
	public void setLinkFactura(String linkFactura) {
		this.linkFactura = linkFactura;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getVerAqui() {
		return verAqui;
	}
	public void setVerAqui(String verAqui) {
		this.verAqui = verAqui;
	}
}
