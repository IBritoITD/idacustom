package cl.itdchile.custom;

public enum EnumProcessType {
	ABERTIS(1), 
	AFIANZA(2), 
	RECSA(3), 
	ABERTISOUT(4), 
	SEIDEMAN(5),
	NORMALIZA(6); 
	

	private int id;

	private EnumProcessType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
