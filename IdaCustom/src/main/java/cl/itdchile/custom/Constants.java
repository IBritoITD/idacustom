package cl.itdchile.custom;

public class Constants {
	/**
	 * File separator from System properties
	 */

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static final String FILE_SEP = System.getProperty("file.separator");
	public static final String FILE_SEP_REMOTE = "/";

	public static final String BASE_DIR = (isWindows()) ? "C:" + FILE_SEP : FILE_SEP;

	public static final String FTP_DIR = BASE_DIR + "mnt" + FILE_SEP + "datastore01" + FILE_SEP + "ftpFiles" + FILE_SEP;

	public static final String TOMCAT_BASE_PATH = System.getProperty("catalina.base");

	public static final String ADMIN_ROLE = "ROLE_ADMIN";

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isUnix() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

	}

	public static boolean isSolaris() {

		return (OS.indexOf("sunos") >= 0);

	}

}
