package cl.itdchile.custom.thread.abertis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IAppManager;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.FileEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import cl.itdchile.custom.utils.SFTPUtil;
import javassist.NotFoundException;

public class GenerateReportThread extends Thread {
	private static Log log = LogFactory.getLog(GenerateReportThread.class);
	private IAppManager appManager = null;
	private FileEntity file = null;
	private int clientId;
	private SFTPUtil sftp = null;
	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	DateFormat timeFormat = new SimpleDateFormat("hhmmss");
	private String ftpOutFolder = null;
	EmailEntity conf_email = null;

	private File localFile = null;
	private File originalFile = null;
	private File generatedOut = null;
	private RunningJobEntity runningJob = null;
	HashMap<String, String[]> statusMap = null;

	ProcessTrackingEntity tracking = null;
	IProcessTrackingDao dao = null;

	String idaUser = null;
	String idaPass = null;

	private IRunningJobDao jobDao;

	public GenerateReportThread(IAppManager appManager, FileEntity file, int clientId, SFTPUtil sftp,
			String ftpOutFolder, String idaUser, String idaPass) {
		super();
		this.appManager = appManager;
		this.file = file;
		this.clientId = clientId;
		this.sftp = sftp;
		this.ftpOutFolder = ftpOutFolder;
		this.idaUser = idaUser;
		this.idaPass = idaPass;
	}

	public GenerateReportThread(IAppManager appManager, FileEntity file, int clientId, SFTPUtil sftp,
			String ftpOutFolder, String idaUser, String idaPass, IProcessTrackingDao dao,
			ProcessTrackingEntity tracking, EmailEntity conf_email, RunningJobEntity runningJob,
			IRunningJobDao jobDao) {
		super();
		this.appManager = appManager;
		this.file = file;
		this.clientId = clientId;
		this.sftp = sftp;
		this.ftpOutFolder = ftpOutFolder;
		this.idaUser = idaUser;
		this.idaPass = idaPass;
		this.dao = dao;
		this.tracking = tracking;
		this.conf_email = conf_email;
		this.runningJob = runningJob;
		this.jobDao = jobDao;
	}

	public void run() {
		Thread.currentThread().setName("GENERATE_REPORT_" + file.getFileName());

		String originalPath = Constants.FTP_DIR + dateFormat.format(file.getCreadtedAt()) + Constants.FILE_SEP
				+ clientId + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + file.getFileName() + ".txt";
		String localPath = Constants.FTP_DIR + dateFormat.format(file.getCreadtedAt()) + Constants.FILE_SEP + clientId
				+ Constants.FILE_SEP + file.getFileName() + "_Generated_Report.csv";
		String statusFileName = Constants.FTP_DIR + dateFormat.format(file.getCreadtedAt()) + Constants.FILE_SEP
				+ clientId + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + file.getFileName()
				+ timeFormat.format(new Date()) + ".txt";
		try {
			runningJob.setThread_name(Thread.currentThread().getName());

			jobDao.save(runningJob);

			statusMap = new HashMap<String, String[]>();
			if (this.tracking.getPhase().substring(this.tracking.getPhase().length() - 1).equalsIgnoreCase("a")) {
				this.verifyLocalFile(localPath, statusMap);
				log.info("File " + localFile + " exists!");
				tracking.setPhase("a,b");
				tracking.setUpdated_at(new Date());
				tracking.setComments("Local File " + localFile + " Verified");

				dao.save(tracking);
			}

			if (this.tracking.getPhase().substring(this.tracking.getPhase().length() - 1).equalsIgnoreCase("b")) {
				this.generateOutFile(originalPath, statusFileName);
				tracking.setPhase("a,b,c");
				tracking.setUpdated_at(new Date());
				tracking.setComments("File " + statusFileName + " Generated");

				dao.save(tracking);
			}

			if (this.tracking.getPhase().substring(this.tracking.getPhase().length() - 1).equalsIgnoreCase("c")) {
				sftp.uploadFile(generatedOut, ftpOutFolder);
				log.info(
						"Generated Out File" + statusFileName + " succesfully uploaded to " + ftpOutFolder + " folder");
				file.setStatus(true);
				appManager.saveFile(file);

				tracking.setPhase("a,b,c,z");
				tracking.setUpdated_at(new Date());
				tracking.setFinished(true);
				tracking.setComments(
						"Generated Out File" + statusFileName + " succesfully uploaded to " + ftpOutFolder + " folder");

				dao.save(tracking);

				conf_email.setFileName(file.getFileName());
				conf_email.setNotificationType(EnumNotificationType.FINAL.toString());
				conf_email.setMessage(
						EnumNotificationType.FINAL.toString() + " de " + EnumProcessType.ABERTISOUT.toString());
				conf_email.setTitle("Archivo: " + file.getFileName());

				if (conf_email.isActive_end())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC");

				MailUtil.sendEmail(conf_email, MailNotificationUtil.mailEnd(file.getFileName()), "Notificacion IC");

			}

			jobDao.delete(runningJob);
		} catch (NotFoundException e) {
			// TODO: handle exception
			log.error("Error uploading generatedOut file: " + statusFileName + " to: " + ftpOutFolder + ". Error: "
					+ e.getMessage());
			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file.getFileName());
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + file.getFileName());

				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");

				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (FileNotFoundException e) {

			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file.getFileName());
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + file.getFileName());

				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			log.error(e.getMessage());
			file.setProcessing(false);
			appManager.saveFile(file);
			e.printStackTrace();
		} catch (IOException e) {
			// TODO: handle exception
			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file.getFileName());
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + file.getFileName());

				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(" Error al generar reporte. El archivo se pasara a cola nuevamente ");
			file.setProcessing(false);
			appManager.saveFile(file);
			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file.getFileName());
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + file.getFileName());

				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	private void verifyLocalFile(String localPath, HashMap<String, String[]> statusMap) throws Exception {
		java.io.File localFile = new java.io.File(localPath);
		log.info("Verifying report file " + localFile);
		try (BufferedReader br = new BufferedReader(new FileReader(localFile))) {
			String line;

			while ((line = br.readLine()) != null) {
				String key = "";
				try {

					String[] values = line.split(";");
					String email = values[0];
					key = values[1];

					CloseableHttpClient httpClient = HttpClients.createDefault();
					String baseURL = "http://ida.itdchile.cl/publicMailing/mailStatus.html?";
					String params = "username=" + idaUser + "&password=" + idaPass + "&key=" + key;
					String fullUrl = baseURL + params;
					HttpGet httpGet = new HttpGet(fullUrl);
					HttpResponse httpResponse = httpClient.execute(httpGet);
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();

					JSONObject myObject = new JSONObject(json);
					String status = myObject.getString("status");
					String bounceCode = "";
					if (status.equals("BOUNCE")) {
						bounceCode = myObject.getString("bounce_code");
					}

					statusMap.put(email, new String[] { status, bounceCode });

				} catch (Exception e) {
					log.error("Error Generando estado para UUID: " + key);
					e.printStackTrace();
				}

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new FileNotFoundException("Local file " + localFile + " not found.");
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	private void generateOutFile(String originalPath, String statusFileName) throws Exception {

		generatedOut = new File(statusFileName);
		PrintWriter pw1 = new PrintWriter(generatedOut, "iso-8859-1");

		originalFile = new File(originalPath);

		try (BufferedReader br = new BufferedReader(new FileReader(originalFile))) {
			StringBuilder sb = new StringBuilder();
			String line;
			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				if (lineNumber == 0) {

					String newHeader = line.substring(0, 37) + "04";
					sb.append(newHeader);
					sb.append('\n');

				} else {

					String invoice = line.substring(0, 16);
					String stringEmails = line.substring(16, 96).trim().toLowerCase();
					String[] emails = { stringEmails };

					if (stringEmails.contains(";")) {
						emails = stringEmails.split(";");
					}
					boolean sent = false;
					String bounceDetail = "1.1.1";
					for (int i = 0; i < emails.length; i++) {
						String email = emails[i];
						String[] status = statusMap.get(email);

						if (status != null) {
							if (status[0].equals("SENT") || status[0].equals("DELIVERED") || status[0].equals("READ"))
								sent = true;
							if (status[0].equals("BOUNCE")) {
								bounceDetail = status[1];
								if (bounceDetail.length() > 5)
									bounceDetail = bounceDetail.substring(0, 5);
							}

							if (status[0].equals("UNSUBSCRIBED"))
								bounceDetail = "1.1.2";
							if (status[0].equals("INVALID EMAIL ADDRESS"))
								bounceDetail = "1.1.1";
							if (status[0].equals("DELETED"))
								bounceDetail = "1.1.1";
						}
					}

					if (sent) {
						sb.append(invoice + "S");
					} else {
						sb.append(invoice + "N" + bounceDetail);
					}
					sb.append('\n');

				}
				lineNumber++;
			}

			pw1.write(sb.toString());

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new NotFoundException("Error uploading generatedOut file: " + statusFileName + " to: " + ftpOutFolder
					+ ". Error: " + e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException();
		} catch (Exception e) {
			throw new Exception();
		} finally {
			pw1.close();
		}
	}

}
