package cl.itdchile.custom.thread.afianza;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.config.initializer.WebInitializer;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.model.afianza.CTCHReportEntity;
import cl.itdchile.custom.model.afianza.PdfOutEntity;
import cl.itdchile.custom.model.afianza.VCHandVCLHReportEntity;
import cl.itdchile.custom.utils.FileUtil;
import cl.itdchile.custom.utils.JasperReportUtil;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import cl.itdchile.custom.utils.SFTPUtil;
import net.sf.jasperreports.engine.JRException;

public class GenerateAfianzaPdfThread extends Thread {
	private static Logger log = Logger.getLogger(GenerateAfianzaPdfThread.class);
	String file = null;
	String fileName = null;
	SFTPUtil sftp = null;
	ConnectionEntity conn = null;
	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	DateFormat timeFormat = new SimpleDateFormat("hhmmss");
	String typeReport = null;
	EmailEntity conf_email = null;
	ProcessTrackingEntity tracking = null;
	IProcessTrackingDao daoTracking = null;
	RunningJobEntity runningJob = null;
	IRunningJobDao jobDao = null;

	public GenerateAfianzaPdfThread(String file, String fileName, SFTPUtil sftp, ConnectionEntity conn,
			String typeReport, EmailEntity conf_email, ProcessTrackingEntity tracking, IProcessTrackingDao daoTracking,
			RunningJobEntity runningJob, IRunningJobDao jobDao) {
		// TODO Auto-generated constructor stub
		this.file = file;
		this.fileName = fileName;
		this.sftp = sftp;
		this.conn = conn;
		this.typeReport = typeReport;
		this.conf_email = conf_email;
		this.tracking = tracking;
		this.daoTracking = daoTracking;
		this.runningJob = runningJob;
		this.jobDao = jobDao;
	}

	public void run() {
		log.debug("INICIANDO HILO GENERATE_PDF_AFIANZA_" + typeReport + " " + fileName);
		Thread.currentThread().setName("GENERATE_PDF_AFIANZA_" + typeReport + " " + fileName);
		List<VCHandVCLHReportEntity> readFileList = new ArrayList<VCHandVCLHReportEntity>();
		List<CTCHReportEntity> readFileListCTCH = new ArrayList<CTCHReportEntity>();
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();

		System.out.println("jop type " + runningJob.getJobType().getId());

		log.debug("leyendo archivo " + fileName);

		try {

			runningJob.setThread_name(Thread.currentThread().getName());
			jobDao.save(runningJob);
			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("TS")) {
				if (typeReport.equalsIgnoreCase("CTCH")) {
					readFileListCTCH = this.readFileTxt(file);
				} else {
					readFileList = this.readFileXls(file);
				}

			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("RF")) {
				this.generateFileOutTxt();
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 3).equalsIgnoreCase("GFO")) {
				result = this.generateFilePdfFinal(readFileListCTCH, readFileList);
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 3).equalsIgnoreCase("PDF")) {

				if (result.size() == 0)
					result = this.generateFilePdfFinal(readFileListCTCH, readFileList);

				this.generateCsvIda(result);

			}
			System.out.println("tracking " + tracking.getPhase());

			conf_email.setFileName(fileName);
			conf_email.setNotificationType(EnumNotificationType.FINAL.toString());
			conf_email.setMessage(EnumNotificationType.FINAL.toString() + " de " + EnumProcessType.AFIANZA.toString());
			conf_email.setTitle("Archivo: " + fileName);

			if (conf_email.isActive_end())
				MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");

			jobDao.delete(runningJob);
		} catch (Exception e) {
			// TODO: handle exception
			try {
				jobDao.delete(runningJob);
				tracking.setProcessing(true);
				daoTracking.save(tracking);
				conf_email.setFileName(fileName);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ":" + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);

				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	private List<PdfOutEntity> generateFilePdfFinal(List<CTCHReportEntity> readFileListCTCH,
			List<VCHandVCLHReportEntity> readFileList) throws FileNotFoundException, IOException, Exception {
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();
		if (typeReport.equalsIgnoreCase("CTCH")) {
			if (readFileListCTCH.size() == 0)
				readFileListCTCH = this.readFileTxt(file);

			result = this.generateFilePdfAllCTCH(readFileListCTCH);
		} else {
			if (readFileList.size() == 0)
				readFileList = this.readFileXls(file);

			result = this.generateFilePdfAll(readFileList);
		}

		return result;
	}

	private void generateCsvIda(List<PdfOutEntity> result) throws Exception

	{

		PrintWriter pw;
		String generatedCsvName = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
				+ conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + fileName
				+ "_Generated.csv";
		try {

			File generatedCsv = new File(generatedCsvName);
			pw = new PrintWriter(generatedCsv, "iso-8859-1");

			StringBuilder sb = new StringBuilder();

			generateHeader(sb);
			generateRow(sb, result);

			pw.write(sb.toString());
			pw.close();

			sftp.uploadFile(generatedCsv, conn.getIn_repo());

			log.info("Generated CSV " + generatedCsvName + " succesfully uploaded to /in folder");
			tracking.setPhase("I,D,R,TS,RF,GFO,PDF,FIN");
			tracking.setFinished(true);
			tracking.setComments("Finish File");
			tracking.setProcessing(false);
			daoTracking.save(tracking);
		} catch (Exception e) {
			log.error("Error uploading generatedOut file: " + generatedCsvName + " to: " + conn.getIn_repo()
					+ ". Error: " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private void generateRow(StringBuilder sb, List<PdfOutEntity> result) {

		for (PdfOutEntity ctch : result) {
			sb.append(ctch.getCorreo());
			sb.append(';');
			sb.append(ctch.getNombre_cliente());
			sb.append(';');
			sb.append(ctch.getRut());
			sb.append(';');
			sb.append(ctch.getArchivo());
			sb.append(';');
			sb.append(ctch.getMes());
			sb.append(';');
			sb.append('\n');
		}

	}

	private void generateHeader(StringBuilder sb) {
		sb.append("EMAIL");
		sb.append(';');
		sb.append("NOMBRE_CLIENTE");
		sb.append(';');
		sb.append("RUT");
		sb.append(';');
		sb.append("ARCHIVO");
		sb.append(';');
		sb.append("MES");
		sb.append(';');
		sb.append('\n');

	}

	private boolean validateEmail(String email) throws Exception {
		// string character check
		boolean result = true;
		if (!email.matches("[^@]+@([-\\p{Alnum}]+\\.)*\\p{Alnum}+"))
			result = false;

		return result;
	}

	private List<CTCHReportEntity> readFileTxt(String file) throws FileNotFoundException, IOException, Exception {
		List<CTCHReportEntity> result = new ArrayList<CTCHReportEntity>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"))) {
			String line;
			int linuNumber = 0;
			while ((line = br.readLine()) != null) {
				if (linuNumber != 0) {
					System.out.println(line);
					String[] campo = line.split(";");
					if (validateEmail(campo[46])) {

						CTCHReportEntity ctch = new CTCHReportEntity();
						ctch.setNombre_cliente(campo[0]);
						ctch.setFecha_corte(campo[1]);
						ctch.setSello_sernac(campo[2]);
						ctch.setSvs(campo[3]);
						ctch.setCaev(campo[4]);
						ctch.setPlazo_remanente(campo[5]);
						ctch.setNum_cuota(campo[6]);
						ctch.setSaldo_UF(campo[7]);
						ctch.setValor_dividendo_UF(campo[8]);
						ctch.setFecha_prox_pago(campo[9]);
						ctch.setCosto_total_prepago(campo[10]);
						ctch.setCaev_bis(campo[11]);
						ctch.setGarantias_vigentes(campo[12]);
						ctch.setCant_div_pagados(campo[13]);
						ctch.setCant_div_venc_no_pagados(campo[14]);
						ctch.setMonto_vendido_no_pagado(campo[15]);
						ctch.setMonto_total_atrasado(campo[16]);
						ctch.setSeg_des_costo_mensual(campo[17]);
						ctch.setSeg_des_costo_total(campo[18]);
						ctch.setSeg_des_cobertura(campo[19]);
						ctch.setSeg_des_comp_nombre(campo[20]);
						ctch.setSeg_in_costo_mensual(campo[21]);
						ctch.setSeg_in_costo_total(campo[22]);
						ctch.setSeg_in_cobertura(campo[23]);
						ctch.setSeg_in_comp_nombre(campo[24]);
						ctch.setSeg_ces_costo_mensual(campo[25]);
						ctch.setSeg_ces_costo_total(campo[26]);
						ctch.setSeg_ces_cobertura(campo[27]);
						ctch.setSeg_ces_comp_nombre(campo[28]);
						ctch.setTasa_anual(campo[29]);
						ctch.setFecha_cambio_tasa(campo[30]);
						ctch.setCargo_prepago(campo[31]);
						ctch.setPlazo_aviso_prepago(campo[32]);
						ctch.setInteres_monetario(campo[33]);
						ctch.setGasto_cobranza(campo[34]);
						ctch.setGasto_cobranza_2(campo[35]);
						ctch.setRut(campo[36]);
						ctch.setDir1(campo[38]);
						ctch.setDir2(campo[39]);
						ctch.setDir3(campo[40]);
						ctch.setDir4(campo[41]);
						ctch.setDir5(campo[42]);
						ctch.setCorreo(campo[46]);
						ctch.setMes(campo[47]);
						result.add(ctch);
					} else {
						log.error("Error en el correo " + campo[46]);
					}
				}
				linuNumber++;
			}

			tracking.setPhase("I,D,R,TS,RF");
			tracking.setComments("Read File txt");
			tracking.setProcessing(true);
			daoTracking.save(tracking);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new FileNotFoundException("Local file " + file + " not found. " + e);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException(e);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e);

		}
		return result;
	}

	private List<VCHandVCLHReportEntity> readFileXls(String file) throws FileNotFoundException, IOException, Exception {

		List<VCHandVCLHReportEntity> result = new ArrayList<VCHandVCLHReportEntity>();

		try {
			System.out.println(file);
			Workbook wb = WorkbookFactory.create(new FileInputStream(file));
			Sheet sheet = wb.getSheetAt(0); // acceder a la primera hoja

			int rows = 1;
			while (sheet.getRow(rows) != null && sheet.getRow(rows).getCell(2) != null
					&& sheet.getRow(rows).getCell(2).getStringCellValue() != "") {
				Row row = sheet.getRow(rows);
				VCHandVCLHReportEntity ro = new VCHandVCLHReportEntity();

				ro.setNombre_cliente(row.getCell(0).getStringCellValue());
				ro.setRut(row.getCell(1).getStringCellValue());
				ro.setSaldo_vigente(row.getCell(2).getStringCellValue());
				ro.setFecha_vencimiento(row.getCell(3).getStringCellValue());
				ro.setNro_cuotas(row.getCell(4).getStringCellValue());
				ro.setNro_contrato(row.getCell(5).getStringCellValue());
				ro.setDir1(row.getCell(6).getStringCellValue());
				ro.setDir2(row.getCell(7).getStringCellValue());
				ro.setDir3(row.getCell(8).getStringCellValue());
				ro.setDir4(row.getCell(9).getStringCellValue());
				ro.setAmortizacion(row.getCell(10).getStringCellValue());
				ro.setRenta_arrendamiento(row.getCell(11).getStringCellValue());
				ro.setSeg_des(row.getCell(12).getStringCellValue());
				ro.setSeg_in(row.getCell(13).getStringCellValue());
				ro.setOtros_cargos(row.getCell(14).getStringCellValue());
				ro.setTotal_pagar(row.getCell(15).getStringCellValue());
				ro.setPagar_hasta(row.getCell(16).getStringCellValue());
				ro.setClp1(row.getCell(17).getStringCellValue());
				ro.setClp2(row.getCell(18).getStringCellValue());
				ro.setClp3(row.getCell(19).getStringCellValue());
				ro.setClp4(row.getCell(20).getStringCellValue());
				ro.setClp5(row.getCell(21).getStringCellValue());
				ro.setClp6(row.getCell(22).getStringCellValue());
				ro.setClp7(row.getCell(23).getStringCellValue());
				ro.setClp8(row.getCell(24).getStringCellValue());
				ro.setClp9(row.getCell(25).getStringCellValue());
				ro.setClp10(row.getCell(26).getStringCellValue());
				ro.setClp11(row.getCell(27).getStringCellValue());
				ro.setClp12(row.getCell(28).getStringCellValue());
				ro.setFecha1(row.getCell(29).getStringCellValue());
				ro.setFecha2(row.getCell(30).getStringCellValue());
				ro.setFecha3(row.getCell(31).getStringCellValue());
				ro.setFecha4(row.getCell(32).getStringCellValue());
				ro.setFecha5(row.getCell(33).getStringCellValue());
				ro.setFecha6(row.getCell(34).getStringCellValue());
				ro.setFecha7(row.getCell(35).getStringCellValue());
				ro.setFecha8(row.getCell(36).getStringCellValue());
				ro.setFecha9(row.getCell(37).getStringCellValue());
				ro.setFecha10(row.getCell(38).getStringCellValue());
				ro.setFecha11(row.getCell(39).getStringCellValue());
				ro.setFecha12(row.getCell(40).getStringCellValue());
				ro.setSeg_ces(row.getCell(41).getStringCellValue());

				ro.setCorreo(row.getCell(42).getStringCellValue());

				ro.setMes(row.getCell(43).getStringCellValue());

				result.add(ro);
				rows++;
			}
			tracking.setPhase("I,D,R,TS,RF");
			tracking.setComments("Read File Xls");
			tracking.setProcessing(true);
			daoTracking.save(tracking);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return result;
	}

	private void generateFileOutTxt() throws Exception {
		String generatedOutFileName = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at())
				+ Constants.FILE_SEP + conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP
				+ fileName + timeFormat.format(new Date()) + ".txt";
		PrintWriter pw1;

		try {

			File generatedOut = new File(generatedOutFileName);
			pw1 = new PrintWriter(generatedOut, "iso-8859-1");

			StringBuilder sb = new StringBuilder();
			sb.append(fileName + "        200 OK");
			pw1.write(sb.toString());
			pw1.close();

			sftp.uploadFile(generatedOut, conn.getOut_repo());
			log.info("Generated Out File" + generatedOutFileName + " succesfully uploaded to " + conn.getOut_repo()
					+ " folder");

			tracking.setPhase("I,D,R,TS,RF,GFO");
			tracking.setComments("Generated Out File" + generatedOutFileName + " succesfully uploaded to "
					+ conn.getOut_repo() + " folder");
			tracking.setProcessing(true);
			daoTracking.save(tracking);

		} catch (Exception e) {
			log.error("Error uploading generatedOut file: " + generatedOutFileName + " to: " + conn.getOut_repo()
					+ ". Error: " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	private <T> void generateFilePdf(String ruta, String pass, HashMap<String, Object> map, String carpeta,
			String ruta_remota, List<T> objList) throws FileNotFoundException, JRException, IOException, Exception {
		byte[] data = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			String report = "";
			if (typeReport.equalsIgnoreCase("VCH")) {
				report = "vch.jasper";
			}

			if (typeReport.equalsIgnoreCase("VCLH")) {
				report = "vclh.jasper";
			}

			if (typeReport.equalsIgnoreCase("CTCH")) {
				report = "ctch.jasper";
			}

			new JasperReportUtil().generatPDFOutputStreamBean(carpeta + Constants.FILE_SEP + report, out, map, objList);
			data = out.toByteArray();
			out.close();

			String localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
					+ conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + typeReport
					+ Constants.FILE_SEP + fileName;
			File folder = new File(localPath);
			folder.mkdirs();

			File generatedPdf = FileUtil.writeFileByteEncrypt(localPath + Constants.FILE_SEP + ruta, data, pass);

			boolean done = sftp.uploadFile(generatedPdf, conn.getIn_repo() + Constants.FILE_SEP_REMOTE + ruta_remota);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new FileNotFoundException(
					"Local file " + carpeta + Constants.FILE_SEP + "ctch.jasper" + " not found. " + e);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JRException(e);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException(e);
		}

	}

	private List<PdfOutEntity> generateFilePdfAllCTCH(List<CTCHReportEntity> entity) throws Exception {

		String carpeta = WebInitializer.REAL_PATH_REPORT + "afianza";
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();
		String date_file = dateFormat.format(tracking.getCreated_at());
		String prex_file = "CTCH";
		sftp.createDirectory(conn.getIn_repo(), date_file);
		sftp.createDirectory(conn.getIn_repo() + Constants.FILE_SEP_REMOTE + date_file, prex_file);
		sftp.createDirectory(
				conn.getIn_repo() + Constants.FILE_SEP_REMOTE + date_file + Constants.FILE_SEP_REMOTE + prex_file,
				fileName);
		String ruta_remota = date_file + Constants.FILE_SEP_REMOTE + prex_file + Constants.FILE_SEP_REMOTE + fileName;

		try {
			for (CTCHReportEntity ctch : entity) {
				String[] rut = ctch.getRut().replace(".", "").split("-");
				String pass = rut[0].substring(rut[0].length() - 4);
				String generatedPdfName = prex_file + date_file + DigestUtils.md5Hex(ctch.getRut()).toUpperCase()
						+ ".pdf";

				String linkArchivo = ruta_remota + Constants.FILE_SEP_REMOTE + generatedPdfName;

				HashMap<String, Object> param = new HashMap<String, Object>();
				param.put("pathImg", String.valueOf(carpeta));
				List<CTCHReportEntity> listReport = new ArrayList<CTCHReportEntity>();
				listReport.add(ctch);
				this.generateFilePdf(generatedPdfName, pass, param, carpeta, ruta_remota, listReport);

				result.add(new PdfOutEntity(linkArchivo, ctch.getCorreo(), ctch.getNombre_cliente(), ctch.getRut(),
						ctch.getMes()));

			}
			tracking.setPhase("I,D,R,TS,RF,GFO,PDF");
			tracking.setComments("Generate File PDF");
			tracking.setProcessing(true);
			daoTracking.save(tracking);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e);
		}
		return result;
	}

	private List<PdfOutEntity> generateFilePdfAll(List<VCHandVCLHReportEntity> entity) throws Exception {

		String carpeta = WebInitializer.REAL_PATH_REPORT + "afianza";
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();
		String date_file = dateFormat.format(tracking.getCreated_at());
		String prex_file = typeReport;
		sftp.createDirectory(conn.getIn_repo(), date_file);
		sftp.createDirectory(conn.getIn_repo() + Constants.FILE_SEP_REMOTE + date_file, prex_file);
		sftp.createDirectory(
				conn.getIn_repo() + Constants.FILE_SEP_REMOTE + date_file + Constants.FILE_SEP_REMOTE + prex_file,
				fileName);
		String ruta_remota = date_file + Constants.FILE_SEP_REMOTE + prex_file + Constants.FILE_SEP_REMOTE + fileName;

		try {
			for (VCHandVCLHReportEntity ctch : entity) {
				String[] rut = ctch.getRut().replace(".", "").split("-");
				String pass = rut[0].substring(rut[0].length() - 4);
				String generatedPdfName = prex_file + date_file + DigestUtils.md5Hex(ctch.getRut()).toUpperCase()
						+ ".pdf";

				String linkArchivo = ruta_remota + Constants.FILE_SEP_REMOTE + generatedPdfName;

				HashMap<String, Object> param = new HashMap<String, Object>();
				param.put("pathImg", String.valueOf(carpeta));
				List<VCHandVCLHReportEntity> listReport = new ArrayList<VCHandVCLHReportEntity>();
				listReport.add(ctch);

				this.generateFilePdf(generatedPdfName, pass, param, carpeta, ruta_remota, listReport);

				result.add(new PdfOutEntity(linkArchivo, ctch.getCorreo(), ctch.getNombre_cliente(), ctch.getRut(),
						ctch.getMes()));

			}
			tracking.setPhase("I,D,R,TS,RF,GFO,PDF");
			tracking.setComments("Generate File PDF");
			tracking.setProcessing(true);
			daoTracking.save(tracking);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e);
		}
		return result;
	}

}
