package cl.itdchile.custom.thread.seideman;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.util.ResourceUtils;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.model.afianza.PdfOutEntity;
import cl.itdchile.custom.model.seideman.CartolaDetailReportEntity;
import cl.itdchile.custom.model.seideman.CartolaGraphicReportEntity;
import cl.itdchile.custom.model.seideman.CartolaReportEntity;
import cl.itdchile.custom.utils.FTPUtil;
import cl.itdchile.custom.utils.FileUtil;
import cl.itdchile.custom.utils.FormatUtil;
import cl.itdchile.custom.utils.JasperReportUtil;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import net.sf.jasperreports.engine.JRException;

public class GenerateSeidemanPdfThread extends Thread {
	private static Logger log = Logger.getLogger(GenerateSeidemanPdfThread.class);
	String file = null;
	String fileName = null;
	FTPUtil ftp = null;
	ConnectionEntity conn = null;
	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	DateFormat timeFormat = new SimpleDateFormat("hhmmss");
	EmailEntity conf_email = null;
	ProcessTrackingEntity tracking = null;
	IProcessTrackingDao daoTracking = null;
	RunningJobEntity runningJob = null;
	IRunningJobDao jobDao = null;

	public GenerateSeidemanPdfThread(String file, String fileName, FTPUtil ftp, ConnectionEntity conn,
			EmailEntity conf_email, ProcessTrackingEntity tracking, IProcessTrackingDao daoTracking,
			RunningJobEntity runningJob, IRunningJobDao jobDao) {
		// TODO Auto-generated constructor stub
		this.file = file;
		this.fileName = fileName;
		this.ftp = ftp;
		this.conn = conn;
		this.conf_email = conf_email;
		this.tracking = tracking;
		this.daoTracking = daoTracking;
		this.runningJob = runningJob;
		this.jobDao = jobDao;

	}

	public void run() {
		log.debug("INICIANDO HILO GENERATE_PDF_SEIDEMAN_" + fileName);
		Thread.currentThread().setName("GENERATE_PDF_SEIDEMAN_" + fileName);

		List<CartolaReportEntity> readFileList = new ArrayList<CartolaReportEntity>();
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();

		log.debug("leyendo archivo " + fileName);

		try {
			runningJob.setThread_name(Thread.currentThread().getName());
			jobDao.save(runningJob);
			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("TS")) {
				readFileList = this.readFileCsv(file);
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("RF")) {
				this.generateFileOutTxt();
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 3).equalsIgnoreCase("GFO")) {
				result = this.generateFilePdfFinal(readFileList);
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 3).equalsIgnoreCase("PDF")) {

				if (result.size() == 0)
					result = this.generateFilePdfFinal(readFileList);

				this.generateCsvIda(result);

			}
			System.out.println("tracking " + tracking.getPhase());

			conf_email.setFileName(fileName);
			conf_email.setNotificationType(EnumNotificationType.FINAL.toString());
			conf_email.setMessage(EnumNotificationType.FINAL.toString() + ":" + EnumProcessType.SEIDEMAN.toString());
			conf_email.setTitle("Archivo: " + fileName);

			if (conf_email.isActive_end())
				MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC ");

			jobDao.delete(runningJob);
		} catch (Exception e) {
			// TODO: handle exception

			try {
				jobDao.delete(runningJob);
				tracking.setProcessing(true);
				daoTracking.save(tracking);

				conf_email.setFileName(fileName);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ":" + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);

				if (conf_email.isActive_end())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	private List<PdfOutEntity> generateFilePdfFinal(List<CartolaReportEntity> readFileList)
			throws FileNotFoundException, IOException, Exception {
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();

		if (readFileList.size() == 0)
			readFileList = this.readFileCsv(file);

		result = this.generateFilePdfAll(readFileList);

		return result;
	}

	private void generateCsvIda(List<PdfOutEntity> result) throws Exception

	{

		PrintWriter pw;
		String generatedCsvName = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
				+ conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + fileName
				+ "_Generated.csv";
		try {

			File generatedCsv = new File(generatedCsvName);
			pw = new PrintWriter(generatedCsv, "iso-8859-1");

			StringBuilder sb = new StringBuilder();

			generateHeader(sb);
			generateRow(sb, result);

			pw.write(sb.toString());
			pw.close();

			ftp.uploadFile(generatedCsv, conn.getIn_repo());

			log.info("Generated CSV " + generatedCsvName + " succesfully uploaded to /in folder");
			tracking.setPhase("I,D,R,TS,RF,GFO,PDF,FIN");
			tracking.setFinished(true);
			tracking.setComments("Finish File");
			tracking.setProcessing(false);
			daoTracking.save(tracking);

		} catch (Exception e) {

			log.error("Error uploading generatedOut file: " + generatedCsvName + " to: " + conn.getIn_repo()
					+ ". Error: " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private void generateRow(StringBuilder sb, List<PdfOutEntity> result) {

		for (PdfOutEntity ctch : result) {
			sb.append(ctch.getCorreo());
			sb.append(';');
			sb.append(ctch.getNombre_cliente());
			sb.append(';');
			sb.append(ctch.getRut());
			sb.append(';');
			sb.append(ctch.getArchivo());
			sb.append(';');
			sb.append('\n');
		}

	}

	private void generateHeader(StringBuilder sb) {
		sb.append("EMAIL");
		sb.append(';');
		sb.append("NOMBRE_CLIENTE");
		sb.append(';');
		sb.append("RUT");
		sb.append(';');
		sb.append("ARCHIVO");
		sb.append(';');
		sb.append('\n');

	}

	private boolean validateEmail(String email) throws Exception {
		// string character check
		boolean result = true;
		if (!email.matches("[^@]+@([-\\p{Alnum}]+\\.)*\\p{Alnum}+"))
			result = false;

		return result;
	}

	private List<CartolaReportEntity> readFileCsv(String file) throws FileNotFoundException, IOException, Exception {
		List<CartolaReportEntity> result = new ArrayList<CartolaReportEntity>();
		List<CartolaDetailReportEntity> resultD = new ArrayList<CartolaDetailReportEntity>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"))) {
			String line;
			int linuNumber = 0;
			while ((line = br.readLine()) != null) {

				String[] campo = line.split(";");

				CartolaReportEntity cart = new CartolaReportEntity();
				CartolaDetailReportEntity cartD = new CartolaDetailReportEntity();
				List<CartolaGraphicReportEntity> graphicList = new ArrayList<CartolaGraphicReportEntity>();
				cart.setType(campo[0]);

				if (cart.getType().equalsIgnoreCase("1")) {

					cart.setRut(campo[1]);
					cart.setNum_ctas(campo[2]);
					cart.setNombres(campo[3]);
					cart.setApellido1(campo[4]);
					cart.setApellido2(campo[5]);
					cart.setDir(campo[6]);
					cart.setArtificio(campo[7]);
					cart.setFecha_stat_cta(FormatUtil.formatFecha(campo[8]));
					cart.setTotal_pagar(FormatUtil.formatMiles(campo[9]));
					cart.setPagar_hasta(FormatUtil.formatFecha(campo[10]));
					cart.setCupon_total(FormatUtil.formatMiles(campo[11]));
					cart.setCupon_utilizado(FormatUtil.formatMiles(campo[12]));
					cart.setCupon_disponible(FormatUtil.formatMiles(campo[13]));
					cart.setCupon_total_efectivo(FormatUtil.formatMiles(campo[14]));
					cart.setCupon_utilizado_efectivo(FormatUtil.formatMiles(campo[15]));
					cart.setCupon_disponible_efectivo(FormatUtil.formatMiles(campo[16]));
					cart.setCae_prepago(FormatUtil.formatDecimal(campo[17]));
					cart.setTasa_interes_vigente_rotativo(FormatUtil.formatDecimal(campo[18]));
					cart.setTasa_interes_vigente_cuotas(FormatUtil.formatDecimal(campo[19]));
					cart.setTasa_interes_vigente_avances(FormatUtil.formatDecimal(campo[20]));
					cart.setCae_rotativo(FormatUtil.formatDecimal(campo[21]));
					cart.setCae_cuotas(FormatUtil.formatDecimal(campo[22]));
					cart.setCae_avances(FormatUtil.formatDecimal(campo[23]));
					cart.setCalc_ce(campo[24]);
					cart.setPeriodo_fact_desde(FormatUtil.formatFecha(campo[25]));
					cart.setPeriodo_fact_hasta(FormatUtil.formatFecha(campo[26]));
					cart.setPagar_hasta2(FormatUtil.formatFecha(campo[27]));
					cart.setPeriodo_ant_desde(FormatUtil.formatFecha(campo[28]));
					cart.setPeriodo_ant_hasta(FormatUtil.formatFecha(campo[29]));
					cart.setSaldo_adeudado_inicial_periodo_ant(FormatUtil.formatMiles(campo[30]));
					cart.setMonto_facturado_periodo_anterior(FormatUtil.formatMiles(campo[31]));
					cart.setMonto_pagado_periodo_anterior(FormatUtil.formatMiles(campo[32]));
					cart.setSaldo_adeudado_final_periodo_ant(FormatUtil.formatMiles(campo[33]));
					cart.setMonto_total_facturado(FormatUtil.formatMiles(campo[34]));
					cart.setCosto_monerario_prepago(FormatUtil.formatMiles(campo[35]));
					cart.setMonto_minimo_pagar(FormatUtil.formatMiles(campo[36]));
					cart.setFecha_vencimiento_prox(FormatUtil.formatFecha(campo[37]));
					cart.setMonto_prox(FormatUtil.formatMiles(campo[38]));
					cart.setFecha_vencimiento_prox1(FormatUtil.formatFecha(campo[39]));
					cart.setMonto_prox1(FormatUtil.formatMiles(campo[40]));
					cart.setFecha_vencimiento_prox2(FormatUtil.formatFecha(campo[41]));
					cart.setMonto_prox2(FormatUtil.formatMiles(campo[42]));
					cart.setFecha_vencimiento_prox3(FormatUtil.formatFecha(campo[43]));
					cart.setMonto_prox3(FormatUtil.formatMiles(campo[44]));
					cart.setProximo_periodo_fact_desde(FormatUtil.formatFecha(campo[45]));
					cart.setProximo_periodo_fact_hasta(FormatUtil.formatFecha(campo[46]));
					cart.setInteres_monetario(FormatUtil.formatMiles(campo[47]));
					cart.setGasto_cobranzas(FormatUtil.formatMiles(campo[48]));

					graphicList.add(new CartolaGraphicReportEntity(FormatUtil.mes2(campo[49]),
							Double.valueOf(campo[50]), Double.valueOf(campo[51])));
					graphicList.add(new CartolaGraphicReportEntity(FormatUtil.mes2(campo[52]),
							Double.valueOf(campo[53]), Double.valueOf(campo[54])));
					graphicList.add(new CartolaGraphicReportEntity(FormatUtil.mes2(campo[55]),
							Double.valueOf(campo[56]), Double.valueOf(campo[57])));
					graphicList.add(new CartolaGraphicReportEntity(FormatUtil.mes2(campo[58]),
							Double.valueOf(campo[59]), Double.valueOf(campo[60])));
					graphicList.add(new CartolaGraphicReportEntity(FormatUtil.mes2(campo[61]),
							Double.valueOf(campo[62]), Double.valueOf(campo[63])));
					graphicList.add(
							new CartolaGraphicReportEntity(FormatUtil.mes(campo[10]), Double.valueOf(campo[9]), 0.0));
					cart.setCartolaGraphicList(graphicList);
					cart.setCorreo(campo[64]);

					result.add(cart);
				}

				if (!cart.getType().equalsIgnoreCase("1")) {
					cartD.setType(cart.getType());
					cartD.setRut(campo[1]);
					cartD.setNum_ctas(campo[2]);
					cartD.setLugar_operacion(campo[3]);
					cartD.setFecha_operacion(FormatUtil.formatFecha(campo[4]));
					cartD.setDescripcion_operacion(campo[5]);
					cartD.setMonto_operacion(FormatUtil.formatMiles(campo[6]));
					cartD.setMonto_total_a_pagar(FormatUtil.formatMiles(campo[7]));
					cartD.setCargo_nro_cuotas(campo[8] + "/" + campo[9]);
					cartD.setCargo_valor_cuotas(FormatUtil.formatMiles(campo[10]));
					resultD.add(cartD);
				}

				linuNumber++;
			}

			for (CartolaReportEntity cartola : result) {
				cartola.setTotal_operaciones_list(
						resultD.stream().filter(x -> x.getNum_ctas().equalsIgnoreCase(cartola.getNum_ctas())
								&& x.getType().equalsIgnoreCase("2")).collect(Collectors.toList()));

				cartola.setProductos_servicios_list(
						resultD.stream().filter(x -> x.getNum_ctas().equalsIgnoreCase(cartola.getNum_ctas())
								&& x.getType().equalsIgnoreCase("3")).collect(Collectors.toList()));

				cartola.setCargos_comisiones_list(
						resultD.stream().filter(x -> x.getNum_ctas().equalsIgnoreCase(cartola.getNum_ctas())
								&& x.getType().equalsIgnoreCase("4")).collect(Collectors.toList()));
			}

			tracking.setPhase("I,D,R,TS,RF");
			tracking.setComments("Read File CSV");
			daoTracking.save(tracking);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			throw new FileNotFoundException("Local file " + file + " not found. " + e);

		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			throw new IOException(e);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			throw new Exception(e);

		}
		return result;
	}

	private void generateFileOutTxt() throws Exception {

		String generatedOutFileName = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at())
				+ Constants.FILE_SEP + conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP
				+ fileName + timeFormat.format(new Date()) + ".txt";
		PrintWriter pw1;

		try {

			File generatedOut = new File(generatedOutFileName);
			pw1 = new PrintWriter(generatedOut, "iso-8859-1");

			StringBuilder sb = new StringBuilder();
			sb.append(fileName + "        200 OK");
			pw1.write(sb.toString());
			pw1.close();

			ftp.uploadFile(generatedOut, conn.getOut_repo());
			log.info("Generated Out File" + generatedOutFileName + " succesfully uploaded to " + conn.getOut_repo()
					+ " folder");

			tracking.setPhase("I,D,R,TS,RF,GFO");
			tracking.setComments("Generated Out File" + generatedOutFileName + " succesfully uploaded to "
					+ conn.getOut_repo() + " folder");
			daoTracking.save(tracking);
		} catch (Exception e) {

			log.error("Error uploading generatedOut file: " + generatedOutFileName + " to: " + conn.getOut_repo()
					+ ". Error: " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	private void generateFilePdf(String ruta, String pass, HashMap<String, Object> map, String ruta_remota,
			List<CartolaReportEntity> objList) throws FileNotFoundException, JRException, IOException, Exception {
		byte[] data = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		String report = "";
		try {
			report = ResourceUtils.getFile("classpath:report/seideman/cartola.jasper").getPath();

			new JasperReportUtil().generatPDFOutputStreamBean(report, out, map, objList);
			data = out.toByteArray();
			out.close();

			String localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
					+ conn.getId_ida_client() + Constants.FILE_SEP + "custom" + Constants.FILE_SEP + fileName;
			File folder = new File(localPath);
			folder.mkdirs();

			File generatedPdf = FileUtil.writeFileByteEncrypt(localPath + Constants.FILE_SEP + ruta, data, pass);

			boolean done = ftp.uploadFile(generatedPdf, conn.getIn_repo() + Constants.FILE_SEP_REMOTE + ruta_remota);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new FileNotFoundException("Local file " + report + " not found. " + e);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JRException(e);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException(e);
		}

	}

	private List<PdfOutEntity> generateFilePdfAll(List<CartolaReportEntity> entity) throws Exception {

		String carpeta = ResourceUtils.getFile("classpath:report/seideman").getPath();
		List<PdfOutEntity> result = new ArrayList<PdfOutEntity>();
		String date_file = dateFormat.format(tracking.getCreated_at());
		String prex_file = "EECC";
		ftp.createDirectory(conn.getIn_repo(), date_file);
		ftp.createDirectory(conn.getIn_repo() + Constants.FILE_SEP_REMOTE + date_file, fileName);

		String ruta_remota = date_file + Constants.FILE_SEP_REMOTE + fileName;

		try {
			for (CartolaReportEntity cartola : entity) {

				String pass = cartola.getRut();
				String generatedPdfName = prex_file + date_file + DigestUtils.md5Hex(cartola.getRut()).toUpperCase()
						+ ".pdf";

				String linkArchivo = ruta_remota + Constants.FILE_SEP_REMOTE + generatedPdfName;

				HashMap<String, Object> param = new HashMap<String, Object>();
				param.put("baseUrl", String.valueOf(carpeta));
				List<CartolaReportEntity> listReport = new ArrayList<CartolaReportEntity>();
				listReport.add(cartola);
				this.generateFilePdf(generatedPdfName, pass, param, ruta_remota, listReport);

				result.add(new PdfOutEntity(linkArchivo, cartola.getCorreo(),
						cartola.getNombres() + " " + cartola.getApellido1() + " " + cartola.getApellido2(),
						cartola.getRut()));

			}
			tracking.setPhase("I,D,R,TS,RF,GFO,PDF");
			tracking.setComments("Generate File PDF");
			daoTracking.save(tracking);
		} catch (Exception e) {
			tracking.setProcessing(true);
			daoTracking.save(tracking);
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e);
		}
		return result;
	}

}
