package cl.itdchile.custom.thread.normaliza;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IReportSentSmsDao;
import cl.itdchile.custom.dao.IReportSentSmsWaitingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.dao.ida.ISentSmsDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.model.ida.SentSmsEntity;
import cl.itdchile.custom.model.normaliza.ReportSentSmsEntity;
import cl.itdchile.custom.model.normaliza.ReportSentSmsWaitingEntity;
import cl.itdchile.custom.utils.FTPUtil;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;

public class GenerateNormalizaCsvThread extends Thread {
	private static Logger log = Logger.getLogger(GenerateNormalizaCsvThread.class);
	String fileName = null;
	String file = null;
	String localPath = null;

	FTPUtil ftp = null;
	ConnectionEntity conn = null;
	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	DateFormat dateFormatIda = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	DateFormat timeFormat = new SimpleDateFormat("hhmmss");
	DateFormat dateFormatFile = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
	EmailEntity conf_email = null;
	ProcessTrackingEntity tracking = null;
	IProcessTrackingDao daoTracking = null;
	RunningJobEntity runningJob = null;
	IRunningJobDao jobDao = null;
	IReportSentSmsDao reportSentSmsDao = null;
	ISentSmsDao sentSmsIdadao = null;
	IReportSentSmsWaitingDao reportSentSmsWaitingDao = null;

	public GenerateNormalizaCsvThread(String fileName, FTPUtil ftp, ConnectionEntity conn, EmailEntity conf_email,
			ProcessTrackingEntity tracking, IProcessTrackingDao daoTracking, RunningJobEntity runningJob,
			IRunningJobDao jobDao, IReportSentSmsDao reportSentSmsDao, ISentSmsDao sentSmsIdadao,
			IReportSentSmsWaitingDao reportSentSmsWaitingDao) {
		super();
		this.fileName = fileName;
		this.ftp = ftp;
		this.conn = conn;
		this.conf_email = conf_email;
		this.tracking = tracking;
		this.daoTracking = daoTracking;
		this.runningJob = runningJob;
		this.jobDao = jobDao;
		this.reportSentSmsDao = reportSentSmsDao;
		this.sentSmsIdadao = sentSmsIdadao;
		this.reportSentSmsWaitingDao = reportSentSmsWaitingDao;
	}

	public void run() {
		log.debug("INICIANDO HILO CREATING_CSV_NORMALIZA_" + fileName);
		Thread.currentThread().setName("CREATING_CSV_NORMALIZA_" + fileName);

		localPath = Constants.FTP_DIR + dateFormat.format(tracking.getCreated_at()) + Constants.FILE_SEP
				+ conn.getId_ida_client() + Constants.FILE_SEP + "custom";

		List<ReportSentSmsWaitingEntity> readFileList = new ArrayList<ReportSentSmsWaitingEntity>();
		List<ReportSentSmsWaitingEntity> readFileIDAList = new ArrayList<ReportSentSmsWaitingEntity>();

		log.debug("leyendo archivo " + fileName);

		try {
			File local = new File(localPath);
			local.mkdirs();
			runningJob.setThread_name(Thread.currentThread().getName());
			jobDao.save(runningJob);
			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("TS")) {

				if (fileName.length() > 0) {
					file = localPath + Constants.FILE_SEP + fileName;
					readFileList = this.readFileCsv(file);
				} else {
					tracking.setPhase("I,D,R,TS,CF");
					tracking.setComments("CREATE File CSV");
					daoTracking.save(tracking);
				}
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("RF")
					|| tracking.getPhase().substring(tracking.getPhase().length() - 2).equalsIgnoreCase("CF")) {
				readFileIDAList = this.getSentSmsIda();
				log.info("ida " + readFileIDAList.size());
				log.info("archivo " + readFileList.size());
				readFileList.addAll(readFileIDAList);
				log.info("total archivo " + readFileList.size());
			}

			if (tracking.getPhase().substring(tracking.getPhase().length() - 4).equalsIgnoreCase("GSMS")) {
				if (readFileList.size() == 0)
					readFileList = this.generateFileFinal(readFileList);

				this.generateCsv(readFileList);
			}

			jobDao.delete(runningJob);
		} catch (Exception e) {
			// TODO: handle exception

			try {
				jobDao.delete(runningJob);
				tracking.setProcessing(true);
				daoTracking.save(tracking);

				conf_email.setFileName(fileName);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + " de "
						+ EnumProcessType.NORMALIZA.toString() + " - " + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);

				if (conf_email.isActive_start())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	private List<ReportSentSmsWaitingEntity> generateFileFinal(List<ReportSentSmsWaitingEntity> readFileList)
			throws FileNotFoundException, IOException, Exception {
		List<ReportSentSmsWaitingEntity> result = new ArrayList<ReportSentSmsWaitingEntity>();
		if (fileName.length() > 0) {
			file = localPath + Constants.FILE_SEP + fileName;
			readFileList = this.readFileCsv(file);
		}
		result = this.getSentSmsIda();
		readFileList.addAll(result);

		return readFileList;
	}

	private void generateCsv(List<ReportSentSmsWaitingEntity> result) throws Exception

	{

		PrintWriter pw;
		String nameFile = conn.getPrefix() + dateFormatFile.format(new Date()) + ".csv";

		String generatedCsvName = this.localPath + Constants.FILE_SEP + nameFile;
		try {

			File generatedCsv = new File(generatedCsvName);
			pw = new PrintWriter(generatedCsv, "iso-8859-1");

			StringBuilder sb = new StringBuilder();

			generateHeader(sb);
			generateRow(sb, result);

			pw.write(sb.toString());
			pw.close();
			this.registerReportSentSms(result, nameFile);
			ftp.uploadFile(generatedCsv, conn.getIn_repo());

			log.info("Generated CSV " + generatedCsvName + " succesfully uploaded to /in folder");
			if (this.fileName.length() > 0) {
				tracking.setPhase("I,D,R,TS,RF,GSMS,FIN");
			} else {
				tracking.setPhase("I,D,R,TS,CF,GSMS,FIN");
			}
			tracking.setFile_name(nameFile);
			tracking.setFinished(true);
			tracking.setComments("Finish File");
			tracking.setProcessing(false);
			daoTracking.save(tracking);

		} catch (Exception e) {

			log.error("Error uploading generatedOut file: " + generatedCsvName + " to: " + conn.getIn_repo()
					+ ". Error: " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private void registerReportSentSms(List<ReportSentSmsWaitingEntity> result, String file) throws Exception {
		ReportSentSmsWaitingEntity report = result.get(result.size() - 1);
		ReportSentSmsEntity rptSent = new ReportSentSmsEntity();

		try {
			rptSent = this.reportSentSmsDao.getReportSentSms(EnumProcessType.NORMALIZA.getId());

			if (rptSent == null) {
				rptSent = new ReportSentSmsEntity();
			}
			rptSent.setProcess_type_id(EnumProcessType.NORMALIZA.getId());
			rptSent.setCreated_at(new Date());

			rptSent.setFile(file);

			rptSent.setCreated_at_last_register(report.getFecha_envio());
			rptSent.setId_sent_ida_last_register(report.getId_sent_sms_ida());

			reportSentSmsDao.save(rptSent);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private List<ReportSentSmsWaitingEntity> getSentSmsIda() throws Exception {
		List<ReportSentSmsWaitingEntity> result = new ArrayList<ReportSentSmsWaitingEntity>();
		List<SentSmsEntity> resultIda = new ArrayList<SentSmsEntity>();
		ReportSentSmsEntity report = null;
		try {
			report = reportSentSmsDao.getReportSentSms(EnumProcessType.NORMALIZA.getId());

			if (report != null) {
				resultIda = this.sentSmsIdadao.getSentSmsFilter(conn.getId_ida_client(),
						report.getCreated_at_last_register(), report.getId_sent_ida_last_register());
			} else {
				resultIda = this.sentSmsIdadao.getSentSmsAll(conn.getId_ida_client());
			}
			if (this.fileName.length() > 0) {
				tracking.setPhase("I,D,R,TS,RF,GSMS");
			} else {
				tracking.setPhase("I,D,R,TS,CF,GSMS");

			}

			for (SentSmsEntity r : resultIda) {
				result.add(new ReportSentSmsWaitingEntity(r.getId(), r.getMovil(), r.getMessage().replaceAll(";", ","),
						r.getSent_at(), r.getStatus(), r.getDlr_at()));
				System.out.println("dlr " + r.getDlr_at() + "  id sent " + r.getId());
				if (r.getDlr_at() == null) {
					ReportSentSmsWaitingEntity entity = new ReportSentSmsWaitingEntity(r.getId(), r.getMovil(),
							r.getMessage().replaceAll(";", ","), r.getSent_at(), r.getStatus(), r.getDlr_at());
					this.reportSentSmsWaitingDao.save(entity);
				}
			}
			tracking.setComments("CONSULTING DBA IDA SENT SMS");
			daoTracking.save(tracking);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e);
		}

		return result;
	}

	private List<ReportSentSmsWaitingEntity> readFileCsv(String file)
			throws FileNotFoundException, IOException, Exception {
		List<ReportSentSmsWaitingEntity> result = new ArrayList<ReportSentSmsWaitingEntity>();
		List<ReportSentSmsWaitingEntity> resultWaiting = new ArrayList<ReportSentSmsWaitingEntity>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"))) {
			String line;
			int linuNumber = 0;
			resultWaiting = this.reportSentSmsWaitingDao.getReportSentSmsWaiting();
			while ((line = br.readLine()) != null) {
				if (linuNumber != 0) {
					String[] campo = line.split(";");

					ReportSentSmsWaitingEntity rss = new ReportSentSmsWaitingEntity();
					// System.out.println(line);
					if (resultWaiting != null && resultWaiting.size() > 0) {
						for (ReportSentSmsWaitingEntity entity : resultWaiting) { // recorro los sms waiting

							if (entity.getId_sent_sms_ida() == new Long(campo[0])) { // me paro en el id sent sms
								// pregunto si en el archivo el drl_at es distinto de null

								if (campo[5] != null && !campo[5].equalsIgnoreCase("-")) {
									this.reportSentSmsWaitingDao.delete(entity); // elimino registro basura
								} else {
									// pregunto por el id en el ida

									SentSmsEntity sms = this.sentSmsIdadao.getSentSmsId(entity.getId_sent_sms_ida());

									if (sms.getDlr_at() != null) {
										rss.setFecha_estado(sms.getDlr_at());
										this.reportSentSmsWaitingDao.delete(entity);
									} else {
										if (campo[5] != null && !campo[5].equalsIgnoreCase("-"))
											rss.setFecha_estado(dateFormatIda.parse(campo[5]));
									}
								}
								break;
							} else {
								if (campo[5] != null && !campo[5].equalsIgnoreCase("-"))
									rss.setFecha_estado(dateFormatIda.parse(campo[5]));
							}
						}
					} else {
						if (campo[5] != null && !campo[5].equalsIgnoreCase("-"))
							rss.setFecha_estado(dateFormatIda.parse(campo[5]));
					}

					rss.setId_sent_sms_ida(new Long(campo[0]));
					rss.setTelefono(campo[1]);
					rss.setMensaje(campo[2]);
					rss.setFecha_envio(dateFormatIda.parse(campo[3]));
					rss.setEstado(campo[4]);

					result.add(rss);
				}
				linuNumber++;
			}

			tracking.setPhase("I,D,R,TS,RF");
			tracking.setComments("Read File CSV");
			daoTracking.save(tracking);

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			throw new Exception(e);

		}
		return result;
	}

	private void generateRow(StringBuilder sb, List<ReportSentSmsWaitingEntity> result) {

		for (ReportSentSmsWaitingEntity r : result) {
			sb.append(r.getId_sent_sms_ida());
			sb.append(';');
			sb.append(r.getTelefono());
			sb.append(';');
			sb.append(r.getMensaje().replaceAll("\r\n", " "));
			sb.append(';');
			sb.append(dateFormatIda.format(r.getFecha_envio()));
			sb.append(';');
			sb.append(r.getEstado());
			sb.append(';');
			if (r.getFecha_estado() != null) {
				sb.append(dateFormatIda.format(r.getFecha_estado()));
			} else {
				sb.append("-");
			}
			sb.append(';');
			sb.append('\n');
		}

	}

	private void generateHeader(StringBuilder sb) {
		sb.append("ID");
		sb.append(';');
		sb.append("TELEFONO");
		sb.append(';');
		sb.append("MENSAJE");
		sb.append(';');
		sb.append("FECHA_ENVIO");
		sb.append(';');
		sb.append("ESTADO");
		sb.append(';');
		sb.append("FECHA_ESTADO");
		sb.append(';');
		sb.append('\n');

	}

}
