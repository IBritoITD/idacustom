package cl.itdchile.custom.thread.abertis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.itdchile.custom.Constants;
import cl.itdchile.custom.EnumNotificationType;
import cl.itdchile.custom.EnumProcessType;
import cl.itdchile.custom.Invoice;
import cl.itdchile.custom.dao.IAppManager;
import cl.itdchile.custom.dao.IProcessTrackingDao;
import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.ConnectionEntity;
import cl.itdchile.custom.model.EmailEntity;
import cl.itdchile.custom.model.FileEntity;
import cl.itdchile.custom.model.ProcessTrackingEntity;
import cl.itdchile.custom.model.RunningJobEntity;
import cl.itdchile.custom.utils.MailNotificationUtil;
import cl.itdchile.custom.utils.MailUtil;
import cl.itdchile.custom.utils.SFTPUtil;

public class GenerateXlsxThread extends Thread {
	private static Log log = LogFactory.getLog(GenerateXlsxThread.class);
	String file = null;
	String fileName = null;
	int clientId;
	Date date = null;
	IAppManager appManager = null;
	SFTPUtil sftp = null;
	DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	DateFormat timeFormat = new SimpleDateFormat("hhmmss");
	String ftpOutfolder = null;
	ProcessTrackingEntity tracking = null;
	IProcessTrackingDao dao = null;
	EmailEntity conf_email = null;
	RunningJobEntity runningJob = null;
	IRunningJobDao jobDao = null;
	ConnectionEntity conection = null;

	public GenerateXlsxThread(String file, String fileName, int clientId, Date date, IAppManager appManager,
			SFTPUtil sftp, String ftpOutfolder, IProcessTrackingDao dao, ProcessTrackingEntity tracking,
			EmailEntity conf_email, RunningJobEntity runningJob, IRunningJobDao jobDao, ConnectionEntity conection) {
		// TODO Auto-generated constructor stub
		this.file = file;
		this.fileName = fileName;
		this.clientId = clientId;
		this.date = date;
		this.appManager = appManager;
		this.sftp = sftp;
		this.ftpOutfolder = ftpOutfolder;
		this.dao = dao;
		this.tracking = tracking;
		this.conf_email = conf_email;
		this.runningJob = runningJob;
		this.jobDao = jobDao;
		this.conection = conection;
	}

	public void run() {
		Thread.currentThread().setName("GENERATE_XLSX_" + fileName);
		Map<String, List<Invoice>> values = new HashMap<String, List<Invoice>>();
		String header = "";

		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"))) {
			runningJob.setThread_name(Thread.currentThread().getName());
			jobDao.save(runningJob);
			if (this.tracking.getPhase().equalsIgnoreCase("C")) {
				header = this.readFile(header, br, values);

				this.tracking.setPhase("D");
				tracking.setUpdated_at(new Date());
				tracking.setComments("File " + fileName + " registered in BD");

				dao.save(tracking);
			}

			String generatedOutFileName = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP + clientId
					+ Constants.FILE_SEP + "custom" + Constants.FILE_SEP + fileName + timeFormat.format(new Date())
					+ ".txt";

			if (this.tracking.getPhase().equalsIgnoreCase("D")) {

				PrintWriter pw1;

				try {

					File generatedOut = new File(generatedOutFileName);
					pw1 = new PrintWriter(generatedOut, "iso-8859-1");

					String dist = header.substring(0, 5).trim();
					String remesa = header.substring(5, 15).trim();
					String lote = header.substring(15, 17).trim();
					String fecha = header.substring(17, 25).trim();

					StringBuilder sb = new StringBuilder();
					sb.append(dist + remesa + lote + fecha + "           102");
					pw1.write(sb.toString());
					pw1.close();

					sftp.uploadFile(generatedOut, conection.getOut_repo());

					log.info("Generated Out File" + generatedOutFileName + " succesfully uploaded to " + ftpOutfolder
							+ " folder");
				} catch (Exception e) {
					log.error("Error uploading generatedOut file: " + generatedOutFileName + " to: " + ftpOutfolder
							+ ". Error: " + e.getMessage());
				}

				this.tracking.setPhase("E");
				tracking.setUpdated_at(new Date());
				tracking.setComments("Generated Out File" + generatedOutFileName + " succesfully uploaded to "
						+ ftpOutfolder + " folder");

				dao.save(tracking);

			}

			PrintWriter pw;
			String generatedCsvName = Constants.FTP_DIR + dateFormat.format(date) + Constants.FILE_SEP + clientId
					+ Constants.FILE_SEP + "custom" + Constants.FILE_SEP + fileName + "_Generated.csv";
			if (this.tracking.getPhase().equalsIgnoreCase("E")) {
				try {

					File generatedCsv = new File(generatedCsvName);
					pw = new PrintWriter(generatedCsv, "iso-8859-1");

					StringBuilder sb = new StringBuilder();

					generateHeader(sb);

					Iterator<Entry<String, List<Invoice>>> it = values.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, List<Invoice>> pair = it.next();

						generateRow(sb, pair);
						it.remove(); // avoids a ConcurrentModificationException
					}

					pw.write(sb.toString());
					pw.close();

					sftp.uploadFile(generatedCsv, conection.getIn_repo());
					this.tracking.setPhase("DONE");
					tracking.setFinished(true);
					tracking.setUpdated_at(new Date());
					tracking.setComments("Generated CSV " + generatedCsvName + " succesfully uploaded to /in folder");

					dao.save(tracking);

					conf_email.setFileName(file);
					conf_email.setNotificationType(EnumNotificationType.FINAL.toString());
					conf_email.setMessage(
							EnumNotificationType.FINAL.toString() + " de " + EnumProcessType.ABERTIS.toString());
					conf_email.setTitle("Archivo: " + fileName);
					if (conf_email.isActive_end())
						MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC");

					log.info("Generated CSV " + generatedCsvName + " succesfully uploaded to /in folder");

				} catch (FileNotFoundException e) {
					log.error("Error uploading generatedOut file: " + generatedOutFileName + " to: " + ftpOutfolder
							+ ". Error: " + e.getMessage());
					this.tracking.setComments(e.getMessage());
					tracking.setUpdated_at(new Date());
					dao.save(this.tracking);
				}

			}

			jobDao.delete(runningJob);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);
				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e2) {
				e2.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);
				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");
				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			try {
				jobDao.delete(runningJob);
				conf_email.setFileName(file);
				conf_email.setNotificationType(EnumNotificationType.ERROR.toString());
				conf_email.setMessage(EnumNotificationType.ERROR.toString() + ": " + e.getMessage());
				conf_email.setTitle("Archivo: " + fileName);
				if (conf_email.isActive_error())
					MailUtil.sendEmail(conf_email, MailNotificationUtil.mailHtml(conf_email), "Notificacion IC Error");

				this.tracking.setComments(e.getMessage());
				tracking.setUpdated_at(new Date());
				dao.save(this.tracking);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	private String readFile(String header, BufferedReader br, Map<String, List<Invoice>> values) throws Exception {
		try {
			tracking.setPhase("a,b,c,d");
			tracking.setUpdated_at(new Date());
			tracking.setComments("Reading IN File");

			dao.save(tracking);

			String line;
			int linuNumber = 0;
			while ((line = br.readLine()) != null) {

				if (linuNumber == 0) {
					header = line;
				} else {
					String invoice = line.substring(0, 16).trim();
					String stringEmails = line.substring(16, 96).trim().toLowerCase();
					String rut = line.substring(96, 111).trim();
					String nombre = line.substring(111, 151).trim();
					String monto = line.substring(151, 167).trim();
					String fechaEmision = line.substring(167, 175).trim();
					String fechaVenc = line.substring(175, 183).trim();
					String url = line.substring(183, line.length()).trim();

					if (url.toLowerCase().startsWith("http")) {

						Invoice inv = new Invoice(nombre, invoice, rut, monto, fechaEmision, url, fechaVenc, "");

						String[] emails = { stringEmails };

						if (stringEmails.contains(";")) {
							emails = stringEmails.split(";");
						}

						for (int i = 0; i < emails.length; i++) {
							String email = emails[i];
							try {
								validateEmail(email);
								List<Invoice> invoices = new ArrayList<Invoice>();
								if (values.containsKey(email)) {
									invoices = values.get(email);
									invoices.add(inv);
									values.remove(email);
									values.put(email, invoices);
								} else {

									invoices.add(inv);
									values.put(email, invoices);
								}
							} catch (Exception e) {
								log.warn("Skipping bad email: " + email);
								e.printStackTrace();
							}

						}

					} else {
						log.error("Wrong URL for Invoice: " + invoice);
					}
				}
				linuNumber++;
			}

			FileEntity f = new FileEntity();
			f.setCreadtedAt(new Date());
			f.setUpdateddAt(new Date());
			f.setFileName(fileName);
			f.setHeader(header);
			f.setProcessing(false);
			f.setStatus(false);
			appManager.saveFile(f);

			tracking.setId_file(f.getId());
			tracking.setUpdated_at(new Date());

			dao.save(tracking);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return header;
	}

	private void generateRow(StringBuilder sb, Entry<String, List<Invoice>> pair) {
		String email = pair.getKey();
		List<Invoice> invoices = pair.getValue();

		int invoicesCount = invoices.size();
		String[] names = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"" };
		String[] ruts = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
		String[] montos = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"" };
		String[] fechas = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"" };
		String[] links = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"" };
		String[] vencimintos = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "" };
		String[] vers = new String[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };

		int i = 0;
		for (Invoice invoice : invoices) {
			if (i < 20) {
				names[i] = invoice.getNombre();
				ruts[i] = invoice.getRut();
				montos[i] = invoice.getMonto();
				fechas[i] = invoice.getFechaEmision();
				links[i] = invoice.getLinkFactura();
				vencimintos[i] = invoice.getFechaVencimiento();
				vers[i] = invoice.getVerAqui();
			} else {
				System.out.println("skipping");
			}

			i++;
		}

		sb.append(email);
		sb.append(';');
		for (int j = 0; j < names.length; j++) {
			sb.append(names[j]);
			sb.append(';');
		}
		for (int j = 0; j < ruts.length; j++) {
			sb.append(ruts[j]);
			sb.append(';');
		}
		for (int j = 0; j < montos.length; j++) {
			sb.append(montos[j]);
			sb.append(';');
		}
		for (int j = 0; j < fechas.length; j++) {
			sb.append(fechas[j]);
			sb.append(';');
		}
		for (int j = 0; j < links.length; j++) {
			sb.append(links[j]);
			sb.append(';');
		}
		for (int j = 0; j < vencimintos.length; j++) {
			sb.append(vencimintos[j]);
			sb.append(';');
		}
		for (int j = 0; j < vers.length; j++) {
			sb.append(vers[j]);
			if (j == 19)
				sb.append('\n');
			else
				sb.append(';');
		}

	}

	private void generateHeader(StringBuilder sb) {
		sb.append("EMAIL");
		sb.append(';');
		sb.append("NOMBRE 1");
		sb.append(';');
		sb.append("NOMBRE 2");
		sb.append(';');
		sb.append("NOMBRE 3");
		sb.append(';');
		sb.append("NOMBRE 4");
		sb.append(';');
		sb.append("NOMBRE 5");
		sb.append(';');
		sb.append("NOMBRE 6");
		sb.append(';');
		sb.append("NOMBRE 7");
		sb.append(';');
		sb.append("NOMBRE 8");
		sb.append(';');
		sb.append("NOMBRE 9");
		sb.append(';');
		sb.append("NOMBRE 10");
		sb.append(';');
		sb.append("NOMBRE 11");
		sb.append(';');
		sb.append("NOMBRE 12");
		sb.append(';');
		sb.append("NOMBRE 13");
		sb.append(';');
		sb.append("NOMBRE 14");
		sb.append(';');
		sb.append("NOMBRE 15");
		sb.append(';');
		sb.append("NOMBRE 16");
		sb.append(';');
		sb.append("NOMBRE 17");
		sb.append(';');
		sb.append("NOMBRE 18");
		sb.append(';');
		sb.append("NOMBRE 19");
		sb.append(';');
		sb.append("NOMBRE 20");
		sb.append(';');
		sb.append("RUT 1");
		sb.append(';');
		sb.append("RUT 2");
		sb.append(';');
		sb.append("RUT 3");
		sb.append(';');
		sb.append("RUT 4");
		sb.append(';');
		sb.append("RUT 5");
		sb.append(';');
		sb.append("RUT 6");
		sb.append(';');
		sb.append("RUT 7");
		sb.append(';');
		sb.append("RUT 8");
		sb.append(';');
		sb.append("RUT 9");
		sb.append(';');
		sb.append("RUT 10");
		sb.append(';');
		sb.append("RUT 11");
		sb.append(';');
		sb.append("RUT 12");
		sb.append(';');
		sb.append("RUT 13");
		sb.append(';');
		sb.append("RUT 14");
		sb.append(';');
		sb.append("RUT 15");
		sb.append(';');
		sb.append("RUT 16");
		sb.append(';');
		sb.append("RUT 17");
		sb.append(';');
		sb.append("RUT 18");
		sb.append(';');
		sb.append("RUT 19");
		sb.append(';');
		sb.append("RUT 20");
		sb.append(';');
		sb.append("Monto Factura 1");
		sb.append(';');
		sb.append("Monto Factura 2");
		sb.append(';');
		sb.append("Monto Factura 3");
		sb.append(';');
		sb.append("Monto Factura 4");
		sb.append(';');
		sb.append("Monto Factura 5");
		sb.append(';');
		sb.append("Monto Factura 6");
		sb.append(';');
		sb.append("Monto Factura 7");
		sb.append(';');
		sb.append("Monto Factura 8");
		sb.append(';');
		sb.append("Monto Factura 9");
		sb.append(';');
		sb.append("Monto Factura 10");
		sb.append(';');
		sb.append("Monto Factura 11");
		sb.append(';');
		sb.append("Monto Factura 12");
		sb.append(';');
		sb.append("Monto Factura 13");
		sb.append(';');
		sb.append("Monto Factura 14");
		sb.append(';');
		sb.append("Monto Factura 15");
		sb.append(';');
		sb.append("Monto Factura 16");
		sb.append(';');
		sb.append("Monto Factura 17");
		sb.append(';');
		sb.append("Monto Factura 18");
		sb.append(';');
		sb.append("Monto Factura 19");
		sb.append(';');
		sb.append("Monto Factura 20");
		sb.append(';');
		sb.append("Fecha Emision 1");
		sb.append(';');
		sb.append("Fecha Emision 2");
		sb.append(';');
		sb.append("Fecha Emision 3");
		sb.append(';');
		sb.append("Fecha Emision 4");
		sb.append(';');
		sb.append("Fecha Emision 5");
		sb.append(';');
		sb.append("Fecha Emision 6");
		sb.append(';');
		sb.append("Fecha Emision 7");
		sb.append(';');
		sb.append("Fecha Emision 8");
		sb.append(';');
		sb.append("Fecha Emision 9");
		sb.append(';');
		sb.append("Fecha Emision 10");
		sb.append(';');
		sb.append("Fecha Emision 11");
		sb.append(';');
		sb.append("Fecha Emision 12");
		sb.append(';');
		sb.append("Fecha Emision 13");
		sb.append(';');
		sb.append("Fecha Emision 14");
		sb.append(';');
		sb.append("Fecha Emision 15");
		sb.append(';');
		sb.append("Fecha Emision 16");
		sb.append(';');
		sb.append("Fecha Emision 17");
		sb.append(';');
		sb.append("Fecha Emision 18");
		sb.append(';');
		sb.append("Fecha Emision 19");
		sb.append(';');
		sb.append("Fecha Emision 20");
		sb.append(';');
		sb.append("LINK FACTURA 1");
		sb.append(';');
		sb.append("LINK FACTURA 2");
		sb.append(';');
		sb.append("LINK FACTURA 3");
		sb.append(';');
		sb.append("LINK FACTURA 4");
		sb.append(';');
		sb.append("LINK FACTURA 5");
		sb.append(';');
		sb.append("LINK FACTURA 6");
		sb.append(';');
		sb.append("LINK FACTURA 7");
		sb.append(';');
		sb.append("LINK FACTURA 8");
		sb.append(';');
		sb.append("LINK FACTURA 9");
		sb.append(';');
		sb.append("LINK FACTURA 10");
		sb.append(';');
		sb.append("LINK FACTURA 11");
		sb.append(';');
		sb.append("LINK FACTURA 12");
		sb.append(';');
		sb.append("LINK FACTURA 13");
		sb.append(';');
		sb.append("LINK FACTURA 14");
		sb.append(';');
		sb.append("LINK FACTURA 15");
		sb.append(';');
		sb.append("LINK FACTURA 16");
		sb.append(';');
		sb.append("LINK FACTURA 17");
		sb.append(';');
		sb.append("LINK FACTURA 18");
		sb.append(';');
		sb.append("LINK FACTURA 19");
		sb.append(';');
		sb.append("LINK FACTURA 20");
		sb.append(';');
		sb.append("Fecha Vencimiento 1");
		sb.append(';');
		sb.append("Fecha Vencimiento 2");
		sb.append(';');
		sb.append("Fecha Vencimiento 3");
		sb.append(';');
		sb.append("Fecha Vencimiento 4");
		sb.append(';');
		sb.append("Fecha Vencimiento 5");
		sb.append(';');
		sb.append("Fecha Vencimiento 6");
		sb.append(';');
		sb.append("Fecha Vencimiento 7");
		sb.append(';');
		sb.append("Fecha Vencimiento 8");
		sb.append(';');
		sb.append("Fecha Vencimiento 9");
		sb.append(';');
		sb.append("Fecha Vencimiento 10");
		sb.append(';');
		sb.append("Fecha Vencimiento 11");
		sb.append(';');
		sb.append("Fecha Vencimiento 12");
		sb.append(';');
		sb.append("Fecha Vencimiento 13");
		sb.append(';');
		sb.append("Fecha Vencimiento 14");
		sb.append(';');
		sb.append("Fecha Vencimiento 15");
		sb.append(';');
		sb.append("Fecha Vencimiento 16");
		sb.append(';');
		sb.append("Fecha Vencimiento 17");
		sb.append(';');
		sb.append("Fecha Vencimiento 18");
		sb.append(';');
		sb.append("Fecha Vencimiento 19");
		sb.append(';');
		sb.append("Fecha Vencimiento 20");
		sb.append(';');
		sb.append("Ver Aqu� 1");
		sb.append(';');
		sb.append("Ver Aqu� 2");
		sb.append(';');
		sb.append("Ver Aqu� 3");
		sb.append(';');
		sb.append("Ver Aqu� 4");
		sb.append(';');
		sb.append("Ver Aqu� 5");
		sb.append(';');
		sb.append("Ver Aqu� 6");
		sb.append(';');
		sb.append("Ver Aqu� 7");
		sb.append(';');
		sb.append("Ver Aqu� 8");
		sb.append(';');
		sb.append("Ver Aqu� 9");
		sb.append(';');
		sb.append("Ver Aqu� 10");
		sb.append(';');
		sb.append("Ver Aqu� 11");
		sb.append(';');
		sb.append("Ver Aqu� 12");
		sb.append(';');
		sb.append("Ver Aqu� 13");
		sb.append(';');
		sb.append("Ver Aqu� 14");
		sb.append(';');
		sb.append("Ver Aqu� 15");
		sb.append(';');
		sb.append("Ver Aqu� 16");
		sb.append(';');
		sb.append("Ver Aqu� 17");
		sb.append(';');
		sb.append("Ver Aqu� 18");
		sb.append(';');
		sb.append("Ver Aqu� 19");
		sb.append(';');
		sb.append("Ver Aqu� 20");
		sb.append('\n');

	}

	public static void validateEmail(String email) throws Exception {
		// string character check
		if (!email.matches("[^@]+@([-\\p{Alnum}]+\\.)*\\p{Alnum}+"))
			throw new Exception("Invalid Characters in e-mail address.");

		return;
	}

}
