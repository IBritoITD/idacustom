package cl.itdchile.custom;

public enum EnumHostConn {
	FTP(1),
	SFTP(2);
	

	private int conn;
	
	private EnumHostConn (int conn){
		this.conn = conn;
	}

	public int getConn() {
		return conn;
	}

	public void setConn(int conn) {
		this.conn = conn;
	}

	
}
