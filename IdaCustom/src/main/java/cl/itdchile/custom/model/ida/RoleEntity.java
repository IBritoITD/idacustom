package cl.itdchile.custom.model.ida;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.security.core.GrantedAuthority;

/**
 * This class is used to represent available roles in the database.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Version by
 *         Dan Kibler dan@getrolling.com Extended to implement Acegi
 *         GrantedAuthority interface by David Carter david@carter.net
 */
@Entity
@Table(name = "role")
@NamedNativeQueries({ @NamedNativeQuery(resultClass = RoleEntity.class,name = "findRoleByName", query = "select r from Role r where r.name = ?1 ") })
public class RoleEntity implements Serializable, GrantedAuthority {
	private static final long serialVersionUID = 3690197650654049848L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name", length = 20)
	private String name;
	
	@Column(name = "description",length = 64)
	private String description;

	/**
	 * Default constructor - creates a new instance with no values set.
	 */
	public RoleEntity() {
	}

	/**
	 * Create a new instance and set the name.
	 * 
	 * @param name name of the role.
	 */
	public RoleEntity(final String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	/**
	 * @see org.springframework.security.GrantedAuthority#getAuthority()
	 * @return the name property (getAuthority required by Acegi's GrantedAuthority
	 *         interface)
	 */
	@Transient
	public String getAuthority() {
		return getName();
	}

	public String getName() {
		return this.name;
	}

	
	public String getDescription() {
		return this.description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
