package cl.itdchile.custom.model.seideman;

/**
 * @author Gerencia ITD
 *
 */
public class CartolaGraphicReportEntity {

	private String fecha_facturado_pagados;
	private double monto_facturado;
	private double monto_pagados;

	public CartolaGraphicReportEntity() {
		super();

	}

	public CartolaGraphicReportEntity(String fecha_facturado_pagados, double monto_facturado, double monto_pagados) {
		super();
		this.fecha_facturado_pagados = fecha_facturado_pagados;
		this.monto_facturado = monto_facturado;
		this.monto_pagados = monto_pagados;
	}

	public String getFecha_facturado_pagados() {
		return fecha_facturado_pagados;
	}

	public void setFecha_facturado_pagados(String fecha_facturado_pagados) {
		this.fecha_facturado_pagados = fecha_facturado_pagados;
	}

	public double getMonto_facturado() {
		return monto_facturado;
	}

	public void setMonto_facturado(double monto_facturado) {
		this.monto_facturado = monto_facturado;
	}

	public double getMonto_pagados() {
		return monto_pagados;
	}

	public void setMonto_pagados(double monto_pagados) {
		this.monto_pagados = monto_pagados;
	}

}
