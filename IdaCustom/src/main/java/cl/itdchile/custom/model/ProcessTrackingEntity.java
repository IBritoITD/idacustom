package cl.itdchile.custom.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_process_tracking")
public class ProcessTrackingEntity extends BaseObject implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	protected int id;
	@Column(name = "id_file")
	protected long id_file;
	@Column(name = "phase", nullable = false)
	protected String phase;
	@Column(name = "file_name")
	protected String file_name;
	@Column(name = "comments")
	protected String comments;
	@Column(name = "created_at", updatable = false, nullable = false)
	protected Date created_at;
	@Column(name = "updated_at")
	protected Date updated_at;
	@Column(name = "processing", nullable = false)
	protected boolean processing;
	@Column(name = "finished", nullable = false)
	protected boolean finished;
	@Column(name = "id_process_type", nullable = false)
	protected int id_process_type;

	public ProcessTrackingEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId_file() {
		return id_file;
	}

	public void setId_file(long id_file) {
		this.id_file = id_file;
	}

	public int getId_process_type() {
		return id_process_type;
	}

	public void setId_process_type(int id_process_type) {
		this.id_process_type = id_process_type;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public boolean isProcessing() {
		return processing;
	}

	public void setProcessing(boolean processing) {
		this.processing = processing;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	public ProcessTrackingEntity(String phase, String file_name, String comments, Date created_at, Date updated_at,
			boolean processing, boolean finished, int id_process_type) {
		super();
		this.phase = phase;
		this.file_name = file_name;
		this.comments = comments;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.processing = processing;
		this.finished = finished;
		this.id_process_type = id_process_type;

	}

	public ProcessTrackingEntity(String phase, String comments, Date updated_at, boolean processing, boolean finished,
			int id_process_type) {
		super();
		this.phase = phase;
		this.comments = comments;
		this.updated_at = updated_at;
		this.processing = processing;
		this.finished = finished;
		this.id_process_type = id_process_type;

	}

}
