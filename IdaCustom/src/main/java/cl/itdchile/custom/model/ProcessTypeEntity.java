package cl.itdchile.custom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_process_type")
public class ProcessTypeEntity extends BaseObject {

	@Id
	@Column(name = "id", nullable = false)
	protected int id;
	@Column(name = "name")
	protected String name;
	@Column(name = "description")
	protected String description;

	public ProcessTypeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProcessTypeEntity(int id) {
		super();
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
