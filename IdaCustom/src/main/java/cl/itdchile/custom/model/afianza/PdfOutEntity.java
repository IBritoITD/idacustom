package cl.itdchile.custom.model.afianza;

/**
 * @author Gerencia ITD
 *
 */
public class PdfOutEntity {
	private String archivo;
	private String correo;
	private String nombre_cliente;
	private String rut;
	private String mes;

	public PdfOutEntity() {
		super();
	}

	
	public PdfOutEntity(String archivo, String correo, String nombre_cliente,String rut, String mes) {
		super();
		this.archivo = archivo;
		this.correo = correo;
		this.nombre_cliente= nombre_cliente;
		this.rut = rut;
		this.mes = mes;
	}


	public PdfOutEntity(String archivo, String correo, String nombre_cliente, String rut) {
		super();
		this.archivo = archivo;
		this.correo = correo;
		this.nombre_cliente = nombre_cliente;
		this.rut = rut;
	}


	public String getArchivo() {
		return archivo;
	}


	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}


	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public String getNombre_cliente() {
		return nombre_cliente;
	}


	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}


	public String getRut() {
		return rut;
	}


	public void setRut(String rut) {
		this.rut = rut;
	}


	public String getMes() {
		return mes;
	}


	public void setMes(String mes) {
		this.mes = mes;
	}

}
