package cl.itdchile.custom.model.client;

import cl.itdchile.custom.model.BaseObject;

public class IndexFullEntity extends BaseObject {

	protected Long abertisProcessed;
	protected Long afianzaProcessed;
	protected Long normalizaProcessed;

	public IndexFullEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getAbertisProcessed() {
		return abertisProcessed;
	}

	public void setAbertisProcessed(Long abertisProcessed) {
		this.abertisProcessed = abertisProcessed;
	}

	public Long getAfianzaProcessed() {
		return afianzaProcessed;
	}

	public void setAfianzaProcessed(Long afianzaProcessed) {
		this.afianzaProcessed = afianzaProcessed;
	}

	public Long getNormalizaProcessed() {
		return normalizaProcessed;
	}

	public void setNormalizaProcessed(Long normalizaProcessed) {
		this.normalizaProcessed = normalizaProcessed;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
