package cl.itdchile.custom.model.seideman;

/**
 * @author Gerencia ITD
 *
 */
public class CartolaDetailReportEntity {
	private String type;
	private String rut;
	private String num_ctas;
	private String lugar_operacion;
	private String fecha_operacion;
	private String descripcion_operacion;
	private String monto_operacion;
	private String monto_total_a_pagar;
	private String cargo_nro_cuotas;
	private String cargo_valor_cuotas;
	
	
	
	
	
	
	public CartolaDetailReportEntity() {
		super();
	}
	public CartolaDetailReportEntity(String type, String rut,String num_ctas, String lugar_operacion, String fecha_operacion,
			String descripcion_operacion, String monto_operacion, String monto_total_a_pagar, String cargo_nro_cuotas,
			String cargo_valor_cuotas) {
		super();
		this.type = type;
		this.rut = rut;
		this.lugar_operacion = lugar_operacion;
		this.fecha_operacion = fecha_operacion;
		this.descripcion_operacion = descripcion_operacion;
		this.monto_operacion = monto_operacion;
		this.monto_total_a_pagar = monto_total_a_pagar;
		this.cargo_nro_cuotas = cargo_nro_cuotas;
		this.cargo_valor_cuotas = cargo_valor_cuotas;
		this.num_ctas= num_ctas;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getLugar_operacion() {
		return lugar_operacion;
	}
	public void setLugar_operacion(String lugar_operacion) {
		this.lugar_operacion = lugar_operacion;
	}
	public String getFecha_operacion() {
		return fecha_operacion;
	}
	public void setFecha_operacion(String fecha_operacion) {
		this.fecha_operacion = fecha_operacion;
	}
	public String getDescripcion_operacion() {
		return descripcion_operacion;
	}
	public void setDescripcion_operacion(String descripcion_operacion) {
		this.descripcion_operacion = descripcion_operacion;
	}
	public String getMonto_operacion() {
		return monto_operacion;
	}
	public void setMonto_operacion(String monto_operacion) {
		this.monto_operacion = monto_operacion;
	}
	public String getMonto_total_a_pagar() {
		return monto_total_a_pagar;
	}
	public void setMonto_total_a_pagar(String monto_total_a_pagar) {
		this.monto_total_a_pagar = monto_total_a_pagar;
	}
	public String getCargo_nro_cuotas() {
		return cargo_nro_cuotas;
	}
	public void setCargo_nro_cuotas(String cargo_nro_cuotas) {
		this.cargo_nro_cuotas = cargo_nro_cuotas;
	}
	public String getCargo_valor_cuotas() {
		return cargo_valor_cuotas;
	}
	public void setCargo_valor_cuotas(String cargo_valor_cuotas) {
		this.cargo_valor_cuotas = cargo_valor_cuotas;
	}
	public String getNum_ctas() {
		return num_ctas;
	}
	public void setNum_ctas(String num_ctas) {
		this.num_ctas = num_ctas;
	}
	
	

}
