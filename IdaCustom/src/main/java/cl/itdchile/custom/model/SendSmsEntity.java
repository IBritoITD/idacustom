package cl.itdchile.custom.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "t_send_sms")
public class SendSmsEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	protected int id;
	@Column(name = "text")
	protected String text;
	@Column(name = "number", nullable = false)
	protected String number;
	@Column(name = "password", nullable = false)
	protected String password;
	@Column(name = "id_ida")
	@ColumnDefault("")
	protected String id_ida;
	@Column(name = "recsa_id", columnDefinition = "BIGINT")
	protected Long recsa_id;
	@Column(name = "created_at", updatable = false)
	protected Date created_at;
	@Column(name = "updated_at")
	protected Date updated_at;
	@Column(name = "status")
	protected String status;
	@Column(name = "user_name", nullable = false)
	protected String user_name;

	public SendSmsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SendSmsEntity(String text, String number, Long recsa_id) {
		super();
		this.text = text;
		this.number = number;
		this.recsa_id = recsa_id;
	}

	public SendSmsEntity(String text, String number, String password, Long recsa_id, String user_name) {
		super();
		this.text = text;
		this.number = number;
		this.password = password;
		this.recsa_id = recsa_id;
		this.user_name = user_name;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getRecsa_id() {
		return recsa_id;
	}

	public void setRecsa_id(Long recsa_id) {
		this.recsa_id = recsa_id;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getId_ida() {
		return id_ida;
	}

	public void setId_ida(String id_ida) {
		this.id_ida = id_ida;
	}

}
