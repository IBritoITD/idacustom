package cl.itdchile.custom.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_config_connection")
public class ConnectionEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected int id;

	@Column(name = "name", nullable = false)
	protected String name;

	@Column(name = "conn_type_id", nullable = false)
	protected int conn_type_id;

	@Column(name = "conn_ip", nullable = false)
	protected String conn_ip;

	@Column(name = "conn_user", nullable = false)
	protected String conn_user;

	@Column(name = "conn_pass", nullable = false)
	protected String conn_pass;

	@Column(name = "conn_port", nullable = false)
	protected int conn_port;

	@Column(name = "id_ida_client", nullable = false)
	protected int id_ida_client;

	@Column(name = "in_repo", nullable = false)
	protected String in_repo;

	@Column(name = "out_repo", nullable = false)
	protected String out_repo;

	@Column(name = "serv_ida_user", nullable = false)
	protected String serv_ida_user;

	@Column(name = "serv_ida_pass", nullable = false)
	protected String serv_ida_pass;

	@Column(name = "prefix", nullable = false)
	protected String prefix;

	@Column(name = "generate_input_time", nullable = false)
	protected long generate_input_time;
	
	@Column(name = "generate_output_time", nullable = false)
	protected long generate_output_time;
	
	@Column(name = "not_processed_time", nullable = false)
	protected long not_processed_time;

	public ConnectionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getConn_type_id() {
		return conn_type_id;
	}

	public void setConn_type_id(int conn_type_id) {
		this.conn_type_id = conn_type_id;
	}

	public String getConn_ip() {
		return conn_ip;
	}

	public void setConn_ip(String conn_ip) {
		this.conn_ip = conn_ip;
	}

	public String getConn_user() {
		return conn_user;
	}

	public void setConn_user(String conn_user) {
		this.conn_user = conn_user;
	}

	public String getConn_pass() {
		return conn_pass;
	}

	public void setConn_pass(String conn_pass) {
		this.conn_pass = conn_pass;
	}

	public int getConn_port() {
		return conn_port;
	}

	public void setConn_port(int conn_port) {
		this.conn_port = conn_port;
	}

	public int getId_ida_client() {
		return id_ida_client;
	}

	public void setId_ida_client(int id_ida_client) {
		this.id_ida_client = id_ida_client;
	}

	public String getIn_repo() {
		return in_repo;
	}

	public void setIn_repo(String in_repo) {
		this.in_repo = in_repo;
	}

	public String getOut_repo() {
		return out_repo;
	}

	public void setOut_repo(String out_repo) {
		this.out_repo = out_repo;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getServ_ida_user() {
		return serv_ida_user;
	}

	public void setServ_ida_user(String serv_ida_user) {
		this.serv_ida_user = serv_ida_user;
	}

	public String getServ_ida_pass() {
		return serv_ida_pass;
	}

	public void setServ_ida_pass(String serv_ida_pass) {
		this.serv_ida_pass = serv_ida_pass;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public long getGenerate_input_time() {
		return generate_input_time;
	}

	public void setGenerate_input_time(long generate_input_time) {
		this.generate_input_time = generate_input_time;
	}

	public long getGenerate_output_time() {
		return generate_output_time;
	}

	public void setGenerate_output_time(long generate_output_time) {
		this.generate_output_time = generate_output_time;
	}

	public long getNot_processed_time() {
		return not_processed_time;
	}

	public void setNot_processed_time(long not_processed_time) {
		this.not_processed_time = not_processed_time;
	}

}
