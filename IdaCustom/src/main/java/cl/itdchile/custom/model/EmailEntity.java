package cl.itdchile.custom.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_config_email")
public class EmailEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected int id;

	@Column(name = "host", nullable = false)
	protected String host;

	@Column(name = "user", nullable = false)
	protected String user;

	@Column(name = "pass", nullable = false)
	protected String pass;

	@Column(name = "port", nullable = false)
	protected int port;

	@Column(name = "auth", nullable = false)
	protected boolean auth;

	@Column(name = "starttls_enable", nullable = false)
	protected boolean starttls_enable;

	@Column(name = "email_to", nullable = false)
	protected String email_to;

	@Column(name = "active_start", nullable = false)
	protected boolean active_start;

	@Column(name = "active_end", nullable = false)
	protected boolean active_end;

	@Column(name = "active_error", nullable = false)
	protected boolean active_error;

	@Column(name = "id_process_type", nullable = false, insertable = false, updatable = false)
	protected int id_process_type;

	@OneToOne
	@JoinColumn(name = "id_process_type")
	private ProcessTypeEntity processType;

	@Transient
	protected String message;

	@Transient
	protected String notificationType; // Error, inicio o final

	@Transient
	protected String title;

	@Transient
	protected String fileName;

	public EmailEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmailEntity(int id, String host, String user, String pass, int port, boolean auth, boolean starttls_enable,
			String email_to) {
		super();
		this.id = id;
		this.host = host;
		this.user = user;
		this.pass = pass;
		this.port = port;
		this.auth = auth;
		this.starttls_enable = starttls_enable;
		this.email_to = email_to;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId_process_type() {
		return id_process_type;
	}

	public void setId_process_type(int id_process_type) {
		this.id_process_type = id_process_type;
	}

	public ProcessTypeEntity getProcessType() {
		return processType;
	}

	public void setProcessType(ProcessTypeEntity processType) {
		this.processType = processType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public boolean isActive_start() {
		return active_start;
	}

	public void setActive_start(boolean active_start) {
		this.active_start = active_start;
	}

	public boolean isActive_end() {
		return active_end;
	}

	public void setActive_end(boolean active_end) {
		this.active_end = active_end;
	}

	public boolean isActive_error() {
		return active_error;
	}

	public void setActive_error(boolean active_error) {
		this.active_error = active_error;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isAuth() {
		return auth;
	}

	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public boolean isStarttls_enable() {
		return starttls_enable;
	}

	public void setStarttls_enable(boolean starttls_enable) {
		this.starttls_enable = starttls_enable;
	}

	public String getEmail_to() {
		return email_to;
	}

	public void setEmail_to(String email_to) {
		this.email_to = email_to;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
