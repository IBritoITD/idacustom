package cl.itdchile.custom.model.afianza;

/**
 * @author Gerencia ITD
 *
 */
public class CTCHReportEntity {
	public String sello_sernac;
	public String nombre_cliente;
	public String dir1;
	public String dir2;
	public String dir3;
	public String dir4;
	public String dir5;
	public String fecha_corte;
	public String rut;
	public String svs;
	public String caev;
	public String plazo_remanente;
	public String num_cuota;
	public String saldo_UF;
	public String valor_dividendo_UF;
	public String fecha_prox_pago;
	public String costo_total_prepago;
	public String caev_bis;
	public String garantias_vigentes;
	public String cant_div_pagados;
	public String cant_div_venc_no_pagados;
	public String monto_vendido_no_pagado;
	public String monto_total_atrasado;
	public String seg_des_costo_mensual;
	public String seg_des_costo_total;
	public String seg_des_cobertura;
	public String seg_des_comp_nombre;
	public String seg_in_costo_mensual;
	public String seg_in_costo_total;
	public String seg_in_cobertura;
	public String seg_in_comp_nombre;
	public String seg_ces_costo_mensual;
	public String seg_ces_costo_total;
	public String seg_ces_cobertura;
	public String seg_ces_comp_nombre;
	public String tasa_anual;
	public String fecha_cambio_tasa;
	public String cargo_prepago;
	public String plazo_aviso_prepago;
	public String interes_monetario;
	public String gasto_cobranza;
	public String gasto_cobranza_2;
	public String correo;
	public String mes;

	public CTCHReportEntity() {
		super();
	}

	public CTCHReportEntity(String sello_sernac, String nombre_cliente, String dir1, String dir2, String dir3,
			String dir4, String dir5, String fecha_corte, String rut, String svs, String caev, String plazo_remanente,
			String num_cuota, String saldo_UF, String valor_dividendo_UF, String fecha_prox_pago,
			String costo_total_prepago, String caev_bis, String garantias_vigentes, String cant_div_pagados,
			String cant_div_venc_no_pagados, String monto_vendido_no_pagado, String monto_total_atrasado,
			String seg_des_costo_mensual, String seg_des_costo_total, String seg_des_cobertura,
			String seg_des_comp_nombre, String seg_in_costo_mensual, String seg_in_costo_total, String seg_in_cobertura,
			String seg_in_comp_nombre, String seg_ces_costo_mensual, String seg_ces_costo_total,
			String seg_ces_cobertura, String seg_ces_comp_nombre, String tasa_anual, String fecha_cambio_tasa,
			String cargo_prepago, String plazo_aviso_prepago, String interes_monetario, String gasto_cobranza,
			String gasto_cobranza_2,String correo,String mes) {
		super();
		this.sello_sernac = sello_sernac;
		this.nombre_cliente = nombre_cliente;
		this.dir1 = dir1;
		this.dir2 = dir2;
		this.dir3 = dir3;
		this.dir4 = dir4;
		this.dir5 = dir5;
		this.fecha_corte = fecha_corte;
		this.rut = rut;
		this.svs = svs;
		this.caev = caev;
		this.plazo_remanente = plazo_remanente;
		this.num_cuota = num_cuota;
		this.saldo_UF = saldo_UF;
		this.valor_dividendo_UF = valor_dividendo_UF;
		this.fecha_prox_pago = fecha_prox_pago;
		this.costo_total_prepago = costo_total_prepago;
		this.caev_bis = caev_bis;
		this.garantias_vigentes = garantias_vigentes;
		this.cant_div_pagados = cant_div_pagados;
		this.cant_div_venc_no_pagados = cant_div_venc_no_pagados;
		this.monto_vendido_no_pagado = monto_vendido_no_pagado;
		this.monto_total_atrasado = monto_total_atrasado;
		this.seg_des_costo_mensual = seg_des_costo_mensual;
		this.seg_des_costo_total = seg_des_costo_total;
		this.seg_des_cobertura = seg_des_cobertura;
		this.seg_des_comp_nombre = seg_des_comp_nombre;
		this.seg_in_costo_mensual = seg_in_costo_mensual;
		this.seg_in_costo_total = seg_in_costo_total;
		this.seg_in_cobertura = seg_in_cobertura;
		this.seg_in_comp_nombre = seg_in_comp_nombre;
		this.seg_ces_costo_mensual = seg_ces_costo_mensual;
		this.seg_ces_costo_total = seg_ces_costo_total;
		this.seg_ces_cobertura = seg_ces_cobertura;
		this.seg_ces_comp_nombre = seg_ces_comp_nombre;
		this.tasa_anual = tasa_anual;
		this.fecha_cambio_tasa = fecha_cambio_tasa;
		this.cargo_prepago = cargo_prepago;
		this.plazo_aviso_prepago = plazo_aviso_prepago;
		this.interes_monetario = interes_monetario;
		this.gasto_cobranza = gasto_cobranza;
		this.gasto_cobranza_2 = gasto_cobranza_2;
		this.correo= correo;
		this.mes= mes;
	}

	public String getSello_sernac() {
		return sello_sernac;
	}

	public void setSello_sernac(String sello_sernac) {
		this.sello_sernac = sello_sernac;
	}

	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	public String getDir1() {
		return dir1;
	}

	public void setDir1(String dir1) {
		this.dir1 = dir1;
	}

	public String getDir2() {
		return dir2;
	}

	public void setDir2(String dir2) {
		this.dir2 = dir2;
	}

	public String getDir3() {
		return dir3;
	}

	public void setDir3(String dir3) {
		this.dir3 = dir3;
	}

	public String getDir4() {
		return dir4;
	}

	public void setDir4(String dir4) {
		this.dir4 = dir4;
	}

	public String getDir5() {
		return dir5;
	}

	public void setDir5(String dir5) {
		this.dir5 = dir5;
	}

	public String getFecha_corte() {
		return fecha_corte;
	}

	public void setFecha_corte(String fecha_corte) {
		this.fecha_corte = fecha_corte;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getSvs() {
		return svs;
	}

	public void setSvs(String svs) {
		this.svs = svs;
	}

	public String getCaev() {
		return caev;
	}

	public void setCaev(String caev) {
		this.caev = caev;
	}

	public String getPlazo_remanente() {
		return plazo_remanente;
	}

	public void setPlazo_remanente(String plazo_remanente) {
		this.plazo_remanente = plazo_remanente;
	}

	public String getNum_cuota() {
		return num_cuota;
	}

	public void setNum_cuota(String num_cuota) {
		this.num_cuota = num_cuota;
	}

	public String getSaldo_UF() {
		return saldo_UF;
	}

	public void setSaldo_UF(String saldo_UF) {
		this.saldo_UF = saldo_UF;
	}

	public String getValor_dividendo_UF() {
		return valor_dividendo_UF;
	}

	public void setValor_dividendo_UF(String valor_dividendo_UF) {
		this.valor_dividendo_UF = valor_dividendo_UF;
	}

	public String getFecha_prox_pago() {
		return fecha_prox_pago;
	}

	public void setFecha_prox_pago(String fecha_prox_pago) {
		this.fecha_prox_pago = fecha_prox_pago;
	}

	public String getCosto_total_prepago() {
		return costo_total_prepago;
	}

	public void setCosto_total_prepago(String costo_total_prepago) {
		this.costo_total_prepago = costo_total_prepago;
	}

	public String getCaev_bis() {
		return caev_bis;
	}

	public void setCaev_bis(String caev_bis) {
		this.caev_bis = caev_bis;
	}

	public String getGarantias_vigentes() {
		return garantias_vigentes;
	}

	public void setGarantias_vigentes(String garantias_vigentes) {
		this.garantias_vigentes = garantias_vigentes;
	}

	public String getCant_div_pagados() {
		return cant_div_pagados;
	}

	public void setCant_div_pagados(String cant_div_pagados) {
		this.cant_div_pagados = cant_div_pagados;
	}

	public String getCant_div_venc_no_pagados() {
		return cant_div_venc_no_pagados;
	}

	public void setCant_div_venc_no_pagados(String cant_div_venc_no_pagados) {
		this.cant_div_venc_no_pagados = cant_div_venc_no_pagados;
	}

	public String getMonto_vendido_no_pagado() {
		return monto_vendido_no_pagado;
	}

	public void setMonto_vendido_no_pagado(String monto_vendido_no_pagado) {
		this.monto_vendido_no_pagado = monto_vendido_no_pagado;
	}

	public String getMonto_total_atrasado() {
		return monto_total_atrasado;
	}

	public void setMonto_total_atrasado(String monto_total_atrasado) {
		this.monto_total_atrasado = monto_total_atrasado;
	}

	public String getSeg_des_costo_mensual() {
		return seg_des_costo_mensual;
	}

	public void setSeg_des_costo_mensual(String seg_des_costo_mensual) {
		this.seg_des_costo_mensual = seg_des_costo_mensual;
	}

	public String getSeg_des_costo_total() {
		return seg_des_costo_total;
	}

	public void setSeg_des_costo_total(String seg_des_costo_total) {
		this.seg_des_costo_total = seg_des_costo_total;
	}

	public String getSeg_des_cobertura() {
		return seg_des_cobertura;
	}

	public void setSeg_des_cobertura(String seg_des_cobertura) {
		this.seg_des_cobertura = seg_des_cobertura;
	}

	public String getSeg_des_comp_nombre() {
		return seg_des_comp_nombre;
	}

	public void setSeg_des_comp_nombre(String seg_des_comp_nombre) {
		this.seg_des_comp_nombre = seg_des_comp_nombre;
	}

	public String getSeg_in_costo_mensual() {
		return seg_in_costo_mensual;
	}

	public void setSeg_in_costo_mensual(String seg_in_costo_mensual) {
		this.seg_in_costo_mensual = seg_in_costo_mensual;
	}

	public String getSeg_in_costo_total() {
		return seg_in_costo_total;
	}

	public void setSeg_in_costo_total(String seg_in_costo_total) {
		this.seg_in_costo_total = seg_in_costo_total;
	}

	public String getSeg_in_cobertura() {
		return seg_in_cobertura;
	}

	public void setSeg_in_cobertura(String seg_in_cobertura) {
		this.seg_in_cobertura = seg_in_cobertura;
	}

	public String getSeg_in_comp_nombre() {
		return seg_in_comp_nombre;
	}

	public void setSeg_in_comp_nombre(String seg_in_comp_nombre) {
		this.seg_in_comp_nombre = seg_in_comp_nombre;
	}

	public String getSeg_ces_costo_mensual() {
		return seg_ces_costo_mensual;
	}

	public void setSeg_ces_costo_mensual(String seg_ces_costo_mensual) {
		this.seg_ces_costo_mensual = seg_ces_costo_mensual;
	}

	public String getSeg_ces_costo_total() {
		return seg_ces_costo_total;
	}

	public void setSeg_ces_costo_total(String seg_ces_costo_total) {
		this.seg_ces_costo_total = seg_ces_costo_total;
	}

	public String getSeg_ces_cobertura() {
		return seg_ces_cobertura;
	}

	public void setSeg_ces_cobertura(String seg_ces_cobertura) {
		this.seg_ces_cobertura = seg_ces_cobertura;
	}

	public String getSeg_ces_comp_nombre() {
		return seg_ces_comp_nombre;
	}

	public void setSeg_ces_comp_nombre(String seg_ces_comp_nombre) {
		this.seg_ces_comp_nombre = seg_ces_comp_nombre;
	}

	public String getTasa_anual() {
		return tasa_anual;
	}

	public void setTasa_anual(String tasa_anual) {
		this.tasa_anual = tasa_anual;
	}

	public String getFecha_cambio_tasa() {
		return fecha_cambio_tasa;
	}

	public void setFecha_cambio_tasa(String fecha_cambio_tasa) {
		this.fecha_cambio_tasa = fecha_cambio_tasa;
	}

	public String getCargo_prepago() {
		return cargo_prepago;
	}

	public void setCargo_prepago(String cargo_prepago) {
		this.cargo_prepago = cargo_prepago;
	}

	public String getPlazo_aviso_prepago() {
		return plazo_aviso_prepago;
	}

	public void setPlazo_aviso_prepago(String plazo_aviso_prepago) {
		this.plazo_aviso_prepago = plazo_aviso_prepago;
	}

	public String getInteres_monetario() {
		return interes_monetario;
	}

	public void setInteres_monetario(String interes_monetario) {
		this.interes_monetario = interes_monetario;
	}

	public String getGasto_cobranza() {
		return gasto_cobranza;
	}

	public void setGasto_cobranza(String gasto_cobranza) {
		this.gasto_cobranza = gasto_cobranza;
	}

	public String getGasto_cobranza_2() {
		return gasto_cobranza_2;
	}

	public void setGasto_cobranza_2(String gasto_cobranza_2) {
		this.gasto_cobranza_2 = gasto_cobranza_2;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

}
