package cl.itdchile.custom.model.ida;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import cl.itdchile.custom.Constants;

/**
 *
 */
@Entity
@Table(name = "app_user")
@XmlRootElement
public class UserEntity implements Serializable, UserDetails {
	private static final long serialVersionUID = 3832626162173359411L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "username",nullable = false, length = 50, unique = true)
	private String username; // required
	
	@Column(name = "password",nullable = false)
	private String password; // required
	
	
	
	
	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName; // required
	
	@Column(name = "last_name", nullable = false, length = 50)
	private String lastName; // required
	
	@Column(name = "email",nullable = false)
	private String email; // required; unique
	
	@Column(name = "version")
	private Integer version;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "user_id") }, inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<RoleEntity> roles = new HashSet<RoleEntity>();
	
	@Column(name = "account_enabled")
	private boolean enabled;
	
	@Column(name = "account_expired", nullable = false)
	private boolean accountExpired;
	
	@Column(name = "account_locked", nullable = false)
	private boolean accountLocked;
	
	@Column(name = "credentials_expired", nullable = false)
	private boolean credentialsExpired;
	
	
	
	@Column(name = "created_at", nullable = false)
	private Date createdAt;
	
	@Column(name = "updated_at", nullable = true)
	private Date updatedAt;
	
	@Column(name = "password_changed_at", nullable = false)
	private Date passwordChangedAt;
	
	@Column(name = "last_login_at", nullable = true)
	private Date lastLoginAt;
	
	@Column(name = "last_login_ip", nullable = true)
	private String lastLoginIp;

	/**
	 * Default constructor - creates a new instance with no values set.
	 */
	public UserEntity() {
	}

	

	@Transient
	public boolean getNormalUser() {

		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			RoleEntity role = (RoleEntity) iterator.next();
			if (role.getName().equals(Constants.ADMIN_ROLE)) {
				return false;
			}

		}
		return true;
	}

	/**
	 * Create a new instance and set the username.
	 * 
	 * @param username login name for user.
	 */
	public UserEntity(final String username) {
		this.username = username;
	}

	
	public Date getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(Date lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	@Column(name = "last_login_ip", nullable = true)
	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	
	public Date getPasswordChangedAt() {
		return passwordChangedAt;
	}

	public void setPasswordChangedAt(Date passwordChangedAt) {
		this.passwordChangedAt = passwordChangedAt;
	}

	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	
	public String getPassword() {
		return password;
	}


	
	public String getFirstName() {
		return firstName;
	}

	
	public String getLastName() {
		return lastName;
	}

	
	public String getEmail() {
		return email;
	}

	/**
	 * Returns the full name.
	 * 
	 * @return firstName + ' ' + lastName
	 */
	@Transient
	public String getFullName() {
		return firstName + ' ' + lastName;
	}

	
	public Set<RoleEntity> getRoles() {
		return roles;
	}

	/**
	 * Convert user roles to LabelValue objects for convenience.
	 * 
	 * @return a list of LabelValue objects with role information
	 */
	@Transient
	public List<LabelValueComparator> getRoleList() {
		List<LabelValueComparator> userRoles = new ArrayList<LabelValueComparator>();

		if (this.roles != null) {
			for (RoleEntity role : roles) {
				// convert the user's roles to LabelValue Objects
				userRoles.add(new LabelValueComparator(role.getName(), role.getName()));
			}
		}

		return userRoles;
	}

	/**
	 * Adds a role for the user
	 * 
	 * @param role the fully instantiated role
	 */
	public void addRole(RoleEntity role) {
		getRoles().add(role);
	}

	/**
	 * @return GrantedAuthority[] an array of roles.
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	@Transient
	public Set<GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new LinkedHashSet<GrantedAuthority>();
		authorities.addAll(roles);
		return authorities;
	}

	@Version
	public Integer getVersion() {
		return version;
	}


	public boolean isEnabled() {
		return enabled;
	}

	
	public boolean isAccountExpired() {
		return accountExpired;
	}

	/**
	 * @see org.springframework.security.userdetails.UserDetails#isAccountNonExpired()
	 */
	@Transient
	public boolean isAccountNonExpired() {
		return !isAccountExpired();
	}

	
	public boolean isAccountLocked() {
		return accountLocked;
	}

	/**
	 * @see org.springframework.security.userdetails.UserDetails#isAccountNonLocked()
	 */
	@Transient
	public boolean isAccountNonLocked() {
		return !isAccountLocked();
	}

	
	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	/**
	 * @see org.springframework.security.userdetails.UserDetails#isCredentialsNonExpired()
	 */
	@Transient
	public boolean isCredentialsNonExpired() {
		return !credentialsExpired;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRoles(Set<RoleEntity> roles) {
		this.roles = roles;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

}
