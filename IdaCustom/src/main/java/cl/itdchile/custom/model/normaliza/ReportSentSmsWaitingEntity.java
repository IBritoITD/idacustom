package cl.itdchile.custom.model.normaliza;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import cl.itdchile.custom.model.ida.SentSmsEntity;

/**
 * @author Gerencia ITD
 *
 */
@Entity
@Table(name = "t_report_sent_sms_waiting")
@NamedNativeQueries({
		@NamedNativeQuery(resultClass = ReportSentSmsWaitingEntity.class, name = "reportSentSmsWaiting", query = "SELECT * FROM t_report_sent_sms_waiting") })
public class ReportSentSmsWaitingEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "id_sent_sms_ida", nullable = false)
	private long id_sent_sms_ida;

	public ReportSentSmsWaitingEntity(long id_sent_sms_ida, String telefono, String mensaje, Date fecha_envio,
			String estado, Date fecha_estado) {
		super();
		this.id_sent_sms_ida = id_sent_sms_ida;
		this.telefono = telefono;
		this.mensaje = mensaje;
		this.fecha_envio = fecha_envio;
		this.estado = estado;
		this.fecha_estado = fecha_estado;
	}

	@Column(name = "telefono", nullable = false)
	private String telefono;

	@Column(name = "mensaje", nullable = false)
	private String mensaje;

	@Column(name = "fecha_envio", nullable = false)
	private Date fecha_envio;

	@Column(name = "estado", nullable = false)
	private String estado;

	@Column(name = "fecha_estado", nullable = true)
	private Date fecha_estado;

	public ReportSentSmsWaitingEntity() {
		super();
	}

	public long getId_sent_sms_ida() {
		return id_sent_sms_ida;
	}

	public void setId_sent_sms_ida(long id_sent_sms_ida) {
		this.id_sent_sms_ida = id_sent_sms_ida;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	public Date getFecha_estado() {
		return fecha_estado;
	}

	public void setFecha_estado(Date fecha_estado) {
		this.fecha_estado = fecha_estado;
	}

	public int getId() {
		return id;
	}

}
