package cl.itdchile.custom.model.seideman;

import java.util.List;

/**
 * @author Gerencia ITD
 *
 */
public class CartolaReportEntity {
	private String type;
	private String rut;
	private String num_ctas;
	private String nombres;
	private String apellido1;
	private String apellido2;
	private String dir;
	private String artificio;
	private String fecha_stat_cta;
	private String total_pagar;
	private String pagar_hasta;
	private String cupon_total;
	private String cupon_utilizado;
	private String cupon_disponible;
	private String cupon_total_efectivo;
	private String cupon_utilizado_efectivo;
	private String cupon_disponible_efectivo;
	private String cae_prepago;
	private String tasa_interes_vigente_rotativo;
	private String tasa_interes_vigente_cuotas;
	private String tasa_interes_vigente_avances;
	private String cae_rotativo;
	private String cae_cuotas;
	private String cae_avances;
	private String calc_ce;
	private String periodo_fact_desde;
	private String periodo_fact_hasta;
	private String pagar_hasta2;
	private String periodo_ant_desde;
	private String periodo_ant_hasta;
	private String saldo_adeudado_inicial_periodo_ant;
	private String monto_facturado_periodo_anterior;
	private String monto_pagado_periodo_anterior;
	private String saldo_adeudado_final_periodo_ant;
	private String monto_total_facturado;
	private String costo_monerario_prepago;
	private String monto_minimo_pagar;
	private String fecha_vencimiento_prox;
	private String monto_prox;
	private String fecha_vencimiento_prox1;
	private String monto_prox1;
	private String fecha_vencimiento_prox2;
	private String monto_prox2;
	private String fecha_vencimiento_prox3;
	private String monto_prox3;
	private String proximo_periodo_fact_desde;
	private String proximo_periodo_fact_hasta;
	private String interes_monetario;
	private String gasto_cobranzas;
	private List<CartolaGraphicReportEntity> cartolaGraphicList;
	private String correo;
	private List<CartolaDetailReportEntity> total_operaciones_list;
	private List<CartolaDetailReportEntity> productos_servicios_list;
	private List<CartolaDetailReportEntity> cargos_comisiones_list;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNum_ctas() {
		return num_ctas;
	}

	public void setNum_ctas(String num_ctas) {
		this.num_ctas = num_ctas;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getFecha_stat_cta() {
		return fecha_stat_cta;
	}

	public void setFecha_stat_cta(String fecha_stat_cta) {
		this.fecha_stat_cta = fecha_stat_cta;
	}

	public String getTotal_pagar() {
		return total_pagar;
	}

	public void setTotal_pagar(String total_pagar) {
		this.total_pagar = total_pagar;
	}

	public String getPagar_hasta() {
		return pagar_hasta;
	}

	public void setPagar_hasta(String pagar_hasta) {
		this.pagar_hasta = pagar_hasta;
	}

	public String getCupon_total() {
		return cupon_total;
	}

	public void setCupon_total(String cupon_total) {
		this.cupon_total = cupon_total;
	}

	public String getCupon_utilizado() {
		return cupon_utilizado;
	}

	public void setCupon_utilizado(String cupon_utilizado) {
		this.cupon_utilizado = cupon_utilizado;
	}

	public String getCupon_disponible() {
		return cupon_disponible;
	}

	public void setCupon_disponible(String cupon_disponible) {
		this.cupon_disponible = cupon_disponible;
	}

	public String getCupon_total_efectivo() {
		return cupon_total_efectivo;
	}

	public void setCupon_total_efectivo(String cupon_total_efectivo) {
		this.cupon_total_efectivo = cupon_total_efectivo;
	}

	public String getCupon_utilizado_efectivo() {
		return cupon_utilizado_efectivo;
	}

	public void setCupon_utilizado_efectivo(String cupon_utilizado_efectivo) {
		this.cupon_utilizado_efectivo = cupon_utilizado_efectivo;
	}

	public String getCupon_disponible_efectivo() {
		return cupon_disponible_efectivo;
	}

	public void setCupon_disponible_efectivo(String cupon_disponible_efectivo) {
		this.cupon_disponible_efectivo = cupon_disponible_efectivo;
	}

	public String getCae_prepago() {
		return cae_prepago;
	}

	public void setCae_prepago(String cae_prepago) {
		this.cae_prepago = cae_prepago;
	}

	public String getTasa_interes_vigente_rotativo() {
		return tasa_interes_vigente_rotativo;
	}

	public void setTasa_interes_vigente_rotativo(String tasa_interes_vigente_rotativo) {
		this.tasa_interes_vigente_rotativo = tasa_interes_vigente_rotativo;
	}

	public String getTasa_interes_vigente_cuotas() {
		return tasa_interes_vigente_cuotas;
	}

	public void setTasa_interes_vigente_cuotas(String tasa_interes_vigente_cuotas) {
		this.tasa_interes_vigente_cuotas = tasa_interes_vigente_cuotas;
	}

	public String getTasa_interes_vigente_avances() {
		return tasa_interes_vigente_avances;
	}

	public void setTasa_interes_vigente_avances(String tasa_interes_vigente_avances) {
		this.tasa_interes_vigente_avances = tasa_interes_vigente_avances;
	}

	public String getCae_rotativo() {
		return cae_rotativo;
	}

	public void setCae_rotativo(String cae_rotativo) {
		this.cae_rotativo = cae_rotativo;
	}

	public String getCae_cuotas() {
		return cae_cuotas;
	}

	public void setCae_cuotas(String cae_cuotas) {
		this.cae_cuotas = cae_cuotas;
	}

	public String getCae_avances() {
		return cae_avances;
	}

	public void setCae_avances(String cae_avances) {
		this.cae_avances = cae_avances;
	}

	public String getCalc_ce() {
		return calc_ce;
	}

	public void setCalc_ce(String calc_ce) {
		this.calc_ce = calc_ce;
	}

	public String getPeriodo_fact_desde() {
		return periodo_fact_desde;
	}

	public void setPeriodo_fact_desde(String periodo_fact_desde) {
		this.periodo_fact_desde = periodo_fact_desde;
	}

	public String getPeriodo_fact_hasta() {
		return periodo_fact_hasta;
	}

	public void setPeriodo_fact_hasta(String periodo_fact_hasta) {
		this.periodo_fact_hasta = periodo_fact_hasta;
	}

	public String getPagar_hasta2() {
		return pagar_hasta2;
	}

	public void setPagar_hasta2(String pagar_hasta2) {
		this.pagar_hasta2 = pagar_hasta2;
	}

	public String getPeriodo_ant_desde() {
		return periodo_ant_desde;
	}

	public void setPeriodo_ant_desde(String periodo_ant_desde) {
		this.periodo_ant_desde = periodo_ant_desde;
	}

	public String getPeriodo_ant_hasta() {
		return periodo_ant_hasta;
	}

	public void setPeriodo_ant_hasta(String periodo_ant_hasta) {
		this.periodo_ant_hasta = periodo_ant_hasta;
	}

	public String getSaldo_adeudado_inicial_periodo_ant() {
		return saldo_adeudado_inicial_periodo_ant;
	}

	public void setSaldo_adeudado_inicial_periodo_ant(String saldo_adeudado_inicial_periodo_ant) {
		this.saldo_adeudado_inicial_periodo_ant = saldo_adeudado_inicial_periodo_ant;
	}

	public String getMonto_facturado_periodo_anterior() {
		return monto_facturado_periodo_anterior;
	}

	public void setMonto_facturado_periodo_anterior(String monto_facturado_periodo_anterior) {
		this.monto_facturado_periodo_anterior = monto_facturado_periodo_anterior;
	}

	public String getMonto_pagado_periodo_anterior() {
		return monto_pagado_periodo_anterior;
	}

	public void setMonto_pagado_periodo_anterior(String monto_pagado_periodo_anterior) {
		this.monto_pagado_periodo_anterior = monto_pagado_periodo_anterior;
	}

	public String getSaldo_adeudado_final_periodo_ant() {
		return saldo_adeudado_final_periodo_ant;
	}

	public void setSaldo_adeudado_final_periodo_ant(String saldo_adeudado_final_periodo_ant) {
		this.saldo_adeudado_final_periodo_ant = saldo_adeudado_final_periodo_ant;
	}

	public String getMonto_total_facturado() {
		return monto_total_facturado;
	}

	public void setMonto_total_facturado(String monto_total_facturado) {
		this.monto_total_facturado = monto_total_facturado;
	}

	public String getCosto_monerario_prepago() {
		return costo_monerario_prepago;
	}

	public void setCosto_monerario_prepago(String costo_monerario_prepago) {
		this.costo_monerario_prepago = costo_monerario_prepago;
	}

	public String getMonto_minimo_pagar() {
		return monto_minimo_pagar;
	}

	public void setMonto_minimo_pagar(String monto_minimo_pagar) {
		this.monto_minimo_pagar = monto_minimo_pagar;
	}

	public String getFecha_vencimiento_prox() {
		return fecha_vencimiento_prox;
	}

	public void setFecha_vencimiento_prox(String fecha_vencimiento_prox) {
		this.fecha_vencimiento_prox = fecha_vencimiento_prox;
	}

	public String getMonto_prox() {
		return monto_prox;
	}

	public void setMonto_prox(String monto_prox) {
		this.monto_prox = monto_prox;
	}

	public String getFecha_vencimiento_prox1() {
		return fecha_vencimiento_prox1;
	}

	public void setFecha_vencimiento_prox1(String fecha_vencimiento_prox1) {
		this.fecha_vencimiento_prox1 = fecha_vencimiento_prox1;
	}

	public String getMonto_prox1() {
		return monto_prox1;
	}

	public void setMonto_prox1(String monto_prox1) {
		this.monto_prox1 = monto_prox1;
	}

	public String getFecha_vencimiento_prox2() {
		return fecha_vencimiento_prox2;
	}

	public void setFecha_vencimiento_prox2(String fecha_vencimiento_prox2) {
		this.fecha_vencimiento_prox2 = fecha_vencimiento_prox2;
	}

	public String getMonto_prox2() {
		return monto_prox2;
	}

	public void setMonto_prox2(String monto_prox2) {
		this.monto_prox2 = monto_prox2;
	}

	public String getFecha_vencimiento_prox3() {
		return fecha_vencimiento_prox3;
	}

	public void setFecha_vencimiento_prox3(String fecha_vencimiento_prox3) {
		this.fecha_vencimiento_prox3 = fecha_vencimiento_prox3;
	}

	public String getMonto_prox3() {
		return monto_prox3;
	}

	public void setMonto_prox3(String monto_prox3) {
		this.monto_prox3 = monto_prox3;
	}

	public String getProximo_periodo_fact_desde() {
		return proximo_periodo_fact_desde;
	}

	public void setProximo_periodo_fact_desde(String proximo_periodo_fact_desde) {
		this.proximo_periodo_fact_desde = proximo_periodo_fact_desde;
	}

	public String getProximo_periodo_fact_hasta() {
		return proximo_periodo_fact_hasta;
	}

	public void setProximo_periodo_fact_hasta(String proximo_periodo_fact_hasta) {
		this.proximo_periodo_fact_hasta = proximo_periodo_fact_hasta;
	}

	public String getInteres_monetario() {
		return interes_monetario;
	}

	public void setInteres_monetario(String interes_monetario) {
		this.interes_monetario = interes_monetario;
	}

	public String getGasto_cobranzas() {
		return gasto_cobranzas;
	}

	public void setGasto_cobranzas(String gasto_cobranzas) {
		this.gasto_cobranzas = gasto_cobranzas;
	}

	public List<CartolaGraphicReportEntity> getCartolaGraphicList() {
		return cartolaGraphicList;
	}

	public void setCartolaGraphicList(List<CartolaGraphicReportEntity> cartolaGraphicList) {
		this.cartolaGraphicList = cartolaGraphicList;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public List<CartolaDetailReportEntity> getTotal_operaciones_list() {
		return total_operaciones_list;
	}

	public void setTotal_operaciones_list(List<CartolaDetailReportEntity> total_operaciones_list) {
		this.total_operaciones_list = total_operaciones_list;
	}

	public List<CartolaDetailReportEntity> getProductos_servicios_list() {
		return productos_servicios_list;
	}

	public void setProductos_servicios_list(List<CartolaDetailReportEntity> productos_servicios_list) {
		this.productos_servicios_list = productos_servicios_list;
	}

	public List<CartolaDetailReportEntity> getCargos_comisiones_list() {
		return cargos_comisiones_list;
	}

	public void setCargos_comisiones_list(List<CartolaDetailReportEntity> cargos_comisiones_list) {
		this.cargos_comisiones_list = cargos_comisiones_list;
	}

	public CartolaReportEntity(String rut) {
		super();
		this.rut = rut;
	}

	public CartolaReportEntity(String type, String rut, String num_ctas, String nombres, String apellido1,
			String apellido2, String dir, String artificio, String fecha_stat_cta, String total_pagar,
			String pagar_hasta, String cupon_total, String cupon_utilizado, String cupon_disponible,
			String cupon_total_efectivo, String cupon_utilizado_efectivo, String cupon_disponible_efectivo,
			String cae_prepago, String tasa_interes_vigente_rotativo, String tasa_interes_vigente_cuotas,
			String tasa_interes_vigente_avances, String cae_rotativo, String cae_cuotas, String cae_avances,
			String calc_ce, String periodo_fact_desde, String periodo_fact_hasta, String pagar_hasta2,
			String periodo_ant_desde, String periodo_ant_hasta, String saldo_adeudado_inicial_periodo_ant,
			String monto_facturado_periodo_anterior, String monto_pagado_periodo_anterior,
			String saldo_adeudado_final_periodo_ant, String monto_total_facturado, String costo_monerario_prepago,
			String monto_minimo_pagar, String fecha_vencimiento_prox, String monto_prox, String fecha_vencimiento_prox1,
			String monto_prox1, String fecha_vencimiento_prox2, String monto_prox2, String fecha_vencimiento_prox3,
			String monto_prox3, String proximo_periodo_fact_desde, String proximo_periodo_fact_hasta,
			String interes_monetario, String gasto_cobranzas, String correo,
			List<CartolaDetailReportEntity> total_operaciones_list,
			List<CartolaDetailReportEntity> productos_servicios_list,
			List<CartolaDetailReportEntity> cargos_comisiones_list,
			List<CartolaGraphicReportEntity> cartolaGraphicList) {
		super();
		this.type = type;
		this.rut = rut;
		this.num_ctas = num_ctas;
		this.nombres = nombres;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dir = dir;
		this.fecha_stat_cta = fecha_stat_cta;
		this.total_pagar = total_pagar;
		this.pagar_hasta = pagar_hasta;
		this.cupon_total = cupon_total;
		this.cupon_utilizado = cupon_utilizado;
		this.cupon_disponible = cupon_disponible;
		this.cupon_total_efectivo = cupon_total_efectivo;
		this.cupon_utilizado_efectivo = cupon_utilizado_efectivo;
		this.cupon_disponible_efectivo = cupon_disponible_efectivo;
		this.cae_prepago = cae_prepago;
		this.tasa_interes_vigente_rotativo = tasa_interes_vigente_rotativo;
		this.tasa_interes_vigente_cuotas = tasa_interes_vigente_cuotas;
		this.tasa_interes_vigente_avances = tasa_interes_vigente_avances;
		this.cae_rotativo = cae_rotativo;
		this.cae_cuotas = cae_cuotas;
		this.cae_avances = cae_avances;
		this.calc_ce = calc_ce;
		this.periodo_fact_desde = periodo_fact_desde;
		this.periodo_fact_hasta = periodo_fact_hasta;
		this.pagar_hasta2 = pagar_hasta2;
		this.periodo_ant_desde = periodo_ant_desde;
		this.periodo_ant_hasta = periodo_ant_hasta;
		this.saldo_adeudado_inicial_periodo_ant = saldo_adeudado_inicial_periodo_ant;
		this.monto_facturado_periodo_anterior = monto_facturado_periodo_anterior;
		this.monto_pagado_periodo_anterior = monto_pagado_periodo_anterior;
		this.saldo_adeudado_final_periodo_ant = saldo_adeudado_final_periodo_ant;
		this.monto_total_facturado = monto_total_facturado;
		this.costo_monerario_prepago = costo_monerario_prepago;
		this.monto_minimo_pagar = monto_minimo_pagar;
		this.fecha_vencimiento_prox = fecha_vencimiento_prox;
		this.monto_prox = monto_prox;
		this.fecha_vencimiento_prox1 = fecha_vencimiento_prox1;
		this.monto_prox1 = monto_prox1;
		this.fecha_vencimiento_prox2 = fecha_vencimiento_prox2;
		this.monto_prox2 = monto_prox2;
		this.fecha_vencimiento_prox3 = fecha_vencimiento_prox3;
		this.monto_prox3 = monto_prox3;
		this.proximo_periodo_fact_desde = proximo_periodo_fact_desde;
		this.proximo_periodo_fact_hasta = proximo_periodo_fact_hasta;
		this.interes_monetario = interes_monetario;
		this.gasto_cobranzas = gasto_cobranzas;

		this.correo = correo;
		this.total_operaciones_list = total_operaciones_list;
		this.productos_servicios_list = productos_servicios_list;
		this.cargos_comisiones_list = cargos_comisiones_list;
		this.artificio = artificio;
		this.cartolaGraphicList = cartolaGraphicList;
	}

	public CartolaReportEntity() {
		super();
	}

	public String getArtificio() {
		return artificio;
	}

	public void setArtificio(String artificio) {
		this.artificio = artificio;
	}

}
