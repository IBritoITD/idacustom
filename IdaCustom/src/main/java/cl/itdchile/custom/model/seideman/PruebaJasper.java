package cl.itdchile.custom.model.seideman;

import java.util.ArrayList;
import java.util.List;

import cl.itdchile.custom.utils.FormatUtil;

public class PruebaJasper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static List<CartolaReportEntity> getCartola() {
		ArrayList<CartolaReportEntity> cartola = new ArrayList<CartolaReportEntity>();

		cartola.add(new CartolaReportEntity("1", "150936454", "32004", "JESSICA ALEXANDRA", "CASTRO", "ZAMORA",
				"POB.EL RECUERDO ALONDRA 21", "ARTIFICIO", "25/09/2018", "63.182", "10/10/2018", "400.000", "308.315",
				"129.400", "0", "0", "0", "23,577%", "0%", "2,910%", "2,910%", "0%", "52,530%", "54,210%", "20",
				"26/08/2018", "25/09/2018", "10/10/2018", "26/07/2018", "25/08/2018", "0", "59.918", "59.918", "0",
				"63.182", "63.182", "63.182", "10/11/2019", "47.312", "10/12/2018", "47.274", "10/01/2019", "30.477",
				"10/02/2019", "24.014", "26/09/2018", "25/10/2018", "0", "0", "renato2182@outlook.com",
				getTotal_operaciones_list(), getproductos_servicios_list(), getCargos_comisiones_list(),
				getCartolaGraphicList()));

		return cartola;
	}

	public static List<CartolaDetailReportEntity> getTotal_operaciones_list() {

		ArrayList<CartolaDetailReportEntity> detailt = new ArrayList<CartolaDetailReportEntity>();

		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));
		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));
		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));

		return detailt;
	}

	public static List<CartolaDetailReportEntity> getproductos_servicios_list() {

		ArrayList<CartolaDetailReportEntity> detailt = new ArrayList<CartolaDetailReportEntity>();

		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));

		return detailt;
	}

	public static List<CartolaDetailReportEntity> getCargos_comisiones_list() {

		ArrayList<CartolaDetailReportEntity> detailt = new ArrayList<CartolaDetailReportEntity>();

		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));
		detailt.add(new CartolaDetailReportEntity("2", "150936454", "32004", "VESTEX", "21/06/2018",
				"VESTUARIO Y TEXTIL LA CALERA", "26.990", "26.990", "3/3", "7.197"));

		return detailt;
	}

	public static List<CartolaGraphicReportEntity> getCartolaGraphicList() {
		ArrayList<CartolaGraphicReportEntity> detailt = new ArrayList<CartolaGraphicReportEntity>();

		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes2("20180510"), Double.valueOf("241075"),
				Double.valueOf("180000")));
		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes2("20180610"), Double.valueOf("144340"),
				Double.valueOf("120000")));
		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes2("20180710"), Double.valueOf("98004"),
				Double.valueOf("34000")));
		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes2("20180810"), Double.valueOf("107850"),
				Double.valueOf("114880")));
		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes2("20180910"),  Double.valueOf("59918"),  Double.valueOf("59920")));
		detailt.add(new CartolaGraphicReportEntity(FormatUtil.mes("10102018"),  Double.valueOf("63182"),  Double.valueOf("0")));

		return detailt;

	}

}
