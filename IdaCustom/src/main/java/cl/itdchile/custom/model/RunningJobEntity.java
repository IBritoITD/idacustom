package cl.itdchile.custom.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_running_job")
public class RunningJobEntity extends BaseObject {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	protected int id;

	@Column(name = "thread_name", nullable = false)
	protected String thread_name;

	@Column(name = "id_job_type", nullable = false, insertable = false, updatable = false)
	protected int id_job_type;

	@Column(name = "id_process_type", nullable = false, insertable = false, updatable = false)
	protected int id_process_type;

	@Column(name = "created_at", nullable = false)
	protected Date created_at;

	@OneToOne
	@JoinColumn(name = "id_job_type")
	private JobTypeEntity jobType;

	@OneToOne
	@JoinColumn(name = "id_process_type")
	private ProcessTypeEntity processType;

	@Transient
	protected boolean isThreadActive;

	@Transient
	protected String executionTime;

	public RunningJobEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JobTypeEntity getJobType() {
		return jobType;
	}

	public void setJobType(JobTypeEntity jobType) {
		this.jobType = jobType;
	}

	public ProcessTypeEntity getProcessType() {
		return processType;
	}

	public void setProcessType(ProcessTypeEntity processType) {
		this.processType = processType;
	}

	public String getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}

	public boolean isThreadActive() {
		return isThreadActive;
	}

	public void setThreadActive(boolean isThreadActive) {
		this.isThreadActive = isThreadActive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getThread_name() {
		return thread_name;
	}

	public void setThread_name(String thread_name) {
		this.thread_name = thread_name;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

//	public RunningJobEntity(String thread_name, int id_job_type, int id_process_type, Date created_at) {
//		super();
//		this.thread_name = thread_name;
//		this.id_job_type = id_job_type;
//		this.id_process_type = id_process_type;
//		this.created_at = created_at;
//	}

}
