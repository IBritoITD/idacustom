package cl.itdchile.custom.model.normaliza;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import cl.itdchile.custom.model.BaseObject;

@Entity
@Table(name = "t_report_sent_sms")
@NamedNativeQuery(resultClass=ReportSentSmsEntity.class, name="reportSentSms", query="SELECT * FROM t_report_sent_sms f WHERE EXTRACT(YEAR_MONTH FROM f.created_at)= EXTRACT(YEAR_MONTH FROM NOW()) and process_type_id = ?1 ")
public class ReportSentSmsEntity extends BaseObject implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected int id;

	@Column(name = "process_type_id")
	protected int process_type_id;

	@Column(name = "file", nullable = false)
	protected String file;

	@Column(name = "id_sent_ida_last_register",nullable = false)
	protected long id_sent_ida_last_register;

	@Column(name = "created_at_last_register",nullable = false)
	protected Date created_at_last_register;
	
	@Column(name = "created_at",nullable = false)
	protected Date created_at;

	

	public ReportSentSmsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getProcess_type_id() {
		return process_type_id;
	}

	public void setProcess_type_id(int process_type_id) {
		this.process_type_id = process_type_id;
	}

	
	
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public long getId_sent_ida_last_register() {
		return id_sent_ida_last_register;
	}

	public void setId_sent_ida_last_register(long id_sent_ida_last_register) {
		this.id_sent_ida_last_register = id_sent_ida_last_register;
	}

	public Date getCreated_at_last_register() {
		return created_at_last_register;
	}

	public void setCreated_at_last_register(Date created_at_last_register) {
		this.created_at_last_register = created_at_last_register;
	}

	
	
	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
