package cl.itdchile.custom.model.ida;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import cl.itdchile.custom.model.BaseObject;

@Entity
@Table(name = "sent_sms_links")
public class SmsLinkEntity extends BaseObject implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected long id;

	@Column(name = "code", nullable = false)
	protected String code;

	@Column(name = "os", nullable = false)
	protected String os;

	@Column(name = "navegator", nullable = false)
	protected String browser;

	@Column(name = "ip", nullable = false)
	protected String ip;

	@Column(name = "device", nullable = false)
	protected String device;

	@Column(name = "original_link", nullable = false)
	protected String original_link;

	@Column(name = "sent_sms_id", nullable = false)
	protected long sent_sms_id;

	@Column(name = "open_counter", nullable = false)
	protected int open_counter;

	@Column(name = "created_at", nullable = false, updatable = false)
	protected Date created_at;

	@Column(name = "opened_at", nullable = false)
	protected Date opened_at;

	public SmsLinkEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getOriginal_link() {
		return original_link;
	}

	public void setOriginal_link(String original_link) {
		this.original_link = original_link;
	}

	public long getSent_sms_id() {
		return sent_sms_id;
	}

	public void setSent_sms_id(long sent_sms_id) {
		this.sent_sms_id = sent_sms_id;
	}

	public int getOpen_counter() {
		return open_counter;
	}

	public void setOpen_counter(int open_counter) {
		this.open_counter = open_counter;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getOpened_at() {
		return opened_at;
	}

	public void setOpened_at(Date opened_at) {
		this.opened_at = opened_at;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
