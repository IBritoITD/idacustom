package cl.itdchile.custom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_job_type")
public class JobTypeEntity extends BaseObject {

	@Id
	@Column(name = "id", nullable = false)
	protected int id;

	@Column(name = "description", nullable = false)
	protected String description;

	public JobTypeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JobTypeEntity(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
