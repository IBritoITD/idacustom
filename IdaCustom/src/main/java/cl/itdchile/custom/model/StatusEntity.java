package cl.itdchile.custom.model;

import java.io.Serializable;

public class StatusEntity implements Serializable {

	protected int id;
	protected String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
