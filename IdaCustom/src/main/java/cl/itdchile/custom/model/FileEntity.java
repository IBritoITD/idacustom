package cl.itdchile.custom.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name="t_file")
@NamedNativeQuery(resultClass=FileEntity.class, name="FileForReport", query="SELECT * FROM t_file f WHERE f.status=0 AND f.processing=0 AND f.created_at < DATE_SUB(NOW(), INTERVAL 48 HOUR) order by f.updated_at asc")
public class FileEntity extends BaseObject {
	private static final long serialVersionUID = -6984748972442090525L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="file_name",nullable=false)
	private String fileName;
	@Column(name="header",nullable=false)
	private String header;
	@Column(name="created_at",nullable=false)
	private Date creadtedAt;
	@Column(name="updated_at",nullable=false)
	private Date updateddAt;
	@Column(columnDefinition = "TINYINT",name="processing",nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean processing;
	@Column(columnDefinition = "TINYINT",name="status",nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean status;
	
	
	public Date getUpdateddAt() {
		return updateddAt;
	}
	public void setUpdateddAt(Date updateddAt) {
		this.updateddAt = updateddAt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Date getCreadtedAt() {
		return creadtedAt;
	}
	public void setCreadtedAt(Date creadtedAt) {
		this.creadtedAt = creadtedAt;
	}
	public boolean isProcessing() {
		return processing;
	}
	public void setProcessing(boolean processing) {
		this.processing = processing;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
