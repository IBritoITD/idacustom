package cl.itdchile.custom.model.afianza;

import java.util.ArrayList;
import java.util.List;

import cl.itdchile.custom.utils.FormatUtil;

public class Prueba2Jasper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static List<CTCHReportEntity> getCTCH() {
		ArrayList<CTCHReportEntity> CTCH = new ArrayList<CTCHReportEntity>();

		CTCH.add(new CTCHReportEntity("NO", "JONATHAN MANUEL PEZO GAJARDO", "UNO 412", "LAS CAMELIAS", "BULNES",
				"�UBLE", "", "30-09-2018", "17.164.076-8", "A030201303-0131", "6,75 %", "211", "29 / 240",
				"UF 397,09720", "UF 3,17360", "10-10-2018", "UF 400,08580", "6,75 %", "SI", "29", "0", "UF 0,00000",
				"UF 0,00000", "UF 0,02313", "UF 0,27751", "SALDO INSOLUTO", "Rigel Seguros de Vida S.A.", "UF 0,09531",
				"UF 1,14372", "UF 852,55000", "Liberty compa��a de Seguros Generales S.A.", "UF 0,00000", "UF 0,00000",
				"6 meses con un mes de deducible", "Zenit Seguros Generales S.A.", "6,19 % FIJA", "NO APLICA",
				"1,5 MESES DE INTERES", "NO APLICA", "TASA MAXIMA CONVENCIONAL", "Escala progresiva de 9%, 6% y 3%",
				"con topes de UF 10 y UF 50 respectivamente", null, null));

		return CTCH;
	}

	public static List<VCHandVCLHReportEntity> getVCHyVCLH() {
		ArrayList<VCHandVCLHReportEntity> VCH = new ArrayList<VCHandVCLHReportEntity>();
		VCH.add(new VCHandVCLHReportEntity("PRG3028", "32 de 135", "01-Dec-2018", "10-Dec-2018", "UF 901,72830",
				"FRANCISCA DEL CARMEN DUARTE GAJARDO", "EL SAUCE 37 CASA 37", "VILLA NAVIDAD", "TENO", "SANTIAGO",
				"5,66130", "2,56410", "0,11433", "0,18778", "0,00000", "0,00000", "11557021-8", "29-Nov", "30-Nov",
				"01-Dec", "02-Dec", "03-Dec", "04-Dec", "05-Dec", "06-Dec", "07-Dec", "08-Dec", "09-Dec", "10-Dec",
				"234.755", "234.786", "234.817", "234.849", "234.880", "234.911", "234.943", "234.974", "235.005",
				"235.036", "235.068", "n.d.", null, null, "8,16626"));
		return VCH;
	}

}
