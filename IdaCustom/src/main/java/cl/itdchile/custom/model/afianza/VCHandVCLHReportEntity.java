package cl.itdchile.custom.model.afianza;

/**
 * @author Gerencia ITD
 *
 */
public class VCHandVCLHReportEntity {
	private String nro_contrato;
	private String nro_cuotas;
	private String fecha_vencimiento;
	private String pagar_hasta;
	private String saldo_vigente;
	private String nombre_cliente;
	private String dir1;
	private String dir2;
	private String dir3;
	private String dir4;
	private String renta_arrendamiento;
	private String amortizacion;
	private String seg_des;
	private String seg_in;
	private String seg_ces;
	private String otros_cargos;
	private String rut;
	private String fecha1;
	private String fecha2;
	private String fecha3;
	private String fecha4;
	private String fecha5;
	private String fecha6;
	private String fecha7;
	private String fecha8;
	private String fecha9;
	private String fecha10;
	private String fecha11;
	private String fecha12;
	private String clp1;
	private String clp2;
	private String clp3;
	private String clp4;
	private String clp5;
	private String clp6;
	private String clp7;
	private String clp8;
	private String clp9;
	private String clp10;
	private String clp11;
	private String clp12;
	private String total_pagar;
	private String correo;
	private String mes;

	public VCHandVCLHReportEntity() {
		super();
	}

	public VCHandVCLHReportEntity(String nro_contrato, String nro_cuotas, String fecha_vencimiento, String pagar_hasta,
			String saldo_vigente, String nombre_cliente, String dir1, String dir2, String dir3, String dir4,
		    String renta_arrendamiento, String amortizacion, String seg_des, String seg_in,
			String seg_ces, String otros_cargos, String rut, String fecha1, String fecha2, String fecha3, String fecha4,
			String fecha5, String fecha6, String fecha7, String fecha8, String fecha9, String fecha10, String fecha11,
			String fecha12, String clp1, String clp2, String clp3, String clp4, String clp5, String clp6, String clp7,
			String clp8, String clp9, String clp10, String clp11, String clp12, String correo, String mes,String total_pagar) {
		super();
		this.nro_contrato = nro_contrato;
		this.nro_cuotas = nro_cuotas;
		this.fecha_vencimiento = fecha_vencimiento;
		this.pagar_hasta = pagar_hasta;
		this.saldo_vigente = saldo_vigente;
		this.nombre_cliente = nombre_cliente;
		this.dir1 = dir1;
		this.dir2 = dir2;
		this.dir3 = dir3;
		this.dir4 = dir4;
		this.renta_arrendamiento = renta_arrendamiento;
		this.amortizacion = amortizacion;
		this.seg_des = seg_des;
		this.seg_in = seg_in;
		this.seg_ces = seg_ces;
		this.otros_cargos = otros_cargos;
		this.rut = rut;
		this.fecha1 = fecha1;
		this.fecha2 = fecha2;
		this.fecha3 = fecha3;
		this.fecha4 = fecha4;
		this.fecha5 = fecha5;
		this.fecha6 = fecha6;
		this.fecha7 = fecha7;
		this.fecha8 = fecha8;
		this.fecha9 = fecha9;
		this.fecha10 = fecha10;
		this.fecha11 = fecha11;
		this.fecha12 = fecha12;
		this.clp1 = clp1;
		this.clp2 = clp2;
		this.clp3 = clp3;
		this.clp4 = clp4;
		this.clp5 = clp5;
		this.clp6 = clp6;
		this.clp7 = clp7;
		this.clp8 = clp8;
		this.clp9 = clp9;
		this.clp10 = clp10;
		this.clp11 = clp11;
		this.clp12 = clp12;
		this.correo = correo;
		this.mes = mes;
		this.total_pagar= total_pagar;
	}

	public String getNro_contrato() {
		return nro_contrato;
	}

	public void setNro_contrato(String nro_contrato) {
		this.nro_contrato = nro_contrato;
	}

	public String getNro_cuotas() {
		return nro_cuotas;
	}

	public void setNro_cuotas(String nro_cuotas) {
		this.nro_cuotas = nro_cuotas;
	}

	public String getFecha_vencimiento() {
		return fecha_vencimiento;
	}

	public void setFecha_vencimiento(String fecha_vencimiento) {
		this.fecha_vencimiento = fecha_vencimiento;
	}

	public String getPagar_hasta() {
		return pagar_hasta;
	}

	public void setPagar_hasta(String pagar_hasta) {
		this.pagar_hasta = pagar_hasta;
	}

	public String getSaldo_vigente() {
		return saldo_vigente;
	}

	public void setSaldo_vigente(String saldo_vigente) {
		this.saldo_vigente = saldo_vigente;
	}

	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	public String getDir1() {
		return dir1;
	}

	public void setDir1(String dir1) {
		this.dir1 = dir1;
	}

	public String getDir2() {
		return dir2;
	}

	public void setDir2(String dir2) {
		this.dir2 = dir2;
	}

	public String getDir3() {
		return dir3;
	}

	public void setDir3(String dir3) {
		this.dir3 = dir3;
	}

	public String getDir4() {
		return dir4;
	}

	public void setDir4(String dir4) {
		this.dir4 = dir4;
	}



	public String getRenta_arrendamiento() {
		return renta_arrendamiento;
	}

	public void setRenta_arrendamiento(String renta_arrendamiento) {
		this.renta_arrendamiento = renta_arrendamiento;
	}

	public String getAmortizacion() {
		return amortizacion;
	}

	public void setAmortizacion(String amortizacion) {
		this.amortizacion = amortizacion;
	}

	public String getSeg_des() {
		return seg_des;
	}

	public void setSeg_des(String seg_des) {
		this.seg_des = seg_des;
	}

	public String getSeg_in() {
		return seg_in;
	}

	public void setSeg_in(String seg_in) {
		this.seg_in = seg_in;
	}

	public String getSeg_ces() {
		return seg_ces;
	}

	public void setSeg_ces(String seg_ces) {
		this.seg_ces = seg_ces;
	}

	public String getOtros_cargos() {
		return otros_cargos;
	}

	public void setOtros_cargos(String otros_cargos) {
		this.otros_cargos = otros_cargos;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getFecha1() {
		return fecha1;
	}

	public void setFecha1(String fecha1) {
		this.fecha1 = fecha1;
	}

	public String getFecha2() {
		return fecha2;
	}

	public void setFecha2(String fecha2) {
		this.fecha2 = fecha2;
	}

	public String getFecha3() {
		return fecha3;
	}

	public void setFecha3(String fecha3) {
		this.fecha3 = fecha3;
	}

	public String getFecha4() {
		return fecha4;
	}

	public void setFecha4(String fecha4) {
		this.fecha4 = fecha4;
	}

	public String getFecha5() {
		return fecha5;
	}

	public void setFecha5(String fecha5) {
		this.fecha5 = fecha5;
	}

	public String getFecha6() {
		return fecha6;
	}

	public void setFecha6(String fecha6) {
		this.fecha6 = fecha6;
	}

	public String getFecha7() {
		return fecha7;
	}

	public void setFecha7(String fecha7) {
		this.fecha7 = fecha7;
	}

	public String getFecha8() {
		return fecha8;
	}

	public void setFecha8(String fecha8) {
		this.fecha8 = fecha8;
	}

	public String getFecha9() {
		return fecha9;
	}

	public void setFecha9(String fecha9) {
		this.fecha9 = fecha9;
	}

	public String getFecha10() {
		return fecha10;
	}

	public void setFecha10(String fecha10) {
		this.fecha10 = fecha10;
	}

	public String getFecha11() {
		return fecha11;
	}

	public void setFecha11(String fecha11) {
		this.fecha11 = fecha11;
	}

	public String getFecha12() {
		return fecha12;
	}

	public void setFecha12(String fecha12) {
		this.fecha12 = fecha12;
	}

	public String getClp1() {
		return clp1;
	}

	public void setClp1(String clp1) {
		this.clp1 = clp1;
	}

	public String getClp2() {
		return clp2;
	}

	public void setClp2(String clp2) {
		this.clp2 = clp2;
	}

	public String getClp3() {
		return clp3;
	}

	public void setClp3(String clp3) {
		this.clp3 = clp3;
	}

	public String getClp4() {
		return clp4;
	}

	public void setClp4(String clp4) {
		this.clp4 = clp4;
	}

	public String getClp5() {
		return clp5;
	}

	public void setClp5(String clp5) {
		this.clp5 = clp5;
	}

	public String getClp6() {
		return clp6;
	}

	public void setClp6(String clp6) {
		this.clp6 = clp6;
	}

	public String getClp7() {
		return clp7;
	}

	public void setClp7(String clp7) {
		this.clp7 = clp7;
	}

	public String getClp8() {
		return clp8;
	}

	public void setClp8(String clp8) {
		this.clp8 = clp8;
	}

	public String getClp9() {
		return clp9;
	}

	public void setClp9(String clp9) {
		this.clp9 = clp9;
	}

	public String getClp10() {
		return clp10;
	}

	public void setClp10(String clp10) {
		this.clp10 = clp10;
	}

	public String getClp11() {
		return clp11;
	}

	public void setClp11(String clp11) {
		this.clp11 = clp11;
	}

	public String getClp12() {
		return clp12;
	}

	public void setClp12(String clp12) {
		this.clp12 = clp12;
	}



	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTotal_pagar() {
		return total_pagar;
	}

	public void setTotal_pagar(String total_pagar) {
		this.total_pagar = total_pagar;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

}
