package cl.itdchile.custom.model.ida;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import cl.itdchile.custom.model.BaseObject;

@Entity
@Table(name = "sent_sms")
@NamedNativeQueries({
		@NamedNativeQuery(resultClass = SentSmsEntity.class, name = "sentSmsClientAll", query = "SELECT * FROM sent_sms as f WHERE f.client_id = ?1 and EXTRACT(YEAR_MONTH FROM f.created_at)= EXTRACT(YEAR_MONTH FROM now()) LIMIT 0,100"),
		@NamedNativeQuery(resultClass = SentSmsEntity.class, name = "sentSmsClientAllValidateIda", query = "SELECT * FROM sent_sms as f WHERE f.client_id = ?1 and EXTRACT(YEAR_MONTH FROM f.created_at)= EXTRACT(YEAR_MONTH FROM now())  and id > ?2 and f.created_at >= ?3 LIMIT 0,100"),
		@NamedNativeQuery(resultClass = SentSmsEntity.class, name = "sentSmsId", query = "SELECT * FROM sent_sms as f WHERE f.id = ?1 ") })

public class SentSmsEntity extends BaseObject implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected Long id;

	@Column(name = "carrier", nullable = false)
	protected String carrier;

	@Column(name = "is_delivered", nullable = true)
	protected boolean is_delivered;

	@Column(name = "is_error", nullable = true)
	protected boolean is_error;

	@Column(name = "type", nullable = false)
	protected boolean type;

	@Column(name = "sms_key", nullable = false)
	protected String sms_key;

	@Column(name = "movil", nullable = false)
	protected String movil;

	@Column(name = "originator", nullable = false)
	protected String originator;

	@Column(name = "is_sent", nullable = false)
	protected boolean is_sent;

	@Column(name = "sent_at", nullable = true)
	protected Date sent_at;

	@Column(name = "message", nullable = false)
	protected String message;

	@Column(name = "status", nullable = false)
	protected String status;

	@Column(name = "updated_at", nullable = false)
	protected Date updated_at;

	@Column(name = "connector_id", nullable = false)
	protected Long connector_id;

	@Column(name = "client_id", nullable = false)
	protected Long client_id;

	@Column(name = "pattern_id", nullable = true)
	protected Long pattern_id;

	@Column(name = "uuid", nullable = false)
	protected String uuid;

	@Column(name = "queued_at", nullable = false)
	protected Date queued_at;

	@Column(name = "in_connector_id")
	protected Long in_connector_id;

	@Column(name = "send_dlr", nullable = false)
	protected boolean send_dlr;

	@Column(name = "source", nullable = false)
	protected boolean source;

	@Column(name = "dlr_at", nullable = true)
	protected Date dlr_at;

	@Column(name = "created_at", nullable = false)
	protected Date created_at;

	@Column(name = "custom_id", nullable = true)
	protected String custom_id;

	@Column(name = "credit_usage", nullable = true)
	protected double credit_usage;

	@Column(name = "retry_count", nullable = true)
	protected int retry_count;

	@Column(name = "message_response", nullable = true)
	protected String message_response;

	@Column(name = "user_id", nullable = true)
	protected Long user_id;

	public SentSmsEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public boolean isIs_delivered() {
		return is_delivered;
	}

	public void setIs_delivered(boolean is_delivered) {
		this.is_delivered = is_delivered;
	}

	public boolean isIs_error() {
		return is_error;
	}

	public void setIs_error(boolean is_error) {
		this.is_error = is_error;
	}

	public boolean isType() {
		return type;
	}

	public void setType(boolean type) {
		this.type = type;
	}

	public String getSms_key() {
		return sms_key;
	}

	public void setSms_key(String sms_key) {
		this.sms_key = sms_key;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public boolean isIs_sent() {
		return is_sent;
	}

	public void setIs_sent(boolean is_sent) {
		this.is_sent = is_sent;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public Long getConnector_id() {
		return connector_id;
	}

	public void setConnector_id(Long connector_id) {
		this.connector_id = connector_id;
	}

	public Long getClient_id() {
		return client_id;
	}

	public void setClient_id(Long client_id) {
		this.client_id = client_id;
	}

	public Long getPattern_id() {
		return pattern_id;
	}

	public void setPattern_id(Long pattern_id) {
		this.pattern_id = pattern_id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Date getQueued_at() {
		return queued_at;
	}

	public void setQueued_at(Date queued_at) {
		this.queued_at = queued_at;
	}

	public Long getIn_connector_id() {
		return in_connector_id;
	}

	public void setIn_connector_id(Long in_connector_id) {
		this.in_connector_id = in_connector_id;
	}

	public boolean isSend_dlr() {
		return send_dlr;
	}

	public void setSend_dlr(boolean send_dlr) {
		this.send_dlr = send_dlr;
	}

	public Date getSent_at() {
		return sent_at;
	}

	public void setSent_at(Date sent_at) {
		this.sent_at = sent_at;
	}

	public boolean isSource() {
		return source;
	}

	public void setSource(boolean source) {
		this.source = source;
	}

	public Date getDlr_at() {
		return dlr_at;
	}

	public void setDlr_at(Date dlr_at) {
		this.dlr_at = dlr_at;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCustom_id() {
		return custom_id;
	}

	public void setCustom_id(String custom_id) {
		this.custom_id = custom_id;
	}

	public double getCredit_usage() {
		return credit_usage;
	}

	public void setCredit_usage(double credit_usage) {
		this.credit_usage = credit_usage;
	}

	public int getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(int retry_count) {
		this.retry_count = retry_count;
	}

	public String getMessage_response() {
		return message_response;
	}

	public void setMessage_response(String message_response) {
		this.message_response = message_response;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

}
