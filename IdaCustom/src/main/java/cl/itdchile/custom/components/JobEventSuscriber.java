package cl.itdchile.custom.components;

import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.dao.IRunningJobDao;
import cl.itdchile.custom.model.RunningJobEntity;

@Component
public class JobEventSuscriber implements DisposableBean, Runnable {
	private static Logger log = Logger.getLogger(JobEventSuscriber.class);
	private Thread thread;
	Timer timer = new Timer();

	@Autowired
	protected IRunningJobDao dao;

	public JobEventSuscriber() {
		super();
		this.thread = new Thread(this);
		this.thread.setName("VerifyJobProcess");

		//this.thread.start();
	}

	TimerTask taskVerification = new TimerTask() {

		@Override
		@Transactional
		public void run() {
			List<RunningJobEntity> jobList = null;
			try {
				log.info("Starting job verification...");
				jobList = dao.getOldProcesses();
				if (jobList.size() > 0) {
					Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
					for (RunningJobEntity job : jobList) {
						if (!setOfThread.stream()
								.anyMatch(t -> t.getName().equalsIgnoreCase(job.getThread_name()) && t.isAlive())) {
							dao.delete(job);
						}
					}

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	};

	@Override
	public void run() {
		log.info("Start Executing VerifyJobProcess...");
		timer.schedule(taskVerification, 60000, 120000); // 2 minutos

//		timer.schedule(taskVerification, 10000, 600000); // 10 minutos // 80000000 1 dia 

//		timer.schedule(taskVerification, 10000, 80000000);  // 80000000 1 dia 

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

}
