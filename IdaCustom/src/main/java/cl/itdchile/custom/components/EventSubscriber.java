package cl.itdchile.custom.components;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cl.itdchile.custom.client.controller.ProcessesController;
import cl.itdchile.custom.dao.ISendSmsManager;

@Component
public class EventSubscriber implements DisposableBean, Runnable {
	private static Logger log = Logger.getLogger(ProcessesController.class);
	private Thread thread;
	Timer timer = new Timer();

	@Autowired
	private ISendSmsManager sendSmsManager;

	EventSubscriber() {
		super();
		this.thread = new Thread(this);
		this.thread.setName("VerifySendSmsThread");

		// this.thread.start();
	}

	@Override
	public void run() {
		log.info("Start Executing VerifySendSmsThread...");

		timer.schedule(taskVerification, 60000, 600000); // 1 minmuto
		timer.schedule(taskNotVerified, 60000, 6000000); // 5 min

//		timer.schedule(taskVerification, 10000, 600000); // 10 minutos // 80000000 1 dia 
//		timer.schedule(taskNotVerified, 10000, 1200000); // 20 minutos // 80000000 1 dia 

//		timer.schedule(taskVerification, 10000, 80000000);  // 80000000 1 dia 
//		timer.schedule(taskNotVerified, 10000, 80000000);  // 80000000 1 dia 

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	TimerTask taskVerification = new TimerTask() {

		@Override
		@Transactional
		public void run() {
			try {
				log.info("Starting sent messages verification...");
				sendSmsManager.getProcessed();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	};

	TimerTask taskNotVerified = new TimerTask() {

		@Override
		@Transactional
		public void run() {
			try {
				log.info("Starting messages not verified...");
				sendSmsManager.getNotVerified();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

}
